<?php

namespace App\Models;

use CodeIgniter\I18n\Time;
use CodeIgniter\Model;
use Ozdemir\Datatables\Datatables;
use Ozdemir\Datatables\DB\Codeigniter4Adapter;

class Mmaster extends Model
{
    public function log($activity = null)
    {
        return;
        if (session('i_user') != '' || session('i_user') != NULL) {
            $data = [
                'i_user' => session('i_user'),
                'ip_address' => $_SERVER['REMOTE_ADDR'],
                'time' => Time::now('Asia/Jakarta', 'id'),
                'activity' => $activity
            ];
            $this->db->table('tm_log')->insert($data);
        }
    }

    public function serverside()
    {
        $datatables = new Datatables(new Codeigniter4Adapter);
        $datatables->query("SELECT i_log, e_user_name, ip_address, time, activity FROM tm_log a, tr_user b WHERE a.i_user = b.i_user");
        return $datatables->generate();
    }

    public function check_user($i_user_code, $e_password)
    {
        $builder = $this->db->table('tr_user');
        $builder->select('*');
        $builder->where('lower(i_user_code)', lower($i_user_code));
        $builder->where('lower(e_user_password)', lower($e_password));
        $builder->where('f_active', 't');
        $query = $builder->get();
        if ($query->getNumRows() > 0) {
            return $query->getRowArray();
        }
    }

    public function get_menu($i_user, $i_level)
    {
        if ($i_user != '' && $i_level != '') {
            $builder = $this->db->table('tr_menu a');
            $builder->select('a.*');
            $builder->join('tm_user_role b', 'b.i_menu = a.i_menu', 'inner');
            $builder->where('a.i_parent', '0');
            $builder->where('a.f_status', true);
            $builder->orderBy(4, 1, 'ASC');
            $builder->distinct();
            return $builder->get();
        }
    }

    public function get_sub_menu($i_user, $i_menu, $i_department, $i_level)
    {
        if ($i_user != '' && $i_menu != '') {
            $builder = $this->db->table('tr_menu a');
            $builder->select('a.*');
            $builder->join('tm_user_role b', 'b.i_menu = a.i_menu', 'inner');
            $builder->where('a.i_parent', $i_menu);
            $builder->where('a.f_status', true);
            $builder->orderBy(4, 1, 'ASC');
            $builder->distinct();
            return $builder->get();
        }
    }

    public function check_role($i_user, $i_menu, $i_access, $i_department=null, $i_level=null)
    {
        if ($i_user != '' && $i_menu != '' && $i_access != '') {
            $builder = $this->db->table('tr_menu a');
            $builder->select('a.*');
            $builder->join('tm_user_role b', 'b.i_menu = a.i_menu', 'inner');
            $builder->where('a.i_menu', $i_menu);
            $builder->where('b.i_access', $i_access);
            $builder->orderBy(4, 1, 'ASC');
            return $builder->get();
        }
    }

    public function check_folder($i_user, $e_folder, $i_access, $i_department=null, $i_level=null)
    {
        if ($i_user != '' && $e_folder != '' && $i_access != '') {
            $builder = $this->db->table('tr_menu a');
            $builder->select('a.*');
            $builder->join('tm_user_role b', 'b.i_menu = a.i_menu', 'inner');
            $builder->where('a.e_folder', $e_folder);
            $builder->where('b.i_access', $i_access);
            $builder->orderBy(4, 1, 'ASC');
            return $builder->get();
        }
    }

    public function get_breadcrumb_title($e_folder)
    {
        $builder = $this->db->table('tr_menu');
        $builder->select('e_menu');
        $builder->where('e_folder', $e_folder);
        return $builder->get();
    }

    public function get_breadcrumb($e_folder)
    {
        return $this->db->query("SELECT i_menu, e_folder, e_menu FROM tr_menu WHERE 
        (i_menu = (SELECT i_menu FROM tr_menu WHERE e_folder = '$e_folder') OR 
        i_menu = (SELECT i_parent FROM tr_menu WHERE e_folder = '$e_folder') OR 
        i_menu = (SELECT i_parent FROM tr_menu WHERE i_menu = (SELECT i_parent FROM tr_menu WHERE e_folder = '$e_folder'))) 
        AND i_menu <> 0
        ORDER BY i_menu::varchar;");
    }

    public function get_super_admin()
    {
        return $this->db->table('tr_level')->getWhere(['f_super_admin' => 't']);
    }

    public function get_level_by_id($id)
    {
        return $this->db->table('tr_level')->getWhere(['i_level' => $id]);
    }

    public function get_department($i_user)
    {
        return;
        /* return $this->db->table('tr_department')->getWhere(['f_active' => 't']); */
        $builder = $this->db->table('tr_department a');
        $builder->select('a.i_department, a.e_department_name');
        $builder->join('tm_user_department_role b', 'b.i_department = a.i_department', 'inner');
        $builder->where('b.i_user', $i_user);
        $builder->where('a.f_active', 't');
        $builder->orderBy(2, 'ASC');
        $builder->distinct();
        return $builder->get();
    }

    public function get_level($i_user)
    {
        $builder = $this->db->table('tr_level a');
        $builder->select('a.i_level, a.e_level_name');
        $builder->join('tr_user u', 'u.i_level = a.i_level', 'inner');
        $builder->where('u.i_user', $i_user);
        $builder->where('u.f_active', 't');
        $builder->orderBy(2, 'ASC');
        $builder->distinct();
        return $builder->get();
    }

    public function check_level($i_user, $i_department, $i_level)
    {
        /* return $this->db->table('tr_level')->getWhere(['f_active' => 't']); */
        $builder = $this->db->table('tr_level a');
        $builder->select('a.i_level, a.e_level_name');
        $builder->where('a.f_active', 't');
        $builder->orderBy(2, 'ASC');
        $builder->distinct();
        return $builder->get();
    }

    public function set_session_department($i_user, $i_department)
    {
        $builder = $this->db->table('tr_user');
        $data = [
            'last_i_department' => $i_department,
        ];
        $builder->where('i_user', $i_user);
        $builder->update($data);
    }

    public function set_session_level($i_user, $i_level)
    {
        $builder = $this->db->table('tr_user');
        $data = [
            'i_level' => $i_level,
        ];
        $builder->where('i_user', $i_user);
        $builder->update($data);
    }
}
