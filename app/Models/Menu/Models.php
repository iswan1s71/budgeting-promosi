<?php

namespace App\Models\Menu;

use CodeIgniter\Model;
use Ozdemir\Datatables\Datatables;
use Ozdemir\Datatables\DB\Codeigniter4Adapter;

class Models extends Model
{
    protected $table = 'tr_menu';
    protected $primaryKey = 'i_menu';

    /** Datatable */
    public function serverside($folder, $i_menu)
    {
        $datatables = new Datatables(new Codeigniter4Adapter);
        $sql = "SELECT 0 AS no, i_menu::varchar AS id, e_menu, i_parent, n_urut, e_folder, icon, '$folder' AS folder, '$i_menu' AS i_menu 
                FROM tr_menu
                WHERE f_status = true";

        $datatables->query($sql);

        /** Cek Hak Akses, Apakah User Bisa Edit */
        // if (check_role($i_menu, 3)->getnumRows() > 0 || check_role($i_menu, 4)->getnumRows() > 0) {
        //     $datatables->add('action', function ($data) {
        //         $id         = $data['id'];
        //         $i_menu     = $data['i_menu'];
        //         $folder     = '/' . $data['folder'] . '/control';
        //         $data       = '';

        //         if (check_role($i_menu, 3)->getnumRows() > 0) {
        //             $data      .= "<a href='" . base_url($folder . '/update/' . encrypt_url($id)) . "' title='Edit Data'><i class='ph-note-pencil edit_data text-pink'></i></a>";
        //         }
        //         if (check_role($i_menu, 4)->getnumRows() > 0) {
        //             $data      .= "<a href='#' onclick='sweet_delete(\"" . $folder . "\",\"" . encrypt_url($id) . "\");' title='Delete Data'><i class='icon-database-remove text-pink-400'></i></a>";
        //         }

        //         return $data;
        //     });
        // }

        $datatables->add('action', function ($data) {
            $id = $data['id'];
            $i_menu = $data['i_menu'];
            $folder = '/' . $data['folder'] . '/control';

            $actions = "<a href='" . base_url($folder . '/update/' . encrypt_url($id)) . "' title='Edit Data'>
                        <i class='ph-note-pencil edit_data text-pink'></i>
                    </a>";

            $actions .= "<a href='#' onclick='sweet_delete_soft(\"" . $folder . "\",\"" . encrypt_url($id) . "\");' title='Delete Data'>
                        <i class='ph-trash text-danger'></i>
                    </a>";
            
            return $actions;
        });

        $datatables->edit('i_parent', function($data) {

            $i_parent = $data['i_parent'];

            $list = static::getList();

            $parent = @$list[$i_parent] ?? '';

            return $parent;
        });

        $datatables->hide('folder');
        $datatables->hide('i_menu');
        return $datatables->generate();
    }

    /** Search Menu */
    public function get_menu($cari)
    {
        return $this->db->query("SELECT i_menu, e_menu FROM tr_menu WHERE (e_menu ILIKE '%$cari%') AND e_folder = '#' ORDER BY i_menu");
    }

    /** Access */
    public function get_access()
    {
        return $this->db->table('tr_access')->get();
    }

    /** Check Already */
    public function check($i_name)
    {
        $builder = $this->db->table($this->table);
        $builder->select('i_menu');
        $builder->where('i_menu', $i_name);
        return $builder->get();
    }

    /** Insert Data */
    public function create($i_parent, $i_menu, $e_menu, $n_urut, $e_folder, $icon, $i_access)
    {
        $data = [
            'i_menu'    => $i_menu,
            'e_menu'    => $e_menu,
            'i_parent'  => $i_parent,
            'n_urut'    => $n_urut,
            'e_folder'  => $e_folder,
            'icon'      => $icon,
        ];
        $this->db->table($this->table)->insert($data);
        $i_level = _models()->get_super_admin()->getRow()->i_level;
        /* if ($i_access) {
            foreach ($i_access as $access) {
                $data = array(
                    'i_menu'    => $i_menu,
                    'i_level'   => $i_level,
                    'i_access'  => $access,
                );
                $this->db->table('tm_role')->insert($data);
            }
        } */
    }

    /** Get Data Edit */
    public function get_edit($id = null)
    {
        // return $this->db->table()->getWhere([$this->primaryKey => $id]);
        $builder = $this->db->table($this->table . ' a');
        $builder->select('a.*, b.e_menu AS menu_parent');
        $builder->join($this->table . ' b', 'b.i_menu = a.i_parent', 'left');
        $builder->where('a.' . $this->primaryKey, $id);
        return $builder->get();
    }

    /** Get Data Access Edit */
    public function get_access_edit($id)
    {
        $i_level = _models()->get_super_admin()->getRow()->i_level;
        return $this->db->query("SELECT a.*, b.selected FROM tr_access a LEFT JOIN (SELECT i_access, 'selected' AS selected FROM tm_role WHERE i_menu = '$id' AND i_level = '$i_level') b ON (b.i_access = a.i_access)");
    }

    /** Check Already Edit */
    public function check_edit($e_name, $e_name_old)
    {
        $builder = $this->db->table($this->table);
        $builder->select('e_menu');
        $builder->where('upper(e_menu)', upper($e_name_old));
        $builder->where('upper(e_menu) !=', upper($e_name_old));
        return $builder->get();
    }

    /** Edit Data */
    public function act_update($i_parent, $i_menu, $e_menu, $n_urut, $e_folder, $icon, $i_access, $i_menu_old)
    {
        $builder = $this->db->table($this->table);
        $data = [
            'i_menu'    => $i_menu,
            'e_menu'    => $e_menu,
            'i_parent'  => $i_parent,
            'n_urut'    => $n_urut,
            'e_folder'  => $e_folder,
            'icon'      => $icon,
        ];
        $builder->where($this->primaryKey, $i_menu_old);
        $builder->update($data);
        $i_level = _models()->get_super_admin()->getRow()->i_level;
        // if ($i_access) {
        //     $query = $this->db->table('tm_role');
        //     /* $query->where('i_menu', $i_menu_old); */
        //     $query->whereIn('i_menu', array($i_menu,$i_menu_old));
        //     $query->delete();
        //     foreach ($i_access as $access) {
        //         $data = array(
        //             'i_menu'    => $i_menu,
        //             'i_level'   => $i_level,
        //             'i_access'  => $access,
        //         );
        //         $this->db->table('tm_role')->insert($data);
        //     }
        // }
    }

    /** Change Data */
    public function toggle_status($id)
    {
        $this->db->query("UPDATE tr_menu SET f_status = CASE WHEN f_status = 't' THEN FALSE ELSE TRUE END WHERE i_menu = '$id' ");
    }

    public function act_soft_delete($id)
    {
        $builder = $this->db->table($this->table);
        $builder->where($this->primaryKey, $id);
        $builder->update(['f_status' => false]);
    }

    /** Delete Data */
    public function act_delete($id)
    {
        $builder = $this->db->table($this->table);
        $builder->where($this->primaryKey, $id);
        $builder->delete();
    }

    public static function getList()
    {
        $list = [];

        $model = new \App\Models\Menu\Models();
        $primaryKey = $model->primaryKey;    
        
        $sql = "SELECT * FROM $model->table WHERE f_status = true";
        
        $query = $model->db->query($sql)->getResult();

        foreach ($query as $row) {    
            $list[$row->$primaryKey] = $row->e_menu;
        }

        return $list;
    }
}
