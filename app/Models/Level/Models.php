<?php

namespace App\Models\Level;

use CodeIgniter\Model;
use Ozdemir\Datatables\Datatables;
use Ozdemir\Datatables\DB\Codeigniter4Adapter;

class Models extends Model
{
    protected $table = 'tr_level';
    protected $primaryKey = 'i_level';
    protected $allowedFields = ['e_level_name', 'e_note'];

    /** Datatable */
    public function serverside($folder, $i_menu)
    {
        $datatables = new Datatables(new Codeigniter4Adapter);
        $datatables->query("SELECT i_level AS id, e_level_name, e_note, f_active, CASE WHEN d_update ISNULL THEN d_entry ELSE d_update END AS d_update, '$folder' AS folder, '$i_menu' AS i_menu FROM tr_level WHERE f_super_admin = 'f'");
        $datatables->edit('f_active', function ($data) {
            $id         = $data['id'];
            $i_menu     = $data['i_menu'];
            $folder     = '/' . $data['folder'] . '/control';
            if ($data['f_active'] == 't') {
                $status = 'Active';
                $color  = 'primary';
            } else {
                $status = 'Not Active';
                $color  = 'pink';
            }
            if (check_role($i_menu, 3)->getnumRows() > 0) {
                $data = "<button class='btn btn-sm badge bg-$color badge-pill status' data-folder='".$folder."' data-id='".encrypt_url($id)."'>$status</button>";
            } else {
                $data = "<span class='badge bg-$color badge-pill'>$status</span>";
            }
            return $data;
        });
        

        /** Cek Hak Akses, Apakah User Bisa Edit */
        // if (check_role($i_menu, 3)->getnumRows() > 0) {
        //     $datatables->add('action', function ($data) {
        //         $id         = $data['id'];
        //         $name       = $data['e_level_name'];
        //         $note       = $data['e_note'];
        //         $i_menu     = $data['i_menu'];
        //         $folder     = '/' . $data['folder'] . '/control';
        //         $data       = '';
        //         // $data      .= "<a href='" . base_url($folder . '/update/' . encrypt_url($id)) . "' class='edit' data-id='".$id."' data-name='".$name."' title='Edit Data'><i class='ph-note-pencil text-pink'></i></a>";
        //         $data      .= "<a href='#' title='Edit Data'><i data-url='" . base_url($folder . '/act_update/') . "'  data-id='".encrypt_url($id)."' data-name='".$name."' data-note='".$note."' class='ph-note-pencil edit_data text-pink'></i></a>";
        //         return $data;
        //     });
        // }

        $datatables->add('action', function ($data) {
            $id = $data['id'];
            $name = $data['e_level_name'];
            $note = $data['e_note'];
            $i_menu = $data['i_menu'];
            $folder = '/' . $data['folder'] . '/control';

            $actions = "<a href='#' title='Edit Data'>
                            <i data-url='" . base_url($folder . '/act_update/') . "'  data-id='".encrypt_url($id)."' data-name='".$name."' data-note='".$note."' class='ph-note-pencil edit_data text-pink'></i>
                        </a>";

            $actions .= "<a href='#' onclick='sweet_delete(\"" . $folder . "\",\"" . encrypt_url($id) . "\");' title='Delete Data'>
                            <i class='ph-trash text-danger'></i>
                        </a>";
            return $actions;
        });

        $datatables->hide('folder');
        $datatables->hide('i_menu');
        return $datatables->generate();
    }

    /** Check Already */
    public function check($e_name)
    {
        $builder = $this->db->table($this->table);
        $builder->select('e_level_name');
        $builder->where('upper(e_level_name)', upper($e_name));
        return $builder->get();
    }

    /** Insert Data */
    public function create($e_name, $e_note)
    {
        $data = [
            'e_level_name' => $e_name,
            'e_note'  => $e_note,
        ];
        $this->db->table($this->table)->insert($data);
    }

    /** Get Data Edit */
    public function get_edit($id = null)
    {
        return $this->db->table($this->table)->getWhere([$this->primaryKey => $id]);
    }

    /** Check Already Edit */
    public function check_edit($e_name, $e_name_old)
    {
        $builder = $this->db->table($this->table);
        $builder->select('e_level_name');
        $builder->where('upper(e_level_name)', upper($e_name));
        $builder->where('upper(e_level_name) !=', upper($e_name_old));
        return $builder->get();
    }

    /** Edit Data */
    public function act_update($id, $e_name, $e_note)
    {
        $builder = $this->db->table($this->table);
        $data = [
            'e_level_name' => $e_name,
            'e_note'  => $e_note,
            'd_update' => current_time(),
        ];
        $builder->where($this->primaryKey, $id);
        $builder->update($data);
    }

    /** Change Data */
    public function change_status($id)
    {
        $this->db->query("UPDATE tr_level SET f_active = CASE WHEN f_active = 't' THEN FALSE ELSE TRUE END WHERE i_level = '$id' ");
    }

    /** Delete Data */
    public function act_delete($id)
    {
        $builder = $this->db->table($this->table);
        $builder->where($this->primaryKey, $id);
        $builder->delete();
    }

    public function get_all()
    {
        $sql = "SELECT * FROM $this->table WHERE i_level <> 1 OR e_level_name NOT ILIKE '%administrator%'";
        return $this->db->query($sql);
    }

    public static function get_list($asArray=false)
    {
        $model = new Models();

        $query = $model->get_all();

        if ($asArray) {
            $array = [];
            foreach ($query->getResult() as $result) {
                $_array = [
                    'id' => $result->i_level,
                    'text' => $result->e_level_name
                ];

                $array[] = $_array;
            }

            return json_encode($array);
        }

        return $query;
    }
}
