<?php

namespace App\Models\Department;

use CodeIgniter\Model;
use Ozdemir\Datatables\Datatables;
use Ozdemir\Datatables\DB\Codeigniter4Adapter;

class Models extends Model
{
    protected $table = 'tr_department';
    protected $primaryKey = 'i_department';
    protected $allowedFields = ['i_department_code', 'e_department_name', 'f_support'];

    /** Datatable */
    public function serverside($folder, $i_menu)
    {
        $datatables = new Datatables(new Codeigniter4Adapter);
        $datatables->query("SELECT i_department AS id, e_department_name, f_active, to_char(CASE WHEN d_update ISNULL THEN d_entry ELSE d_update END, 'YYYY-MM-DD HH24:MI') AS d_update, '$folder' AS folder, '$i_menu' AS i_menu FROM tr_department");        
        $datatables->edit('f_active', function ($data) {
            $id         = $data['id'];
            $i_menu     = $data['i_menu'];
            $folder     = '/' . $data['folder'] . '/control';
            if ($data['f_active'] == 't') {
                $status = 'Active';
                $color  = 'primary';
            } else {
                $status = 'Not Active';
                $color  = 'pink';
            }
            if (check_role($i_menu, 3)->getnumRows() > 0) {
                $data = "<button class='btn btn-sm badge bg-$color badge-pill status' data-folder='".$folder."' data-id='".encrypt_url($id)."'>$status</button>";
            } else {
                $data = "<span class='badge bg-$color badge-pill'>$status</span>";
            }
            return $data;
        });
        

        /** Cek Hak Akses, Apakah User Bisa Edit */
        if (check_role($i_menu, 3)->getnumRows() > 0) {
            $datatables->add('action', function ($data) {
                $id         = $data['id'];
                $name       = $data['e_department_name'];
                $i_menu     = $data['i_menu'];
                $folder     = '/' . $data['folder'] . '/control';
                $data       = '';
                // $data      .= "<a href='" . base_url($folder . '/update/' . encrypt_url($id)) . "' class='edit' data-id='".$id."' data-name='".$name."' title='Edit Data'><i class='ph-note-pencil text-pink'></i></a>";
                $data      .= "<a href='#' title='Edit Data'><i data-url='" . base_url($folder . '/act_update/') . "'  data-id='".encrypt_url($id)."' data-name='".$name."' class='ph-note-pencil edit_data text-pink'></i></a>";
                return $data;
            });
        }

        $datatables->hide('folder');
        $datatables->hide('i_menu');
        return $datatables->generate();
    }

    /** Check Already */
    public function check($i_code)
    {
        $builder = $this->db->table($this->table);
        $builder->select('i_department_code');
        $builder->where('upper(i_department_code)', upper($i_code));
        return $builder->get();
    }

    /** Insert Data */
    public function create(/* $i_code,  */$e_name)
    {
        $data = [
            // 'i_department_code' => $i_code,
            'e_department_name' => $e_name,
        ];
        $this->db->table($this->table)->insert($data);
    }

    /** Get Data Edit */
    public function get_edit($id)
    {
        return $this->db->table($this->table)->getWhere([$this->primaryKey => $id]);
    }

    /** Check Already Edit */
    public function check_edit($i_code, $i_code_old)
    {
        $builder = $this->db->table($this->table);
        $builder->select('i_department_code');
        $builder->where('upper(i_department_code)', upper($i_code));
        $builder->where('upper(i_department_code) !=', upper($i_code_old));
        return $builder->get();
    }

    /** Edit Data */
    public function act_update($id, /* $i_code, */ $e_name)
    {
        $builder = $this->db->table($this->table);
        $data = [
            // 'i_department_code' => $i_code,
            'e_department_name' => $e_name,
            'd_update' => current_time(),
        ];
        $builder->where($this->primaryKey, $id);
        $builder->update($data);
    }

    /** Change Data */
    public function change_status($id)
    {
        $this->db->query("UPDATE tr_department SET d_update = now(), f_active = CASE WHEN f_active = 't' THEN FALSE ELSE TRUE END WHERE i_department = '$id' ");
    }
}
