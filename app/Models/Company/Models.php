<?php

namespace App\Models\Company;

use CodeIgniter\Model;
use Ozdemir\Datatables\Datatables;
use Ozdemir\Datatables\DB\Codeigniter4Adapter;

class Models extends Model
{
    protected $table = 'tr_company';
    protected $primaryKey = 'i_company';
    protected $allowedFields = ['e_company_name', 'e_company_short_name', 'f_support', 'f_active'];

    /** Datatable */
    public function serverside($folder, $i_menu)
    {
        $datatables = new Datatables(new Codeigniter4Adapter);

        $sql = "SELECT i_company AS id, e_company_name, f_support, f_active,
                '$folder' AS folder, 
                '$i_menu' AS i_menu 
                FROM tr_company";

        $datatables->query($sql);

        $datatables->edit('f_support', function ($data) {
            $text = 'Yes';
            if ($data['f_support'] == 'f') {
                $text = 'No';
            }
            return $text;
        });

        $datatables->edit('f_active', function ($data) use($folder) {
            $id = $data['id'];
            $status = $data['f_active'];

            $class = 'success';
            $text = 'Active';
            if ($status == 'f') {
                $class = 'danger';
                $text = 'Inactive';
            }
            
            $button = "<a href='" . base_url($folder . '/control/toggle_status?id=' . encrypt_url($id)) . "' class='btn btn-$class btn-sm btn-toggle-status'>
                            $text
                        </a>";
            
            return $button;
        });

        /** Cek Hak Akses, Apakah User Bisa Edit */
        // if (check_role($i_menu, 3)->getNumRows() > 0) {
        //     $datatables->add('action', function ($data) {
        //         $id         = $data['id'];
        //         $i_menu     = $data['i_menu'];
        //         $folder     = '/' . $data['folder'] . '/control';
        //         $data       = '';
        //         $data      .= "<a href='" . base_url($folder . '/update/' . encrypt_url($id)) . "' title='Edit Data'><i class='icon-database-edit2 text-pink-400'></i></a>";
        //         return $data;
        //     });
        // }

        $datatables->add('action', function($data) {
            $id = $data['id'];
            $folder = '/' . $data['folder'] . '/control';

            $actions = "<a href='" . base_url($folder . '/view?id=' . encrypt_url($id)) . "'>
                            <i title='View Data' class='ph-eye text-secondary mr-1'></i>
                        </a>";

            $actions .= "<a href='" . base_url($folder . '/update?id=' . encrypt_url($id)) . "' title='Edit Data'>
                            <i title='Edit Data' class='ph-note-pencil text-pink'></i>
                        </a>";
            return $actions;
        });

        $datatables->hide('folder');
        $datatables->hide('i_menu');
        return $datatables->generate();
    }

    /** Check Already */
    public function check($e_name)
    {
        $builder = $this->db->table($this->table);
        $builder->select('e_company_name');
        $builder->where('upper(e_company_name)', upper($e_name));
        return $builder->get();
    }

    /** Insert Data */
    public function create($e_name)
    {
        $data = [
            'e_company_name' => $e_name,
        ];
        $this->db->table($this->table)->insert($data);
    }

    /** Get Data Edit */
    public function get_edit($id = null)
    {
        return $this->db->table($this->table)->getWhere([$this->primaryKey => $id]);
    }

    /** Check Already Edit */
    public function check_edit($e_name, $e_name_old)
    {
        $builder = $this->db->table($this->table);
        $builder->select('e_company_name');
        $builder->where('upper(e_company_name)', upper($e_name));
        $builder->where('upper(e_company_name) !=', upper($e_name_old));
        return $builder->get();
    }

    /** Edit Data */
    public function act_update($i_company, $e_company_name, $e_company_shortname, $f_support, $f_active)
    {
        $builder = $this->db->table($this->table);
        $data = [
            'e_company_name' => $e_company_name,
            'e_company_shortname' => $e_company_shortname,
            'f_support' => $f_support,
            'f_active' => $f_active,
            'd_update' => current_time(),
        ];
        $builder->where($this->primaryKey, $i_company);
        $builder->update($data);
    }

    /** Change Data */
    public function change_status($id)
    {
        $this->db->query("UPDATE tr_company SET f_active = CASE WHEN f_active = 't' THEN FALSE ELSE TRUE END WHERE i_company = '$id' ");
    }

    public function get_all($q='')
    {
        $builder = $this->db->table($this->table);
        $builder->select('*');
        $builder->where('f_active', true);

        if ($q != '') {
            $builder->like('lower(e_company_name)', lower($q));
        }

        return $builder->get();
    }

    public static function get_list($asArray=false, $q='')
    {
        $model = new Models();
        $query = $model->get_all($q);

        if ($asArray) {
            $array = [];

            foreach ($query->getResult() as $result) {
                $_array = [
                    'id' => $result->i_company,
                    'text' => $result->e_company_name
                ];

                $array[] = $_array;
            }

            return json_encode($array);
        }

        return $query;
    }

    /** Change Data */
    public function toggle_status($id)
    {
        $this->db->query("UPDATE $this->table SET f_active = CASE WHEN f_active = 't' THEN FALSE ELSE TRUE END WHERE i_company = '$id' ");
    }
}
