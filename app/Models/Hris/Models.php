<?php

namespace App\Models\Hris;

use CodeIgniter\Model;
use Ozdemir\Datatables\Datatables;
use Ozdemir\Datatables\DB\Codeigniter4Adapter;

class Models extends Model
{
    protected $config;
    protected $db;
    protected $dbname = 'hris';
    protected $tablename = 'user';

    public function __construct()
    {
        $this->connect();
    }


    public function connect()
    {   
        $config = new \Config\Database();
        $this->db = \Config\Database::connect($config->hris);        
    }

    
    public function get_connection()
    {
        if ($this->db == null) {
            $this->connect();
        }

        return $this->db;
    }

    public function get_all($q='')
    {
        $builder = $this->db->table($this->tablename . ' a ');
        $builder->select('a.iduser, a.username, a.password, b.e_department_name, c.nama AS e_cabang_name, k.nama');
        $builder->join('tr_department b', 'b.i_department = a.i_departement', 'left');
        $builder->join('level l', 'l.idlevel = a.idlevel', 'left');
        $builder->join('cabang c', 'c.idcabang = l.idcabang', 'left');
        $builder->join('karyawan k', 'k.iduser = a.iduser', 'inner');
        $builder->where('a.status', true);

        if ($q != '') {
            $builder->like('lower(username)', lower($q));
        }

        return $builder->get();
    }

    public function get_by_id($id) {
        $builder = $this->db->table($this->tablename . ' a ');
        $builder->select('a.iduser, a.username, a.password, b.e_department_name, c.nama AS e_cabang_name, k.nama');
        $builder->join('tr_department b', 'b.i_department = a.i_departement', 'left');
        $builder->join('level l', 'l.idlevel = a.idlevel', 'left');
        $builder->join('cabang c', 'c.idcabang = l.idcabang', 'left');
        $builder->join('karyawan k', 'k.iduser = a.iduser', 'inner');
        $builder->where('a.status', true);
        $builder->where('a.iduser', $id);
        return $builder->get();
    }


    public static function get()
    {
        $model = new Models();
        return $model->get_connection();
    }

    public static function get_list($asArray=false, $q='')
    {
        $model = new Models();
        $query = $model->get_all($q);

        if ($asArray) {
            $array = [];

            foreach ($query->getResult() as $result) {
                $_array = [
                    'id' => $result->iduser,
                    'text' => "$result->nama - $result->e_cabang_name"
                ];

                $array[] = $_array;
            }

            return json_encode($array);
        }

        return $query;
    }

    public static function get_detail($asArray=false, $id=null)
    {
        $model = new Models();
        $query = $model->get_by_id($id);

        if ($asArray) {
            $_array = [];

            foreach ($query->getResult() as $result) {
                $_array = [
                    'id' => $result->iduser,
                    'username' => $result->username,
                    'password' => $result->password,
                    'e_department_name' => @$result->e_department_name ?? '',
                    'e_cabang_name' => @$result->e_cabang_name ?? '',
                    'nama' => $result->nama
                ];
            }

            return json_encode($_array);
        }

        return $query;
    }

}
