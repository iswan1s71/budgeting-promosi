<?php

namespace App\Models\Role;

use CodeIgniter\Model;
use Ozdemir\Datatables\Datatables;
use Ozdemir\Datatables\DB\Codeigniter4Adapter;

class Models extends Model
{
    protected $table = 'tm_user_role';
    protected $primaryKey =  'i_user_role';
    protected $allowedFields = ['i_menu', 'i_level', 'i_access'];

    /** Datatable */
    public function serverside($folder, $i_menu)
    {
        $datatables = new Datatables(new Codeigniter4Adapter);
        
        $sql = "SELECT i_level AS id, e_level_name, '$folder' AS folder, '$i_menu' AS i_menu 
                    FROM tr_level 
                    WHERE f_super_admin = 'f' AND f_active = 't'";        

        $datatables->query($sql);

        /** Cek Hak Akses, Apakah User Bisa Edit */
        // if (check_role($i_menu, 3)->getnumRows() > 0) {
        //     $datatables->add('action', function ($data) {
        //         $id         = $data['id'];
        //         $folder     = '/' . $data['folder'] . '/control';
        //         $data       = '';
        //         $data      .= "<a href='" . base_url($folder . '/view/' . encrypt_url($id)) . "'><i data-popup='tooltip' title='View Data' class='icon-database-check text-pink-400 mr-1'></i></a>";
        //         $data      .= "<a href='" . base_url($folder . '/update/' . encrypt_url($id)) . "' title='Edit Data'><i class='icon-database-edit2 text-blue-800'></i></a>";
        //         return $data;
        //     });
        // }

        $datatables->add('action', function ($data) {
            $id = $data['id'];
            $folder = '/' . $data['folder'] . '/control';

            $actions = "<a href='" . base_url($folder . '/view/' . encrypt_url($id)) . "'>
                            <i title='View Data' class='ph-eye text-secondary mr-1'></i>
                        </a>";

            $actions .= "<a href='" . base_url($folder . '/update/' . encrypt_url($id)) . "' title='Edit Data'>
                            <i title='Edit Data' class='ph-note-pencil text-pink'></i>
                        </a>";

            return $actions;
        });

        $datatables->hide('folder');
        $datatables->hide('i_menu');

        return $datatables->generate();
    }

    /** Edit Data */
    public function act_update($i_level, $i_menu, $i_sub_menu)
    {
        if ((is_array($i_menu) || is_object($i_menu)) || (is_array($i_sub_menu) || is_object($i_sub_menu))) {
            $builder = $this->db->table($this->table);
            $builder->where('i_level', $i_level);
            $builder->delete();
        }

        if (is_array($i_menu) || is_object($i_menu)) {
            foreach ($i_menu as $menu) {
                $i_menu   = explode("|", $menu)[0];
                $i_access = explode("|", $menu)[1];
                $data = [
                    'i_menu' => $i_menu,
                    'i_level'  => $i_level,
                    'i_access'  => $i_access,
                ];
                $this->db->table($this->table)->insert($data);
                /* $this->db->query("INSERT INTO tm_role (i_menu, i_level, i_access) 
                    VALUES ($i_menu, $i_level, $i_access)
                    ON CONFLICT (i_menu, i_level, i_access) DO UPDATE 
                    SET i_menu = excluded.i_menu, 
                        i_level = excluded.i_level, 
                        i_access = excluded.i_access
                "); */
            }
        }

        if (is_array($i_sub_menu) || is_object($i_sub_menu)) {
            foreach ($i_sub_menu as $submenu) {
                $i_menu  = explode("|", $submenu)[0];
                $i_access = explode("|", $submenu)[1];
                $data = [
                    'i_menu' => $i_menu,
                    'i_level'  => $i_level,
                    'i_access'  => $i_access,
                ];
                $this->db->table($this->table)->insert($data);
                /* $this->db->query("INSERT INTO tm_role (i_menu, i_level, i_access) 
                    VALUES ($i_menu, $i_level, $i_access)
                    ON CONFLICT (i_menu, i_level, i_access) DO UPDATE 
                    SET i_menu = excluded.i_menu, 
                        i_level = excluded.i_level, 
                        i_access = excluded.i_access
                "); */
            }
        }
    }

    /** Get Data Menu */
    public function get_menu($id)
    {
        return $this->db->query("SELECT a.i_menu, a.e_menu, b.i_access, b.e_access_name, 
                CASE
                    WHEN c.i_menu = a.i_menu
                    AND b.i_access = c.i_access THEN 'selected'
                    ELSE ''
                END AS selected
            FROM
                tr_menu a
            INNER JOIN tm_role d ON
                (d.i_menu = a.i_menu)
            INNER JOIN tr_access b ON
                (b.i_access = d.i_access)
            LEFT JOIN (SELECT i_menu, i_access FROM tm_role WHERE i_level = '$id') c ON 
                (c.i_menu = a.i_menu AND b.i_access = c.i_access)
            WHERE
                e_folder = '#'
                AND b.i_access = '2'
                AND d.i_level = (SELECT i_level FROM tr_level WHERE f_super_admin = 't')
            ORDER BY
                a.i_menu,
                n_urut");
    }

    /** Get Data Sub Menu */
    public function get_sub_menu($id)
    {
        return $this->db->query("SELECT a.i_menu, a.e_menu, b.i_access, b.e_access_name, 
                CASE
                    WHEN c.i_menu = a.i_menu
                    AND b.i_access = c.i_access THEN 'selected'
                    ELSE ''
                END AS selected
            FROM tr_menu a
            INNER JOIN tm_role d ON d.i_menu = a.i_menu
            INNER JOIN tr_access b ON b.i_access = d.i_access
            LEFT JOIN (
                        SELECT i_menu, i_access FROM tm_role WHERE i_level = '$id'
                    ) c ON c.i_menu = a.i_menu AND b.i_access = c.i_access
            WHERE e_folder <> '#' AND 
                d.i_level = (SELECT i_level FROM tr_level WHERE f_super_admin = 't')
            ORDER BY
                a.i_menu,
                n_urut,
                i_access");
    }

    /** Get Data */
    public function get_data($id) 
    {
        $sql = "SELECT tm.i_menu::int, tm.e_menu, a.i_access AS idadmin, a.i_access AS id
                FROM tr_menu tm
                LEFT JOIN (
                            SELECT a.i_menu, a.e_menu , string_agg(b.i_access::varchar, ',') AS i_access FROM tr_menu a 
                            left JOIN tm_user_role b ON b.i_menu = a.i_menu  
                            WHERE a.f_status = 't'
                            AND b.i_level = '$id'
                            GROUP BY 1
                ) AS a ON a.i_menu = tm.i_menu
                WHERE tm.f_status = 't' 
                ORDER BY tm.i_menu ASC, tm.n_urut ASC";

        // var_dump($sql); die();

        return $this->db->query($sql);
    }
    
    public function _get_data($id)
    {
        return $this->db->query("SELECT 
                z.i_menu,
                z.e_menu,
                z.id as idadmin,
                a.id
            FROM(
                    SELECT 
                        a.i_menu, a.e_menu, string_agg(b.i_access::varchar,',') AS id
                    FROM tr_menu a
                    JOIN tm_user_role b ON a.i_menu = b.i_menu
                    JOIN tr_level c ON b.i_level = c.i_level
                    JOIN tr_access d ON b.i_access = d.i_access  
                    WHERE b.i_level = '$id' and a.f_status = 't'
                    GROUP BY a.i_menu, a.e_menu, c.i_level
                    ORDER BY a.n_urut
                ) AS z
            LEFT JOIN (
                        SELECT a.i_menu, a.e_menu, string_agg(b.i_access::varchar,',')as id
                        FROM tr_menu a
                        JOIN tm_user_role b ON a.i_menu = b.i_menu
                        JOIN tr_level c ON b.i_level = c.i_level
                        JOIN tr_access d ON b.i_access = d.i_access  
                        WHERE b.i_level ='$id'
                        GROUP BY a.i_menu, a.e_menu, c.i_level
                        ORDER BY a.n_urut
                        ) AS a ON z.i_menu = a.i_menu
            ORDER BY z.i_menu::varchar ASC
        ");
    }

    public function get_access()
    {
        return $this->db->query("SELECT i_access as id, e_access_name as e_name FROM tr_access ORDER BY i_access ASC");
    }

    public function delete_user_role($i_level)
    {
        $builder = $this->db->table($this->table);
        $builder->where('i_level', $i_level);
        $builder->delete();
    }    

    public function insert_user_role($i_menu, $i_level, $i_access)
    {
        $data = [
            'i_menu' => $i_menu,
            'i_level'  => $i_level,
            'i_access'  => $i_access,
        ];
        $this->db->table($this->table)->insert($data);
    }

}
