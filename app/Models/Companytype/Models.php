<?php

namespace App\Models\Companytype;

use CodeIgniter\Model;
use Ozdemir\Datatables\Datatables;
use Ozdemir\Datatables\DB\Codeigniter4Adapter;

class Models extends Model
{
    protected $table = 'tr_company_type';
    protected $primaryKey = 'i_company_type';
    protected $allowedFields = ['e_company_type_name', 'f_active'];

    /** Datatable */
    public function serverside($folder, $i_menu)
    {
        $datatables = new Datatables(new Codeigniter4Adapter);
        $datatables->query("SELECT i_company_type AS id, e_company_type_name, f_active, CASE WHEN d_update ISNULL THEN d_entry ELSE d_update END AS d_update, '$folder' AS folder, '$i_menu' AS i_menu FROM tr_company_type");
        $datatables->edit('f_active', function ($data) {
            $id         = $data['id'];
            $i_menu     = $data['i_menu'];
            $folder     = '/' . $data['folder'] . '/control';
            if ($data['f_active'] == 't') {
                $status = 'Active';
                $color  = 'primary';
            } else {
                $status = 'Not Active';
                $color  = 'pink';
            }
            if (check_role($i_menu, 3)->getnumRows() > 0) {
                $data = "<button class='btn btn-sm badge bg-$color-400 badge-pill' onclick='change_status(\"" . $folder . "\",\"" . encrypt_url($id) . "\");'>$status</button>";
            } else {
                $data = "<span class='badge bg-$color-400 badge-pill'>$status</span>";
            }
            return $data;
        });

        /** Cek Hak Akses, Apakah User Bisa Edit */
        if (check_role($i_menu, 3)->getNumRows() > 0) {
            $datatables->add('action', function ($data) {
                $id         = $data['id'];
                $i_menu     = $data['i_menu'];
                $folder     = '/' . $data['folder'] . '/control';
                $data       = '';
                $data      .= "<a href='" . base_url($folder . '/update/' . encrypt_url($id)) . "' title='Edit Data'><i class='icon-database-edit2 text-pink-400'></i></a>";
                return $data;
            });
        }

        $datatables->hide('folder');
        $datatables->hide('i_menu');
        return $datatables->generate();
    }

    /** Check Already */
    public function check($e_name)
    {
        $builder = $this->db->table($this->table);
        $builder->select('e_company_type_name');
        $builder->where('upper(e_company_type_name)', upper($e_name));
        return $builder->get();
    }

    /** Insert Data */
    public function create($e_name)
    {
        $data = [
            'e_company_type_name' => $e_name,
        ];
        $this->db->table($this->table)->insert($data);
    }

    /** Get Data Edit */
    public function get_edit($id = null)
    {
        return $this->db->table($this->table)->getWhere([$this->primaryKey => $id]);
    }

    /** Check Already Edit */
    public function check_edit($e_name, $e_name_old)
    {
        $builder = $this->db->table($this->table);
        $builder->select('e_company_type_name');
        $builder->where('upper(e_company_type_name)', upper($e_name));
        $builder->where('upper(e_company_type_name) !=', upper($e_name_old));
        return $builder->get();
    }

    /** Edit Data */
    public function act_update($id, $e_name)
    {
        $builder = $this->db->table($this->table);
        $data = [
            'e_company_type_name' => $e_name,
            'd_update' => current_time(),
        ];
        $builder->where($this->primaryKey, $id);
        $builder->update($data);
    }

    /** Change Data */
    public function change_status($id)
    {
        $this->db->query("UPDATE tr_company_type SET f_active = CASE WHEN f_active = 't' THEN FALSE ELSE TRUE END WHERE i_company_type = '$id' ");
    }
}
