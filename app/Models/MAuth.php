<?php

namespace App\Models;

use CodeIgniter\I18n\Time;
use CodeIgniter\Model;
use Ozdemir\Datatables\Datatables;
use Ozdemir\Datatables\DB\Codeigniter4Adapter;

class MAuth extends Model
{

    protected $table = 'tr_user';
    
    public function login($i_user_code, $e_password)
    {
        $builder = $this->db->table($this->table);
        $builder->select('*');
        $builder->where('lower(i_user_code)', strtolower($i_user_code));
        $builder->where('lower(e_user_password)', strtolower($e_password));
        $builder->where('f_active', 't');
        $query = $builder->get();
        if ($query->getNumRows() > 0) {
            return $query->getRowArray();
        }
    }
}
