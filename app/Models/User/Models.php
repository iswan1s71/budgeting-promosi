<?php

namespace App\Models\User;

use CodeIgniter\Model;
use Ozdemir\Datatables\Datatables;
use Ozdemir\Datatables\DB\Codeigniter4Adapter;

class Models extends Model
{
    protected $table = 'public.tr_user';
    protected $primaryKey =  'i_user';
    protected $allowedFields = ['i_user_code', 'e_user_password', 'e_user_name', 'f_active'];

    /** Datatable */
    public function serverside($folder, $i_menu)
    {
        $datatables = new Datatables(new Codeigniter4Adapter);

        $sql = "SELECT tu.i_user AS id, 
                        tu.i_user_code, 
                        string_agg(DISTINCT l.e_level_name, ', ') AS levels, 
                        string_agg(DISTINCT tc.e_company_name, ', ') AS companies,
                        tu.f_active,
                        '$folder' AS folder,
                        '$i_menu' AS i_menu
                FROM tr_user tu 
                INNER JOIN tm_user_company tuc ON tuc.i_user = tu.i_user 
                INNER JOIN tr_company tc ON tc.i_company = tuc.i_company 
                INNER JOIN tm_user_level tul ON tul.i_user = tu.i_user 
                INNER JOIN tr_level l ON l.i_level = tul.i_level
                WHERE tu.i_user_code NOT ILIKE '%admin%'
                GROUP BY tu.i_user";

        $datatables->query($sql);

        /** Cek Hak Akses, Apakah User Bisa Edit */
        // if (check_role($i_menu, 3)->getnumRows() > 0) {
        //     $datatables->add('action', function ($data) {
        //         $id         = $data['id'];
        //         $folder     = '/' . $data['folder'] . '/control';
        //         $data       = '';
        //         $data      .= "<a href='" . base_url($folder . '/view/' . encrypt_url($id)) . "'><i data-popup='tooltip' title='View Data' class='icon-database-check text-pink-400 mr-1'></i></a>";
        //         $data      .= "<a href='" . base_url($folder . '/update/' . encrypt_url($id)) . "' title='Edit Data'><i class='icon-database-edit2 text-blue-800'></i></a>";
        //         return $data;
        //     });
        // }

        $datatables->edit('f_active', function ($data) use($folder) {
            $id = $data['id'];
            $status = $data['f_active'];

            $class = 'success';
            $text = 'Active';
            if ($status == 'f') {
                $class = 'danger';
                $text = 'Inactive';
            }
            
            $button = "<a href='" . base_url($folder . '/control/toggle_status?id=' . encrypt_url($id)) . "' class='btn btn-$class btn-sm btn-toggle-status'>
                            $text
                        </a>";
            
            return $button;
        });

        $datatables->add('action', function ($data) {
            $id = $data['id'];
            $folder = '/' . $data['folder'] . '/control';

            $actions = "<a href='" . base_url($folder . '/view?id=' . encrypt_url($id)) . "'>
                            <i title='View Data' class='ph-eye text-secondary mr-1'></i>
                        </a>";

            $actions .= "<a href='" . base_url($folder . '/update?id=' . encrypt_url($id)) . "' title='Edit Data'>
                            <i title='Edit Data' class='ph-note-pencil text-pink'></i>
                        </a>";

            // $actions .= "<a href='" . base_url($folder . '/delete?id=' . encrypt_url($id)) . "' title='Delete Data'>
            //                 <i title='Delete Data' class='ph-trash text-danger'></i>
            //             </a>";

            return $actions;
        });

        $datatables->hide('folder');
        $datatables->hide('i_menu');

        return $datatables->generate();
    }

    /** Check Already */
    public function check($iduser)
    {
        $builder = $this->db->table($this->table);
        $builder->select('i_user');
        $builder->where('i_user', $iduser);
        return $builder->get();
    }

    /** Insert Data */
    public function create($iduser, $username, $password, $nama)
    {
        $data = [
            'i_user' => $iduser,
            'i_user_code' => $username,
            'e_user_password' => $password,
            'e_user_name' => $nama,
        ];
        $this->db->table($this->table)->insert($data);
        /* if ($i_access) {
            foreach ($i_access as $access) {
                $data = array(
                    'i_menu'    => $i_menu,
                    'i_level'   => $i_level,
                    'i_access'  => $access,
                );
                $this->db->table('tm_role')->insert($data);
            }
        } */
    }

    /** Get Data Edit */
    public function get_edit($id)
    {
        $sql = "SELECT a.i_user, a.i_user_code, a.e_user_name, a.i_level, l.e_level_name, a.f_active, a.e_user_password
        FROM tr_user a
        LEFT JOIN tr_level l on l.i_level = a.i_level
        WHERE a.i_user = '$id'";

        return $this->db->query($sql);
    }

    /** Get Data Access Edit */
    public function get_access_edit($id)
    {
        $i_level = _models()->get_super_admin()->getRow()->i_level;
        return $this->db->query("SELECT a.*, b.selected FROM tr_access a LEFT JOIN (SELECT i_access, 'selected' AS selected FROM tm_role WHERE i_menu = '$id' AND i_level = '$i_level') b ON (b.i_access = a.i_access)");
    }

    /** Check Already Edit */
    public function check_edit($e_name, $e_name_old)
    {
        $builder = $this->db->table($this->table);
        $builder->select('e_menu');
        $builder->where('upper(e_menu)', upper($e_name_old));
        $builder->where('upper(e_menu) !=', upper($e_name_old));
        return $builder->get();
    }

    /** Edit Data */
    public function act_update($i_user, $i_user_code, $password, $f_active)
    {
        $builder = $this->db->table($this->table);
        $data = [
            'i_user_code' => $i_user_code,
            'e_user_password'  => $password,
            'f_active' => $f_active
        ];
        $builder->where($this->primaryKey, $i_user);
        $builder->update($data);
    }

    /** Change Data */
    public function toggle_status($id)
    {
        $this->db->query("UPDATE $this->table SET f_active = CASE WHEN f_active = 't' THEN FALSE ELSE TRUE END WHERE i_user = '$id' ");
    }

    public function act_soft_delete($id)
    {
        $builder = $this->db->table($this->table);
        $builder->where($this->primaryKey, $id);
        $builder->update(['f_status' => false]);
    }

    /** Delete Data */
    public function act_delete($id)
    {
        $builder = $this->db->table($this->table);
        $builder->where($this->primaryKey, $id);
        $builder->delete();
    }

    public static function get_hashed_old_password($i_user)
    {
        $model = new Models();
        $user = $model->get_edit($i_user)->getRow();

        return $user->e_user_password;
    }

}
