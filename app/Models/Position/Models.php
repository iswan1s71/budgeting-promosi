<?php

namespace App\Models\Position;

use CodeIgniter\Model;
use Ozdemir\Datatables\Datatables;
use Ozdemir\Datatables\DB\Codeigniter4Adapter;

class Models extends Model
{
    protected $table = 'tr_position';
    protected $primaryKey = 'i_position';
    protected $allowedFields = ['i_position_code', 'e_position_name', 'f_support'];

    /** Datatable */
    public function serverside($folder, $i_menu)
    {
        $datatables = new Datatables(new Codeigniter4Adapter);
        $datatables->query("SELECT a.i_position AS id, a.i_position_code, e_position_name, b.e_level_position_name, d.e_department_name, 
            c.e_division_name, c.f_support, a.f_active, CASE WHEN a.d_update ISNULL THEN a.d_entry END AS d_update, '$folder' AS folder, '$i_menu' AS i_menu
            FROM tr_position a
            INNER JOIN tr_level_position b ON (b.i_level_position = a.i_level_position)
            INNER JOIN tr_division c ON (c.i_division = a.i_division)
            INNER JOIN tr_department d ON (d.i_department = a.i_department)");        
        $datatables->edit('f_support', function ($data) {
            if ($data['f_support'] == 't') {
                $status = 'Yes';
                $color  = 'success';
            } else {
                $status = 'No';
                $color  = 'indigo';
            }
            $data = "<span class='badge bg-$color-400 badge-pill'>$status</span>";
            return $data;
        });
        
        
        $datatables->edit('f_active', function ($data) {
            $id         = $data['id'];
            $i_menu     = $data['i_menu'];
            $folder     = '/' . $data['folder'] . '/control';
            if ($data['f_active'] == 't') {
                $status = 'Active';
                $color  = 'primary';
            } else {
                $status = 'Not Active';
                $color  = 'pink';
            }
            if (check_role($i_menu, 3)->getnumRows() > 0) {
                $data = "<button class='btn btn-sm badge bg-$color-400 badge-pill' onclick='change_status(\"" . $folder . "\",\"" . encrypt_url($id) . "\");'>$status</button>";
            } else {
                $data = "<span class='badge bg-$color-400 badge-pill'>$status</span>";
            }
            return $data;
        });        

        /** Cek Hak Akses, Apakah User Bisa Edit */
        if (check_role($i_menu, 3)->getnumRows() > 0) {
            $datatables->add('action', function ($data) {
                $id         = $data['id'];
                $i_menu     = $data['i_menu'];
                $folder     = '/' . $data['folder'] . '/control';
                $data       = '';
                $data      .= "<a href='" . base_url($folder . '/update/' . encrypt_url($id)) . "' title='Edit Data'><i class='icon-database-edit2 text-pink-400'></i></a>";
                return $data;
            });
        }

        $datatables->hide('folder');
        $datatables->hide('i_menu');
        return $datatables->generate();
    }

    /** Search Department */
    public function get_department($cari)
    {
        $builder = $this->db->table('tr_department');
        $builder->select('i_department,  i_department_code, e_department_name');
        $builder->where("(i_department_code ILIKE '%".$cari."%' OR e_department_name ILIKE '%".$cari."%')", NULL, FALSE);
        return $builder->get();
    }
    
    /** Search Division */
    public function get_division($cari, $i_department)
    {
        $builder = $this->db->table('tr_division');
        $builder->select('i_division,  i_division_code, e_division_name');
        $builder->where('i_department', $i_department);
        $builder->where("(i_division_code ILIKE '%".$cari."%' OR e_division_name ILIKE '%".$cari."%')", NULL, FALSE);
        return $builder->get();
    }
    
    /** Search Level Position */
    public function get_level_position($cari)
    {
        $builder = $this->db->table('tr_level_position');
        $builder->select('i_level_position,  e_level_position_name');
        $builder->like('lower(e_level_position_name)', lower($cari));
        return $builder->get();
    }

    /** Check Already */
    public function check($i_code)
    {
        $builder = $this->db->table($this->table);
        $builder->select('i_position_code');
        $builder->where('upper(i_position_code)', upper($i_code));
        return $builder->get();
    }

    /** Insert Data */
    public function create($i_department, $i_division, $i_code, $e_name, $i_level_position)
    {
        $data = [
            'i_position_code'   => $i_code,
            'e_position_name'   => $e_name,
            'i_level_position'  => $i_level_position,
            'i_department'      => $i_department,
            'i_division'        => $i_division,
        ];
        $this->db->table($this->table)->insert($data);
    }

    /** Get Data Edit */
    public function get_edit($id)
    {
        /* return $this->db->table($this->table)->getWhere([$this->primaryKey => $id]); */
        $builder = $this->db->table($this->table . ' a');
        $builder->select('a.*, b.i_department_code, b.e_department_name, c.i_division_code, c.e_division_name, d.e_level_position_name');
        $builder->join('tr_department b', 'b.i_department = a.i_department', 'inner');
        $builder->join('tr_division c', 'c.i_division = a.i_department', 'inner');
        $builder->join('tr_level_position d', 'd.i_level_position = a.i_level_position', 'inner');
        $builder->where('a.' . $this->primaryKey, $id);
        return $builder->get();
    }

    /** Check Already Edit */
    public function check_edit($i_code, $i_code_old)
    {
        $builder = $this->db->table($this->table);
        $builder->select('i_position_code');
        $builder->where('upper(i_position_code)', upper($i_code));
        $builder->where('upper(i_position_code) !=', upper($i_code_old));
        return $builder->get();
    }

    /** Edit Data */
    public function act_update($id, $i_department, $i_division, $i_code, $e_name, $i_level_position)
    {
        $builder = $this->db->table($this->table);
        $data = [
            'i_position_code'   => $i_code,
            'e_position_name'   => $e_name,
            'i_level_position'  => $i_level_position,
            'i_department'      => $i_department,
            'i_division'        => $i_division,
            'd_update'          => current_time(),
        ];
        $builder->where($this->primaryKey, $id);
        $builder->update($data);
    }

    /** Change Data */
    public function change_status($id)
    {
        $this->db->query("UPDATE tr_position SET f_active = CASE WHEN f_active = 't' THEN FALSE ELSE TRUE END WHERE i_position = '$id' ");
    }
}
