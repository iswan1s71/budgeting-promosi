<?php

namespace App\Models\Userlevel;

use CodeIgniter\Model;
use Ozdemir\Datatables\Datatables;
use Ozdemir\Datatables\DB\Codeigniter4Adapter;

class Models extends Model
{
    protected $table = 'tm_user_level';
    protected $primaryKey = 'i_user_level';
    protected $allowedFields = ['i_user', 'i_level'];

    /** Insert Data */
    public function create($i_user, $i_level)
    {
        $data = [
            'i_user' => $i_user,
            'i_level' => $i_level,
        ];
        $this->db->table($this->table)->insert($data);
    }

    /** Get Data Edit */
    public function get_edit($id = null)
    {
        return $this->db->table($this->table)->getWhere([$this->primaryKey => $id]);
    }

    /** Check Already Edit */
    public function check_edit($e_name, $e_name_old)
    {
        $builder = $this->db->table($this->table);
        $builder->select('e_company_type_name');
        $builder->where('upper(e_company_type_name)', upper($e_name));
        $builder->where('upper(e_company_type_name) !=', upper($e_name_old));
        return $builder->get();
    }

    /** Edit Data */
    public function act_update($id, $e_name)
    {
        $builder = $this->db->table($this->table);
        $data = [
            'e_company_type_name' => $e_name,
            'd_update' => current_time(),
        ];
        $builder->where($this->primaryKey, $id);
        $builder->update($data);
    }

    /** Change Data */
    public function change_status($id)
    {
        $this->db->query("UPDATE tr_company_type SET f_active = CASE WHEN f_active = 't' THEN FALSE ELSE TRUE END WHERE i_level_type = '$id' ");
    }

    public function get_all($q='')
    {
        $builder = $this->db->table($this->table);
        $builder->select('*');
        $builder->where('f_active', true);

        if ($q != '') {
            $builder->like('lower(e_company_name)', lower($q));
        }

        return $builder->get();
    }

    public function get_by_i_user($i_user)
    {
        $sql = "SELECT * FROM tm_user_level WHERE i_user::TEXT = '$i_user'";

        return $this->db->query($sql);
    }

    public function delete_by_i_user($i_user)
    {
        $sql = "DELETE FROM tm_user_level WHERE i_user::TEXT = '$i_user'";

        return $this->db->query($sql);
    }

    public static function get_list($asArray=false, $q='')
    {
        $model = new Models();
        $query = $model->get_all($q);

        if ($asArray) {
            $array = [];

            foreach ($query->getResult() as $result) {
                $_array = [
                    'id' => $result->i_level,
                    'text' => $result->e_company_name
                ];

                $array[] = $_array;
            }

            return json_encode($array);
        }

        return $query;
    }

    public static function get_by_id($asArray=false, $id='')
    {
        $model = new Models();
        $query = $model->get_by_i_user($id);

        if ($asArray) {
            $array = [];

            foreach ($query->getResult() as $result) {
                $_array = [
                    'id' => $result->i_level,
                    'text' => $result->e_company_name
                ];

                $array[] = $_array;
            }

            return json_encode($array);
        }

        return $query;
    }

}

