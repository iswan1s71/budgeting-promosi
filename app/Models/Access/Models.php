<?php

namespace App\Models\Access;
use CodeIgniter\Model;
use Ozdemir\Datatables\Datatables;
use Ozdemir\Datatables\DB\Codeigniter4Adapter;

class Models extends Model
{
    /** Datatable */
    public function serverside()
    {
        $datatables = new Datatables(new Codeigniter4Adapter);
        $datatables->query("SELECT i_access, e_access_name FROM tr_access");
        return $datatables->generate();
    }
}
