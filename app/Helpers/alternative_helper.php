<?php

function log_writer($e){
	log_message('error', '[ERROR] {exception}', ['exception' => $e]);
	die($e->getMessage() . ' <br> ' . $e->getFile() . ' Line ' . $e->getLine());
}

function _template($target = '', $array = array()){

	$data = array_merge(['page' => $target], $array);

	echo view('auth', $data);
}

function _view($target, $data = array()){

	echo view($target, $data);
}

function getSegment($segment){
	$request = \Config\Services::request();

	if($segment > $request->uri->getTotalSegments() ) {
		return null;
	}else{
		return $request->uri->getSegment($segment);
	}

}

function module(){
	return getSegment(1);
}

function encode($string){
	return base64_encode($string);
}

function decode($string){
	return base64_decode($string);
}

