<?php
 
namespace App\Helpers;

class Session {

	public static function getId($primaryKey=null)
	{
		$key = 'i_user';
		if ($primaryKey != null) {
			$key = $primaryKey;
		}

		$session = \Config\Services::session();
		return $session->get($key) ?? null;
	}	

 }

