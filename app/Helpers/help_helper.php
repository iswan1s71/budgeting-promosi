<?php

/* use CodeIgniter\Config\BaseConfig; */

use CodeIgniter\I18n\Time;

function _models()
{
	return new \App\Models\Mmaster();
}

function _url()
{
	return new \CodeIgniter\HTTP\URI();
}

function encrypt_password($string)
{
	$output 		= false;
	$secret_key     = 'merubahpassword';
	$secret_iv      = 'menjadipassword';
	$encrypt_method = 'aes-256-cbc';
	$key    		= hash("sha256", $secret_key);
	$iv     		= substr(hash("sha256", $secret_iv), 0, 16);
	$result 		= openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
	$output 		= base64_encode($result);
	$output 		= str_replace('=', '', $output);
	return $output;
}

function decrypt_password($string)
{
	$output 		= false;
	$secret_key     = 'merubahpassword';
	$secret_iv      = 'menjadipassword';
	$encrypt_method = 'aes-256-cbc';
	$key    		= hash("sha256", $secret_key);
	$iv 			= substr(hash("sha256", $secret_iv), 0, 16);
	$output 		= openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
	return $output;
}

function thismenu()
{
	return _url()->getSegment(2);
}

function get_menu()
{
	$i_user = session('i_user');
	$i_level = session('i_level');
	return _models()->get_menu($i_user, $i_level);
}

function get_sub_menu($i_menu)
{
	$i_user = session('i_user');
	$i_department = session('i_department');
	$i_level = session('i_level');
	return _models()->get_sub_menu($i_user, $i_menu, $i_department, $i_level);
}

function check_folder($e_folder, $i_access)
{
	$i_user = session('i_user');
	// $i_department = session('i_department');
	$i_level = session('i_level');
	return _models()->check_folder($i_user, $e_folder, $i_access, $i_department=null, $i_level);
}

function check_role($i_menu, $i_access)
{
	$i_user = session('i_user');
	$i_department = session('i_department');
	$i_level = session('i_level');
	return _models()->check_role($i_user, $i_menu, $i_access, $i_department, $i_level);
}

function get_department()
{
	$i_user = session('i_user');
	return _models()->get_department($i_user);
}

function get_level()
{
	$i_user = session('i_user');
	$i_department = session('i_department');
	return _models()->get_level($i_user, $i_department);
}

function check_level($i_level)
{
	$i_user = session('i_user');
	$i_department = session('i_department');
	return _models()->check_level($i_user, $i_department, $i_level);
}

function add_js($file = '')
{
	$str = '';
	$footer_js  = config(\Config\App::class)->footer_js;

	if (empty($file)) {
		return;
	}

	if (is_array($file)) {
		if (!is_array($file) && count($file) <= 0) {
			return;
		}
		foreach ($file as $item) {
			$footer_js[] = $item;
		}
		config(\Config\App::class)->footer_js = $footer_js;
	} else {
		$str = $file;
		$footer_js[] = $str;
		config(\Config\App::class)->footer_js = $footer_js;
	}
}

function put_footer()
{
	$str = '';
	$footer_js  = config(\Config\App::class)->footer_js;
	foreach ($footer_js as $item) {
		$str .= '<script src="' . base_url($item) . '"></script>' . "\n";
	}
	return $str;
}

function add_css($file = '')
{
	$str = '';
	$header_css = config(\Config\App::class)->header_css;

	if (empty($file)) {
		return;
	}

	if (is_array($file)) {
		if (!is_array($file) && count($file) <= 0) {
			return;
		}
		foreach ($file as $item) {
			$header_css[] = $item;
		}
		config(\Config\App::class)->header_css = $header_css;
	} else {
		$str = $file;
		$header_css[] = $str;
		config(\Config\App::class)->header_css = $header_css;
	}
}

function put_headers()
{
	$str = '';
	$header_css  = config(\Config\App::class)->header_css;
	if ($header_css) {
		foreach ($header_css as $item) {
			$str .= '<link href="' . base_url($item) . '" rel="stylesheet" type="text/css"/>' . "\n";
		}
	}
	return $str;
}

function _view($folder, $file, $data = '')
{
	if ($folder == '') {
		echo view('home');
	} else {
		echo view($folder . $file, $data);
	}
}

function encrypt_url($string)
{
	$output = false;
	$secret_key = 'merubahlinkmenjadiencrypt';
	$secret_iv = 'merubahlinkmenjadidecrypt';
	$encrypt_method = 'aes-256-cbc';
	$key = hash("sha256", $secret_key);
	$iv = substr(hash("sha256", $secret_iv), 0, 16);
	$result = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
	$output = base64_encode($result);
	$output = str_replace('=', '', $output);
	return $output;
}

function decrypt_url($string)
{
	$output = false;
	$secret_key = 'merubahlinkmenjadiencrypt';
	$secret_iv = 'merubahlinkmenjadidecrypt';
	$encrypt_method = 'aes-256-cbc';
	$key = hash("sha256", $secret_key);
	$iv = substr(hash("sha256", $secret_iv), 0, 16);
	$output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
	return $output;
}

function bulan($a)
{
	if ($a == '') $b = '';
	if ($a == '01') $b = 'Januari';
	if ($a == '02') $b = 'Februari';
	if ($a == '03') $b = 'Maret';
	if ($a == '04') $b = 'April';
	if ($a == '05') $b = 'Mei';
	if ($a == '06') $b = 'Juni';
	if ($a == '07') $b = 'Juli';
	if ($a == '08') $b = 'Agustus';
	if ($a == '09') $b = 'September';
	if ($a == '10') $b = 'Oktober';
	if ($a == '11') $b = 'November';
	if ($a == '12') $b = 'Desember';
	return $b;
}

function log_writer($e)
{
	log_message('error', '[ERROR] {exception}', ['exception' => $e]);
	die($e->getMessage() . ' <br> ' . $e->getFile() . ' Line ' . $e->getLine());
}

function _template($target = '', $array = array())
{

	$data = array_merge(['page' => $target], $array);

	echo view('auth', $data);
}

function getSegment($segment)
{
	$request = \Config\Services::request();

	if ($segment > $request->uri->getTotalSegments()) {
		return null;
	} else {
		return $request->uri->getSegment($segment);
	}
}

function module()
{
	return getSegment(1);
}

function encode($string)
{
	return base64_encode($string);
}

function decode($string)
{
	return base64_decode($string);
}

function Ymd($date)
{
	return ($date != '') ? date('Y-m-d', strtotime($date)) : null;
}

function dmY($date)
{
	return ($date != '') ? date('d-m-Y', strtotime($date)) : null;
}

function getPeriode($date)
{
	return ($date != '') ? date('Ym', strtotime($date)) : null;
}

function getPeriodesm($date)
{
	return ($date != '') ? date('ym', strtotime($date)) : null;
}

function now()
{
	return date('Y-m-d H:i:s');
}

function no_comma($string)
{
	$string = str_replace(',', '', $string);
	return $string;
}

function no_petik($string)
{
	return str_replace("'", "", $string);
}

function no_space($string)
{
	return str_replace(" ", "", $string);
}

function capitalize($string)
{
	return ucwords(strtolower($string));
}

function upper($string)
{
	return strtoupper($string);
}

function lower($string)
{
	return strtolower($string);
}

function current_time()
{
	return Time::now('Asia/Jakarta', 'id');
}

function breadcrumb_title()
{
	$folder = getSegment(1);
	if ($folder == null || $folder == '') {
		return 'Home';
	} else {
		$query = _models()->get_breadcrumb_title($folder);
		if ($query->getNumRows() > 0) {
			return $query->getRow()->e_menu;
		} else {
			return 'Home';
		}
	}
}

function breadcrumb()
{
	$folder = getSegment(1);
	return  _models()->get_breadcrumb($folder);
}

function breadcrumb_head()
{
	$folder = getSegment(3);
	if ($folder == null || $folder == '') {
		return 'List';
	} else {
		return capitalize($folder);
	}
}
