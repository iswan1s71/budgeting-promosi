<?php
 
namespace App\Helpers;

class Helper {

	public static function encrypt_password($string)
	{
		$output 		= false;
		$secret_key     = 'merubahpassword';
		$secret_iv      = 'menjadipassword';
		$encrypt_method = 'aes-256-cbc';
		$key    		= hash("sha256", $secret_key);
		$iv     		= substr(hash("sha256", $secret_iv), 0, 16);
		$result 		= openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
		$output 		= base64_encode($result);
		$output 		= str_replace('=', '', $output);
		return $output;
	}
	
	public static function decrypt_password($string)
	{
		$output 		= false;
		$secret_key     = 'merubahpassword';
		$secret_iv      = 'menjadipassword';
		$encrypt_method = 'aes-256-cbc';
		$key    		= hash("sha256", $secret_key);
		$iv 			= substr(hash("sha256", $secret_iv), 0, 16);
		$output 		= openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
		return $output;
	}
	

 }

