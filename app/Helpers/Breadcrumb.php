<?php 

namespace App\Helpers;

class Breadcrumb {

    protected $template;
    protected $controllerName;
    
    public $data = array();

    /** @$uri Codeigniter Service('uri') */
    public $uri;
    public $segments;

    public function buildTemplate()
    {
        $breadcrumbItem = [];

        $count = 1;
        foreach ($this->segments as $segment) {
            
            foreach ($segment as $key => $value) {
                $html = "<a href='$value' class='breadcrumb-item'>". ucwords($key) . "</a>";

                if ($count == count($this->segments)) {
                    $html = "<span class='breadcrumb-item active'>". ucwords($key) ."</span>";
                }
    
                $breadcrumbItem[] = $html;
            }

            $count++;
        }        

        $elements = join("", $breadcrumbItem);

        $this->template = '<div class="page-header page-header-content d-lg-flex">
                                <div class="page-title">
                                    <h5 class="mb-0">' . ucwords($this->controllerName) . '</h5>
                                </div>
                            
                                <div class="mb-3 my-lg-auto ms-lg-auto">
                                    <div class="breadcrumb">'. $elements .'</div>
                                </div>
                            </div>';
    }

    public function getHtml()
    {
        if ($this->template == null) {
            $this->init();
        }

        return $this->template;
    }

    public function init()
    {
        if ($this->uri == null) {
            return null;
        }

        foreach ($this->uri->getSegments() as $segment) {
            $_segment = [
                $segment => '/' . $segment
            ];

            $this->segments[] = $_segment;
        }

        $this->controllerName = array_key_first($this->segments[0]);
        $this->buildTemplate();
    }

    public static function get($uri)
    {
        $breadcrumb = new Breadcrumb();
        $breadcrumb->uri = $uri;
        $html = $breadcrumb->getHtml();

        return $html;
    }



}

?>