<?= $this->extend('template/page_layout') ?>

<?= $this->section('content') ?>
<!-- Content -->
<div class="card shadow-lg">
    <div class="card-header">
        <h5 class="mb-0"><?= lang('App.' . $title); ?></h5>
    </div>

    <!-- <div class="card-body">
        This example shows a single button that is specified using the <code>buttons.buttons.text</code> and <code>buttons.buttons.action</code> parameters only - it simply shows an alert when activated, but any Javascript function could be run when the button is activated. All parameters are optional, and each plug-in button type can also specify its own parameters. Also this example shows how buttons can have optional classes.
    </div> -->
    <div class="card-body">
        <table class="table datatable-basic table-bordered table-striped table-hover table-xs" id="serverside">
            <input type="hidden" id="path" value="<?= $folder; ?>">
            <?php if (check_role($i_menu, 1)->getnumRows() > 0) {
                $menu = $i_menu;
            } else {
                $menu = "";
            } ?>
            <input type="hidden" id="i_menu" value="<?= $menu; ?>">
            <thead>
                <tr class="table-border-double">
                    <th>#</th>
                    <th><?= lang('App.Access'); ?></th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>
<!-- /Content -->
<?= $this->endSection() ?>