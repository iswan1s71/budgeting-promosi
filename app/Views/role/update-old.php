<?= $this->extend('template/page_layout') ?>

<?= $this->section('content') ?>
<!-- Content -->

<form class="form-validate-jquery">
    <div class="row">
        <div class="col-md-12">

            
            <!-- Basic layout-->
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title"><i class="icon-file-check mr-2"></i><?= lang('App.' . $title); ?></h5>
                    <div class="header-elements">
                        <div class="list-icons">
                            <a href="#" hidden class="list-icons-item animation" data-animation="fadeInLeft"><i class="icon-play3"></i></a>
                            <a class="list-icons-item" data-action="collapse"></a>
                            <a class="list-icons-item" data-action="reload"></a>
                            <a class="list-icons-item" data-action="remove"></a>
                        </div>
                    </div>
                </div>
                <hr class="mt-0 mb-0">
                <div class="card-body">
                    <input type="hidden" id="path" value="<?= $folder; ?>">
                    <div class="form-group">
                        <label><?= lang('App.Level Name'); ?> :</label>
                        <input type="hidden" name="i_level" value="<?php if ($level) { echo $level->i_level;}?>" required>
                        <input type="text" readonly name="e_level_name" value="<?php if ($level) { echo $level->e_level_name;}?>" autofocus class="form-control text-capitalize" placeholder="<?= lang('App.Level Name'); ?>" required>
                    </div>
                    <div class="form-group">
                        <label><?= lang('App.Role Access Menu'); ?> :</label>
                        <select multiple="multiple" name="i_menu[]" class="form-control listbox-no-selection" required data-fouc>
                        <?php /* if ($data->getNumRows() > 0) {
                            foreach ($data->getResult() as $key) { ?>
                                <option value="<?= $key->i_menu . '|' . $key->i_access; ?>" <?= $key->selected;?>><?= $key->i_menu . ' - ' . $key->e_menu . ' [' . $key->e_access_name . ']'; ?></option>
                        <?php }
                        } */ ?>
                        </select>
                    </div>
                </div>
            </div>
            <!-- /basic layout -->
            

            <?php /*
            <!-- Basic layout-->
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title"><i class="icon-file-check mr-2"></i><?= lang('App.' . $title); ?></h5>
                    <div class="header-elements">
                        <div class="list-icons">
                            <a class="list-icons-item" data-action="collapse"></a>
                            <a class="list-icons-item" data-action="reload"></a>
                            <a class="list-icons-item" data-action="remove"></a>
                        </div>
                    </div>
                </div>
                <hr class="mt-0 mb-0">
                <div class="card-body">
                    <div class="form-group">
                        <label><?= lang('App.Role Access Sub Menu'); ?> :</label>
                        <select multiple="multiple" name="i_sub_menu[]" class="form-control listbox-no-selection" required data-fouc>
                            <?php if ($submenu->getNumRows() > 0) {
                                foreach ($submenu->getResult() as $key) { ?>
                                    <option value="<?= $key->i_menu . '|' . $key->i_access; ?>" <?= $key->selected;?>><?= $key->i_menu . ' - ' . $key->e_menu . ' [' . $key->e_access_name . ']'; ?></option>
                            <?php }
                            } ?>
                        </select>
                    </div>
                    <div>
                        <button type="button" class="btn bg-slate-800 btn-sm submit"><?= lang('App.Submit'); ?> <i class="icon-paperplane ml-2"></i></button>
                        <a href="<?= base_url($folder . '/control'); ?>" type="reset" class="btn bg-pink-400 btn-sm ml-2" id="reset"><?= lang('App.Back'); ?> <i class="icon-reload-alt ml-2"></i></a>
                    </div>
                </div>
            </div>
            <!-- /basic layout -->
            */ ?>

        </div>
    </div>
</form>

<!-- Content -->
<?= $this->endSection() ?>