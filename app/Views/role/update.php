<?= $this->extend('template/page_layout') ?>

<?= $this->section('content') ?>
<!-- Content -->

<form class="form-validate-jquery">
    <input type="hidden" id="path" value="<?= $folder; ?>">
    <input type="hidden" name="i_level" value="<?= $id; ?>">
    <div class="row">
        <div class="col-md-12">

            <!-- Basic layout-->
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title"><i class="icon-file-check mr-2"></i><?= lang('App.' . $title); ?> <?php if ($level) { ?> <?= $level->e_level_name; ?><?php } ?></h5>
                    <div class="header-elements">
                        <div class="list-icons">
                            <a href="#" hidden class="list-icons-item animation" data-animation="fadeInRight"><i class="icon-play3"></i></a>
                            <a class="list-icons-item" data-action="collapse"></a>
                            <a class="list-icons-item" data-action="reload"></a>
                            <a class="list-icons-item" data-action="remove"></a>
                        </div>
                    </div>
                </div>
                <hr class="mt-0 mb-0">
                <div class="card-body">
                    <div class="form-group">
                        <div class="table-responsive">
                            <table class="table datatable-basic table-bordered table-striped table-hover table-xs dataTable no-footer">
                                <thead>
                                    <tr>
                                        <th width="4%" class="text-center">#</th>
                                        <th><?= lang('App.Menu Code');?></th>
                                        <th><?= lang('App.Menu Name');?></th>
                                        <th><?= lang('App.Menu Access');?></th>
                                        <th><input type="checkbox" class="form-check-input-styled-slate" id="cb-grant-all-row"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 0;
                                    foreach ($data->getResult() as $row) { $i++; ?>
                                        <?php $i_menu = $row->i_menu ?>
                                        <tr>
                                            <td><?= $i ?></td>
                                            <td><?= $i_menu ?></td>
                                            <td><?= $row->e_menu ?></td>
                                            <td>
                                                <div class="row d-flex">
                                                <?php foreach ($access->getResult() as $key) {
                                                        $hidden = '';
                                                        // if ($row->idadmin != null and (
                                                        //     strpos($row->idadmin, $key->id) === false
                                                        // )) {
                                                        //     $hidden   = 'hidden';
                                                        // } 

                                                        $checked = '';
                                                        if ($key->id != null and (
                                                            strpos($row->id, $key->id) !== false
                                                        )) {
                                                            $checked = 'checked';
                                                        } 
                                                        
                                                        ?>

                                                        <div class="col" style="align-self: center;">
                                                            <div class="form-check-inline"><label class="form-check-label" <?= $hidden ?>>
                                                                <input type="checkbox" name="access[<?= $i_menu ?>][<?= $key->id ?>]" class="form-check-input-styled-slate" <?= $checked ?> value="on">
                                                                <span class="custom-control-indicator"></span>
                                                                <span class="custom-control-description"><?= $key->e_name; ?></span>
                                                            </div>
                                                        </div>
                                                <?php } ?>
                                                </div>
                                            </td>
                                            <td><input type="checkbox" class="form-check-input-styled-slate cb-grant-row"></td>
                                        </tr>
                                    <?php } ?>
                                    <input type="hidden" name="jml" id="jml" value="<?= $i; ?>">
                                </tbody>
                            </table>
                        </div>
                    </div>                    
                </div>                
            </div>
            <!-- /basic layout -->
            <div>
                <a href="<?= base_url($folder . '/control'); ?>" type="reset" class="btn btn-sm btn-secondary" id="reset"><?= lang('App.Back'); ?> <i class="icon-reload-alt ml-2"></i></a>
                <button type="button" class="btn btn-sm btn-pink submit"><?= lang('App.Submit'); ?> <i class="icon-paperplane ml-2"></i></button>
            </div>

        </div>
    </div>
</form>

<!-- Content -->
<?= $this->endSection() ?>