<?= $this->extend('template/page_layout') ?>

<?= $this->section('content') ?>
<!-- Content -->
<div class="row">
    <div class="col-lg-4">
        <div class="card">
            <div class="card-header d-flex border-bottom-0 pb-1 mb-0">
                <div>
                    <span class="fw-medium mb-1">Total visitors</span>
                    <h2 class="fw-bold mb-0">5,274 <small class="text-success fs-base fw-normal ms-2">+ 3.6%</small></h2>
                </div>

                <div class="dropdown ms-auto">
                    <button type="button" data-bs-toggle="dropdown" class="btn btn-outline-light btn-icon btn-sm text-body border-transparent rounded-pill">
                        <i class="ph-dots-three-vertical"></i>
                    </button>

                    <div class="dropdown-menu dropdown-menu-end">
                        <a href="#" class="dropdown-item">
                            <i class="ph-eye me-2"></i>
                            View report
                        </a>
                        <a href="#" class="dropdown-item">
                            <i class="ph-printer me-2"></i>
                            Print report
                        </a>
                        <a href="#" class="dropdown-item">
                            <i class="ph-file-pdf me-2"></i>
                            Export report
                        </a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item">
                            <i class="ph-gear me-2"></i>
                            Configure
                        </a>
                    </div>
                </div>
            </div>

            <div class="chart-container">
                <div class="chart" id="area_gradient_blue" style="height: 100px"></div>
            </div>
        </div>
    </div>

    <div class="col-lg-4">
        <div class="card">
            <div class="card-header d-flex border-bottom-0 pb-1 mb-0">
                <div>
                    <span class="fw-medium mb-1">New opportunities</span>
                    <h2 class="fw-bold mb-0">2,785 <small class="text-success fs-base fw-normal ms-2">+ 5.9%</small></h2>
                </div>

                <div class="dropdown ms-auto">
                    <button type="button" data-bs-toggle="dropdown" class="btn btn-outline-light btn-icon btn-sm text-body border-transparent rounded-pill">
                        <i class="ph-dots-three-vertical"></i>
                    </button>

                    <div class="dropdown-menu dropdown-menu-end">
                        <a href="#" class="dropdown-item">
                            <i class="ph-eye me-2"></i>
                            View report
                        </a>
                        <a href="#" class="dropdown-item">
                            <i class="ph-printer me-2"></i>
                            Print report
                        </a>
                        <a href="#" class="dropdown-item">
                            <i class="ph-file-pdf me-2"></i>
                            Export report
                        </a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item">
                            <i class="ph-gear me-2"></i>
                            Configure
                        </a>
                    </div>
                </div>
            </div>

            <div class="chart-container">
                <div class="chart" id="area_gradient_orange" style="height: 100px"></div>
            </div>
        </div>
    </div>

    <div class="col-lg-4">
        <div class="card">
            <div class="card-header d-flex border-bottom-0 pb-1 mb-0">
                <div>
                    <span class="fw-medium mb-1">New leads</span>
                    <h2 class="fw-bold mb-0">1,589 <small class="text-danger fs-base fw-normal ms-2">- 1.8%</small></h2>
                </div>

                <div class="dropdown ms-auto">
                    <button type="button" data-bs-toggle="dropdown" class="btn btn-outline-light btn-icon btn-sm text-body border-transparent rounded-pill">
                        <i class="ph-dots-three-vertical"></i>
                    </button>

                    <div class="dropdown-menu dropdown-menu-end">
                        <a href="#" class="dropdown-item">
                            <i class="ph-eye me-2"></i>
                            View report
                        </a>
                        <a href="#" class="dropdown-item">
                            <i class="ph-printer me-2"></i>
                            Print report
                        </a>
                        <a href="#" class="dropdown-item">
                            <i class="ph-file-pdf me-2"></i>
                            Export report
                        </a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item">
                            <i class="ph-gear me-2"></i>
                            Configure
                        </a>
                    </div>
                </div>
            </div>

            <div class="chart-container">
                <div class="chart" id="area_gradient_green" style="height: 100px"></div>
            </div>
        </div>
    </div>
</div>
<!-- /blocks with chart -->


<!-- Latest orders -->
<div class="card">
    <div class="card-header d-flex py-0">
        <h5 class="py-3 mb-0">Latest orders</h5>

        <div class="d-inline-flex align-items-center ms-auto">
            <span class="badge bg-success fw-semibold">29 new</span>
            <div class="dropdown ms-2">
                <button type="button" class="btn btn-outline-light btn-icon btn-sm text-body border-transparent rounded-pill" data-bs-toggle="dropdown">
                    <i class="ph-dots-three-vertical"></i>
                </button>

                <div class="dropdown-menu dropdown-menu-end">
                    <a href="#" class="dropdown-item">
                        <i class="ph-eye me-2"></i>
                        View report
                    </a>
                    <a href="#" class="dropdown-item">
                        <i class="ph-printer me-2"></i>
                        Print report
                    </a>
                    <a href="#" class="dropdown-item">
                        <i class="ph-file-pdf me-2"></i>
                        Export report
                    </a>
                    <div class="dropdown-divider"></div>
                    <a href="#" class="dropdown-item">
                        <i class="ph-gear me-2"></i>
                        Configure
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="table-responsive">
        <table class="table text-nowrap">
            <thead>
                <tr>
                    <th>Company</th>
                    <th>Delivery date</th>
                    <th>Delivery method</th>
                    <th>Status</th>
                    <th class="text-center" style="width: 20px;"><i class="ph-arrow-circle-down"></i></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <div class="d-flex align-items-center">
                            <a href="#" class="me-3">
                                <img src="<?= base_url('public/global_assets/images/brands/klm.svg'); ?>" class="rounded-circle" width="32" height="32" alt="">
                            </a>

                            <div>
                                <a href="#" class="text-body fw-semibold">KLM Royal Dutch Airlines</a>
                                <div class="text-muted fs-sm">May 21st</div>
                            </div>
                        </div>
                    </td>
                    <td>June 12th</td>
                    <td><img src="<?= base_url('public/global_assets/images/brands/ups.svg'); ?>" class="me-1" width="20" alt=""> UPS Express</td>
                    <td><span class="badge bg-success bg-opacity-10 text-success">Delivered</span></td>
                    <td class="text-center">
                        <div class="dropdown">
                            <button type="button" class="btn btn-outline-light btn-icon btn-sm text-body border-transparent rounded-pill" data-bs-toggle="dropdown">
                                <i class="ph-dots-three-vertical"></i>
                            </button>
                            <div class="dropdown-menu dropdown-menu-end">
                                <a href="#" class="dropdown-item">
                                    <i class="ph-receipt me-2"></i>
                                    Order details
                                </a>
                                <a href="#" class="dropdown-item">
                                    <i class="ph-file-pdf me-2"></i>
                                    Download invoice
                                </a>
                                <a href="#" class="dropdown-item">
                                    <i class="ph-chart-bar me-2"></i>
                                    Statistics
                                </a>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="d-flex align-items-center">
                            <a href="#" class="me-3">
                                <img src="<?= base_url('public/global_assets/images/brands/amazon.svg'); ?>" class="rounded-circle" width="32" height="32" alt="">
                            </a>

                            <div>
                                <a href="#" class="text-body fw-semibold">Amazon Inc.</a>
                                <div class="text-muted fs-sm">May 22nd</div>
                            </div>
                        </div>
                    </td>
                    <td>June 13th</td>
                    <td><img src="<?= base_url('public/global_assets/images/brands/deutsche-post.svg'); ?>" class="rounded-sm me-1" width="20" alt=""> Deutsche post</td>
                    <td><span class="badge bg-danger bg-opacity-10 text-danger">Delayed</span></td>
                    <td class="text-center">
                        <div class="dropdown">
                            <button type="button" class="btn btn-outline-light btn-icon btn-sm text-body border-transparent rounded-pill" data-bs-toggle="dropdown">
                                <i class="ph-dots-three-vertical"></i>
                            </button>
                            <div class="dropdown-menu dropdown-menu-end">
                                <a href="#" class="dropdown-item">
                                    <i class="ph-receipt me-2"></i>
                                    Order details
                                </a>
                                <a href="#" class="dropdown-item">
                                    <i class="ph-file-pdf me-2"></i>
                                    Download invoice
                                </a>
                                <a href="#" class="dropdown-item">
                                    <i class="ph-chart-bar me-2"></i>
                                    Statistics
                                </a>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="d-flex align-items-center">
                            <a href="#" class="me-3">
                                <img src="<?= base_url('public/global_assets/images/brands/honda.svg'); ?>" class="rounded-circle" width="32" height="32" alt="">
                            </a>

                            <div>
                                <a href="#" class="text-body fw-semibold">Honda Motor Company</a>
                                <div class="text-muted fs-sm">May 22nd</div>
                            </div>
                        </div>
                    </td>
                    <td>June 14th</td>
                    <td><img src="<?= base_url('public/global_assets/images/brands/postnl.svg'); ?>" class="me-1" width="20" alt=""> PostNL</td>
                    <td><span class="badge bg-secondary bg-opacity-10 text-secondary">Processing</span></td>
                    <td class="text-center">
                        <div class="dropdown">
                            <button type="button" class="btn btn-outline-light btn-icon btn-sm text-body border-transparent rounded-pill" data-bs-toggle="dropdown">
                                <i class="ph-dots-three-vertical"></i>
                            </button>
                            <div class="dropdown-menu dropdown-menu-end">
                                <a href="#" class="dropdown-item">
                                    <i class="ph-receipt me-2"></i>
                                    Order details
                                </a>
                                <a href="#" class="dropdown-item">
                                    <i class="ph-file-pdf me-2"></i>
                                    Download invoice
                                </a>
                                <a href="#" class="dropdown-item">
                                    <i class="ph-chart-bar me-2"></i>
                                    Statistics
                                </a>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="d-flex align-items-center">
                            <a href="#" class="me-3">
                                <img src="<?= base_url('public/global_assets/images/brands/holiday-inn.svg'); ?>" class="rounded-circle" width="32" height="32" alt="">
                            </a>

                            <div>
                                <a href="#" class="text-body fw-semibold">Holiday Inn Hotels</a>
                                <div class="text-muted fs-sm">May 23rd</div>
                            </div>
                        </div>
                    </td>
                    <td>June 15th</td>
                    <td><img src="<?= base_url('public/global_assets/images/brands/fedex.svg'); ?>" class="rounded-sm me-1" width="20" alt=""> Fedex Express</td>
                    <td><span class="badge bg-success bg-opacity-10 text-success">Delivered</span></td>
                    <td class="text-center">
                        <div class="dropdown">
                            <button type="button" class="btn btn-outline-light btn-icon btn-sm text-body border-transparent rounded-pill" data-bs-toggle="dropdown">
                                <i class="ph-dots-three-vertical"></i>
                            </button>
                            <div class="dropdown-menu dropdown-menu-end">
                                <a href="#" class="dropdown-item">
                                    <i class="ph-receipt me-2"></i>
                                    Order details
                                </a>
                                <a href="#" class="dropdown-item">
                                    <i class="ph-file-pdf me-2"></i>
                                    Download invoice
                                </a>
                                <a href="#" class="dropdown-item">
                                    <i class="ph-chart-bar me-2"></i>
                                    Statistics
                                </a>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="d-flex align-items-center">
                            <a href="#" class="me-3">
                                <img src="<?= base_url('public/global_assets/images/brands/apple.svg'); ?>" class="rounded-circle" width="32" height="32" alt="">
                            </a>

                            <div>
                                <a href="#" class="text-body fw-semibold">Apple Inc.</a>
                                <div class="text-muted fs-sm">May 23rd</div>
                            </div>
                        </div>
                    </td>
                    <td>June 16th</td>
                    <td><img src="<?= base_url('public/global_assets/images/brands/deutsche-post.svg'); ?>" class="rounded-sm me-1" width="20" alt=""> Deutsche post</td>
                    <td><span class="badge bg-indigo bg-opacity-10 text-indigo">Dispatched</span></td>
                    <td class="text-center">
                        <div class="dropdown">
                            <button type="button" class="btn btn-outline-light btn-icon btn-sm text-body border-transparent rounded-pill" data-bs-toggle="dropdown">
                                <i class="ph-dots-three-vertical"></i>
                            </button>
                            <div class="dropdown-menu dropdown-menu-end">
                                <a href="#" class="dropdown-item">
                                    <i class="ph-receipt me-2"></i>
                                    Order details
                                </a>
                                <a href="#" class="dropdown-item">
                                    <i class="ph-file-pdf me-2"></i>
                                    Download invoice
                                </a>
                                <a href="#" class="dropdown-item">
                                    <i class="ph-chart-bar me-2"></i>
                                    Statistics
                                </a>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="d-flex align-items-center">
                            <a href="#" class="me-3">
                                <img src="<?= base_url('public/global_assets/images/brands/debijenkorf.svg'); ?>" class="rounded-circle" width="32" height="32" alt="">
                            </a>

                            <div>
                                <a href="#" class="text-body fw-semibold">de Bijenkorf</a>
                                <div class="text-muted fs-sm">May 23rd</div>
                            </div>
                        </div>
                    </td>
                    <td>June 17th</td>
                    <td><img src="<?= base_url('public/global_assets/images/brands/tnt.svg'); ?>" class="rounded-sm me-1" width="20" alt=""> TNT</td>
                    <td><span class="badge bg-indigo bg-opacity-10 text-indigo">Dispatched</span></td>
                    <td class="text-center">
                        <div class="dropdown">
                            <button type="button" class="btn btn-outline-light btn-icon btn-sm text-body border-transparent rounded-pill" data-bs-toggle="dropdown">
                                <i class="ph-dots-three-vertical"></i>
                            </button>
                            <div class="dropdown-menu dropdown-menu-end">
                                <a href="#" class="dropdown-item">
                                    <i class="ph-receipt me-2"></i>
                                    Order details
                                </a>
                                <a href="#" class="dropdown-item">
                                    <i class="ph-file-pdf me-2"></i>
                                    Download invoice
                                </a>
                                <a href="#" class="dropdown-item">
                                    <i class="ph-chart-bar me-2"></i>
                                    Statistics
                                </a>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="d-flex align-items-center">
                            <a href="#" class="me-3">
                                <img src="<?= base_url('public/global_assets/images/brands/texaco.svg'); ?>" class="rounded-circle" width="32" height="32" alt="">
                            </a>

                            <div>
                                <a href="#" class="text-body fw-semibold">Texaco Inc.</a>
                                <div class="text-muted fs-sm">May 24th</div>
                            </div>
                        </div>
                    </td>
                    <td>June 18th</td>
                    <td><img src="<?= base_url('public/global_assets/images/brands/dpd.svg'); ?>" class="me-1" width="20" alt=""> DPD</td>
                    <td><span class="badge bg-danger bg-opacity-10 text-danger">Delayed</span></td>
                    <td class="text-center">
                        <div class="dropdown">
                            <button type="button" class="btn btn-outline-light btn-icon btn-sm text-body border-transparent rounded-pill" data-bs-toggle="dropdown">
                                <i class="ph-dots-three-vertical"></i>
                            </button>
                            <div class="dropdown-menu dropdown-menu-end">
                                <a href="#" class="dropdown-item">
                                    <i class="ph-receipt me-2"></i>
                                    Order details
                                </a>
                                <a href="#" class="dropdown-item">
                                    <i class="ph-file-pdf me-2"></i>
                                    Download invoice
                                </a>
                                <a href="#" class="dropdown-item">
                                    <i class="ph-chart-bar me-2"></i>
                                    Statistics
                                </a>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="d-flex align-items-center">
                            <a href="#" class="me-3">
                                <img src="<?= base_url('public/global_assets/images/brands/shell.svg'); ?>" class="rounded-circle" width="32" height="32" alt="">
                            </a>

                            <div>
                                <a href="#" class="text-body fw-semibold">Royal Dutch Shell</a>
                                <div class="text-muted fs-sm">May 25th</div>
                            </div>
                        </div>
                    </td>
                    <td>June 19th</td>
                    <td><img src="<?= base_url('public/global_assets/images/brands/ups.svg'); ?>" class="me-1" width="20" alt=""> UPS Express</td>
                    <td><span class="badge bg-success bg-opacity-10 text-success">Delivered</span></td>
                    <td class="text-center">
                        <div class="dropdown">
                            <button type="button" class="btn btn-outline-light btn-icon btn-sm text-body border-transparent rounded-pill" data-bs-toggle="dropdown">
                                <i class="ph-dots-three-vertical"></i>
                            </button>
                            <div class="dropdown-menu dropdown-menu-end">
                                <a href="#" class="dropdown-item">
                                    <i class="ph-receipt me-2"></i>
                                    Order details
                                </a>
                                <a href="#" class="dropdown-item">
                                    <i class="ph-file-pdf me-2"></i>
                                    Download invoice
                                </a>
                                <a href="#" class="dropdown-item">
                                    <i class="ph-chart-bar me-2"></i>
                                    Statistics
                                </a>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="d-flex align-items-center">
                            <a href="#" class="me-3">
                                <img src="<?= base_url('public/global_assets/images/brands/tesla.svg'); ?>" class="rounded-circle" width="32" height="32" alt="">
                            </a>

                            <div>
                                <a href="#" class="text-body fw-semibold">Tesla Inc.</a>
                                <div class="text-muted fs-sm">May 26th</div>
                            </div>
                        </div>
                    </td>
                    <td>June 21st</td>
                    <td><img src="<?= base_url('public/global_assets/images/brands/dpd.svg'); ?>" class="me-1" width="20" alt=""> DPD</td>
                    <td><span class="badge bg-secondary bg-opacity-10 text-secondary">Processing</span></td>
                    <td class="text-center">
                        <div class="dropdown">
                            <button type="button" class="btn btn-outline-light btn-icon btn-sm text-body border-transparent rounded-pill" data-bs-toggle="dropdown">
                                <i class="ph-dots-three-vertical"></i>
                            </button>
                            <div class="dropdown-menu dropdown-menu-end">
                                <a href="#" class="dropdown-item">
                                    <i class="ph-receipt me-2"></i>
                                    Order details
                                </a>
                                <a href="#" class="dropdown-item">
                                    <i class="ph-file-pdf me-2"></i>
                                    Download invoice
                                </a>
                                <a href="#" class="dropdown-item">
                                    <i class="ph-chart-bar me-2"></i>
                                    Statistics
                                </a>
                            </div>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<!-- /content -->

<?= $this->endSection() ?>