<?= $this->extend('template/page_layout') ?>

<?= $this->section('content') ?>
<!-- Content -->
<div class="card shadow-lg">
    <div class="card-header">
        <h5 class="mb-0"><i class="ph-stack me-2"></i><?= lang('App.' . $title); ?></h5>
    </div>

    <!-- <div class="card-body">
        This example shows a single button that is specified using the <code>buttons.buttons.text</code> and <code>buttons.buttons.action</code> parameters only - it simply shows an alert when activated, but any Javascript function could be run when the button is activated. All parameters are optional, and each plug-in button type can also specify its own parameters. Also this example shows how buttons can have optional classes.
    </div> -->
    <div class="card-body">
        <table class="table datatable-basic table-bordered table-striped table-hover table-xs" id="serverside">
            <input type="hidden" id="path" value="<?= $folder; ?>">
            <?php if (check_role($i_menu, 1)->getnumRows() > 0) {
                $menu = $i_menu;
            } else {
                $menu = "";
            } ?>
            <input type="hidden" id="i_menu" value="<?= $menu; ?>">
            <thead>
                <tr class="table-border-double">
                    <th>#</th>
                    <th>Nama</th>
                    <th>Support</th>
                    <th><?= lang('App.Status'); ?></th>
                    <?php if (check_role($i_menu, 3)->getnumRows() > 0) { ?>
                        <th><?= lang('App.Action'); ?></th>
                    <?php } ?>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
    <div class="modol">
        <!-- Vertical form modal -->
        <div class="modal modal_form_create fade" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title"><?= lang('App.' . $title); ?> form</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
                    </div>

                    <form action="#" class="needs-validation-create" novalidate>
                        <div class="modal-body">
                            <div class="mb-3">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <label class="form-label">Level Name <span class="text-danger">*</span></label>
                                        <input type="text" placeholder="Level Name" name="e_name" required class="form-control">
                                        <div class="invalid-feedback">Invalid feedback</div>
                                        <div class="valid-feedback">Valid feedback</div>
                                    </div>
                                </div>
                            </div>

                            <div class="mb-3">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <label class="form-label">Note Level</label>
                                        <textarea class="form-control" name="e_note" placeholder="Note Level .."></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button type="reset" class="btn btn-warning">Reset</button>
                            <button type="button" class="btn btn-pink" data-bs-dismiss="modal">Close</button>
                            <button type="submit" data-submit="create" class="btn submit btn-primary">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="modal modal_form_edit fade" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title"><?= lang('App.' . $title); ?> form</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
                    </div>

                    <form action="#" class="needs-validation-edit" novalidate>
                        <div class="modal-body">
                            <div class="mb-3">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <label class="form-label">Level Name <span class="text-danger">*</span></label>
                                        <input type="hidden" placeholder="Level Name" name="id" required class="form-control id">
                                        <input type="text" placeholder="Level Name" name="e_name" required class="form-control name">
                                        <div class="invalid-feedback">Invalid feedback</div>
                                        <div class="valid-feedback">Valid feedback</div>
                                    </div>
                                </div>
                            </div>

                            <div class="mb-3">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <label class="form-label">Note Level</label>
                                        <textarea class="form-control note" name="e_note" placeholder="Note Level .."></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button type="reset" class="btn btn-warning">Reset</button>
                            <button type="button" class="btn btn-pink" data-bs-dismiss="modal">Close</button>
                            <button type="submit" data-submit="edit" class="btn submit btn-primary">Update</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- /vertical form modal -->
    </div>
</div>
<!-- /Content -->
<?= $this->endSection() ?>