<?= $this->extend('template/page_layout') ?>

<?= $this->section('content') ?>
<!-- Content -->

<div class="row">
    <div class="col-md-12">

        <!-- Basic layout-->
        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title"><i class="icon-file-check mr-2"></i><?= lang('App.'.$title); ?></h5>
                <div class="header-elements">
                    <div class="list-icons">
                        <a href="#" hidden class="list-icons-item animation" data-animation="fadeInDown"><i class="icon-play3"></i></a>
                        <a class="list-icons-item" data-action="collapse"></a>
                        <a class="list-icons-item" data-action="reload"></a>
                        <a class="list-icons-item" data-action="remove"></a>
                    </div>
                </div>
            </div>

            <div class="card-body border-top">
                <input type="hidden" id="path" value="<?= $folder;?>">
                <form class="form-validate-jquery">
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group mb-3">
                                <label>Nama :</label>
                                <input type="text" name="e_company_name" value="<?= $model->e_company_name ?>" autofocus class="form-control text-capitalize" placeholder="" readonly>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group mb-3">
                                <label>Nama Pendek :</label>
                                <input type="text" name="e_company_shortname" value="<?= $model->e_company_shortname ?>" autofocus class="form-control text-capitalize" placeholder="" readonly>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-6">
                            <div class="form-group mb-3">
                                <label>Support :</label>
                                <?php $text = ($model->f_support == 't') ? 'Yes' : 'No'; ?>
                                <input type="text" name="f_support" value="<?= $text ?>" autofocus class="form-control text-capitalize" placeholder="" readonly>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group mb-3">
                                <label>Status :</label>
                                <?php $text = ($model->f_active == 't') ? 'Active' : 'inactive'; ?>
                                <input type="text" name="f_active" value="<?= $text ?>" autofocus class="form-control text-capitalize" placeholder="" readonly>
                            </div>
                        </div>
                    </div>

                    <div class="text-end">
                        <div class="d-flex justify-content-between align-items-center">                                
                            <div class="d-inline-flex">
                                <a href="<?= base_url($folder.'/control');?>" class="btn btn-pink">Back<i class="ph-arrow-u-up-left ms-2"></i></a>
                                <?php $id = encrypt_url($model->i_company); ?>
                                <a href="<?= base_url($folder.'/control/update?id=' . $id );?>" class="btn btn-primary ms-2">Edit<i class="ph-pencil ms-2"></i></a>
                            </div>
                        </div>
                        <!-- <button type="submit" class="btn btn-primary">Submit form <i class="ph-paper-plane-tilt ms-2"></i></button> -->
                    </div>
                </form>
            </div>    

        </div>
        <!-- /basic layout -->

    </div>
</div>

<!-- Content -->
<?= $this->endSection() ?>