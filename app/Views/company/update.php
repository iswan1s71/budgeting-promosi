<?= $this->extend('template/page_layout') ?>

<?= $this->section('content') ?>
<!-- Content -->

<div class="row">
    <div class="col-md-12">

        <!-- Basic layout-->
        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title"><i class="icon-file-check mr-2"></i><?= lang('App.'.$title); ?></h5>
                <div class="header-elements">
                    <div class="list-icons">
                        <a href="#" hidden class="list-icons-item animation" data-animation="fadeInDown"><i class="icon-play3"></i></a>
                        <a class="list-icons-item" data-action="collapse"></a>
                        <a class="list-icons-item" data-action="reload"></a>
                        <a class="list-icons-item" data-action="remove"></a>
                    </div>
                </div>
            </div>

            <div class="card-body border-top">
                <form action="#" class="needs-validation" novalidate>
                    <input type="hidden" id="path" value="<?= $folder;?>">
                    <input type="hidden" name="i_company" value="<?= $model->i_company;?>">
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group mb-3">
                                <label>Nama :</label>
                                <input type="text" name="e_company_name" value="<?= $model->e_company_name ?>" autofocus class="form-control text-capitalize" placeholder="" required>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group mb-3">
                                <label>Nama Pendek :</label>
                                <input type="text" name="e_company_shortname" value="<?= $model->e_company_shortname ?>" autofocus class="form-control text-capitalize" placeholder="" required>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-6">
                            <div class="form-group mb-3">
                                <label>Support :</label>
                                <select id="f_support" name="f_support" class="form-control select2" placeholder="Pilih Level" required>                                
                                    <option value="t" <?= ($model->f_support == 't') ? 'selected' : '' ?>>Yes</option>
                                    <option value="f" <?= ($model->f_support == 'f') ? 'selected' : '' ?>>No</option>
                                </select>                                
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group mb-3">
                                <label>Status :</label>
                                <select id="f_active" name="f_active" class="form-control select2" placeholder="Pilih Level" required>                                
                                    <option value="t" <?= ($model->f_active == 't') ? 'selected' : '' ?>>Active</option>
                                    <option value="f" <?= ($model->f_active == 'f') ? 'selected' : '' ?>>Inactive</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="text-end">
                        <div class="d-flex justify-content-between align-items-center">
                            <button type="reset" class="btn btn-warning btn-icon">Reset<i class="ph-arrows-counter-clockwise ms-2"></i></button>
                            <div class="d-inline-flex">
                                <a href="<?= base_url($folder.'/control');?>" class="btn btn-pink">Back<i class="ph-arrow-u-up-left ms-2"></i></a>
                                <button type="submit" class="btn btn-primary ms-3">Update <i class="ph-paper-plane-tilt ms-2"></i></button>
                            </div>
                        </div>
                        <!-- <button type="submit" class="btn btn-primary">Submit form <i class="ph-paper-plane-tilt ms-2"></i></button> -->
                    </div>
                </form>
            </div>    

        </div>
        <!-- /basic layout -->

    </div>
</div>

<!-- Content -->
<?= $this->endSection() ?>