<style>
    .shadow {
        /* JUST FOR STYLING */
        margin-left: 5rem;
        margin-top: 5rem;
        /* JUST FOR STYLING */
        background: linear-gradient(0deg, #000, #262626);
    }

    .shadow:before,
    .shadow:after {
        content: '';
        position: absolute;
        top: -2px;
        left: -2px;
        background: linear-gradient(45deg, #fb0094, #0000ff, #00ff00, #ffff00, #ff0000, #fb0094, #0000ff, #00ff00, #ffff00, #ff0000);
        background-size: 400%;
        width: calc(100% + 4px);
        height: calc(100% + 4px);
        z-index: -1;
        animation: animate 20s linear infinite;
    }

    .shadow:after {
        filter: blur(10px);
    }

    @keyframes animate {
        0% {
            background-position: 0 0;
        }

        50% {
            background-position: 300% 0;
        }

        100% {
            background-position: 0 0;
        }
    }
</style>
<?= $this->extend('layout/page_layout') ?>

<?= $this->section('content') ?>
<!-- Dashboard content -->
<!-- Ajax sourced data -->
<div class="card shadow">
    <!-- <div class="border-slate card-header header-elements-inline"> -->
    <div class="card-header bg-transparent border-slate header-elements-inline">
        <h5 class="card-title"><i class="icon-file-text2 mr-2"></i><?= lang('App.'.$title); ?></h5>
        <div class="header-elements">
            <div class="list-icons">
                <a href="#" hidden class="list-icons-item animation" data-animation="fadeInDown"><i class="icon-play3"></i></a>
                <a class="list-icons-item" data-action="collapse"></a>
                <a class="list-icons-item" data-action="reload"></a>
                <a class="list-icons-item" data-action="remove"></a>
            </div>
        </div>
    </div>
    <hr class="mt-0 mb-0">
    <!-- <div class="card-body">
        DataTables has the ability to read data from virtually any <code>JSON</code> data source that can be obtained by <code>Ajax</code>. This can be done, in its most simple form, by setting the <code>ajax</code> option to the address of the <code>JSON</code> data source. The example below shows DataTables loading data for a table from <code>arrays</code> as the data source (object parameters can also be used through the <code>columns.data</code> option).
    </div> -->
    <div class="table-responsive">
        <table class="table table-xs table-columned" id="serverside">
            <input type="hidden" id="path" value="<?= $folder; ?>">
            <thead>
                <tr class="table-border-double">
                    <th>#</th>
                    <th><?= lang('App.User');?></th>
                    <th><?= lang('App.IP Adress');?></th>
                    <th><?= lang('App.Time');?></th>
                    <th><?= lang('App.Activity');?></th>
                </tr>
            </thead>
        </table>
    </div>
</div>
<!-- /ajax sourced data -->
<!-- /dashboard content -->

<!-- Theme JS files -->
<?= $this->endSection() ?>