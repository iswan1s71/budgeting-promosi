<!-- Global stylesheets -->
<link href="<?= base_url('public/global_assets/fonts/inter/inter.css'); ?>" rel="stylesheet" type="text/css">
<link href="<?= base_url('public/global_assets/icons/phosphor/styles.min.css'); ?>" rel="stylesheet" type="text/css">
<link href="<?= base_url('public/assets/css/ltr/all.min.css'); ?>" id="stylesheet" rel="stylesheet" type="text/css">
<!-- /global stylesheets -->