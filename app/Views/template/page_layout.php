<!DOCTYPE html>
<html lang="<?= session('language'); ?>" dir="ltr">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Apps | v4</title>

	<!-- Global stylesheets -->
	<?= $this->include('template/css') ?>
	<!-- /global stylesheets -->

	<!-- Add stylesheets -->
	<?= put_headers(); ?>
	<!-- /add stylesheets -->
</head>

<body>

	<!-- Main navbar -->
	<?= $this->include('template/navbar') ?>
	<!-- /main navbar -->

	<!-- Page content -->
	<div class="page-content">

		<!-- Main content -->
		<div class="content-wrapper">

			<!-- Inner content -->
			<div class="content-inner">

				<!-- Page header -->
				<?= $this->include('template/header') ?>
				<!-- /page header -->

				<!-- Content area -->
				<div class="content pt-0">
					<?= $this->renderSection('content') ?>
				</div>
				<!-- /content area -->

			</div>
			<!-- /inner content -->

		</div>
		<!-- /main content -->

	</div>
	<!-- /page content -->

	<!-- Notif -->
	<?= $this->include('template/notif') ?>
	<!-- /notif -->

	<!-- Footer -->
	<?= $this->include('template/footer') ?>
	<!-- /footer -->
	
	<!-- Config -->
	<?= $this->include('template/config') ?>
	<!-- /config -->

</body>

<!-- Theme JS files -->
<?= $this->include('template/js') ?>
<!-- /theme JS files -->

<!-- Add JS files -->
<?= put_footer(); ?>
<!-- /add JS files -->

</html>