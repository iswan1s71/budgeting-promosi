<!-- Main navbar -->
<div class="navbar navbar-expand-xl navbar-static shadow">
    <div class="container-fluid">
        <div class="navbar-brand flex-1">
            <a href="<?= base_url(); ?>" class="d-inline-flex align-items-center">
                <img src="<?= base_url('public/global_assets/images/logo_icon.png'); ?>" class="d-none d-sm-inline-block invert-dark ms-3" alt="logo" style="height: 32px;">
                <?php /*
                <img src="<?= base_url('public/global_assets/images/logo_text_dark.svg'); ?>" class="d-none d-sm-inline-block h-16px invert-dark ms-3" alt="">
                */

                            use CodeIgniter\Session\Session;

 ?>
            </a>
        </div>

        <div class="d-flex w-100 w-xl-auto overflow-auto overflow-xl-visible scrollbar-hidden border-top border-top-xl-0 order-1 order-xl-0 pt-2 pt-xl-0 mt-2 mt-xl-0">
            <ul class="nav gap-1 justify-content-center flex-nowrap flex-xl-wrap mx-auto">
                <li class="nav-item">
                    <a href="<?= base_url('main/home'); ?>" class="navbar-nav-link rounded active">
                        <i class="ph-house me-2"></i>
                        Home
                    </a>
                </li>

                <li class="nav-item nav-item-dropdown">
                    <a href="#" class="navbar-nav-link dropdown-toggle rounded" data-bs-toggle="dropdown" data-bs-auto-close="outside">
                        <i class="ph-rows me-2"></i>
                        Menu Navigation
                    </a>

                    <div class="dropdown-menu p-0">
                        <div class="d-xl-flex">
                            <div class="d-flex flex-row flex-xl-column bg-light overflow-auto overflow-xl-visible rounded-top rounded-top-xl-0 rounded-start-xl">
                                <div class="flex-1 border-bottom border-bottom-xl-0 p-2 p-xl-3">
                                    <div class="fw-bold border-bottom d-none d-xl-block pb-2 mb-2">Main Menu</div>
                                    <ul class="nav nav-pills flex-xl-column flex-nowrap text-nowrap justify-content-center wmin-xl-300" role="tablist">
                                        <?php if (get_menu()) : foreach (get_menu()->getResult() as $key) { ?>
                                                <li class="nav-item" role="presentation">
                                                <?php if ($key->e_folder=='#') {?>
                                                    <a href="#<?= no_space($key->e_menu); ?>" class="nav-link rounded" data-bs-toggle="tab" aria-selected="false" tabindex="-1" role="tab">
                                                        <i class="<?= $key->icon; ?> me-2"></i>
                                                        <?= $key->e_menu; ?>
                                                        <i class="ph-arrow-right nav-item-active-indicator d-none d-xl-inline-block ms-auto"></i>
                                                    </a>
                                                    <?php }else{ ?>
                                                        <?php $control = $key->e_folder == 'main' ? '' : '/control'; ?>
                                                        <a href="<?= base_url($key->e_folder . $control); ?>" class="nav-link rounded" aria-selected="false" tabindex="-1">
                                                        <i class="<?= $key->icon; ?> me-2"></i>
                                                        <?= $key->e_menu; ?>
                                                        <i class="ph-arrow-right nav-item-active-indicator d-none d-xl-inline-block ms-auto"></i>
                                                    </a>
                                                    <?php }?>
                                                </li>
                                        <?php } endif ;?>
                                    </ul>
                                </div>
                            </div>

                            <div class="tab-content flex-xl-1">
                                <?php if (get_menu()) {
                                    if (get_menu()->getNumRows() > 0) {
                                        foreach (get_menu()->getResult() as $key) { ?>
                                            <div class="tab-pane dropdown-scrollable-xl fade p-3" id="<?= no_space($key->e_menu); ?>" role="tabpanel">
                                                <div class="row">
                                                    <div class="col-lg-5 mb-3 mb-lg-0">
                                                        <div class="fw-bold border-bottom pb-2 mb-2">Menu</div>
                                                        <div class="align-items-start">
                                                            <div class="nav flex-column nav-pills me-3" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                                                <?php if (get_sub_menu($key->i_menu)->getNumRows() > 0) {
                                                                    foreach (get_sub_menu($key->i_menu)->getResult() as $row) {
                                                                        if ($row->e_folder == '#') { ?>
                                                                            <a href="#" class="dropdown-item rounded" data-bs-toggle="tab" aria-selected="true" id="v-pills-<?= no_space($row->e_menu); ?>-tab" data-bs-toggle="pill" data-bs-target="#v-pills-<?= no_space($row->e_menu); ?>" type="button" role="tab" aria-controls="v-pills-<?= no_space($row->e_menu); ?>" aria-selected="false"><?= $row->e_menu; ?></a>
                                                                        <?php } else { ?>
                                                                            <!-- <a href="<?= base_url($row->e_folder . '/control'); ?>" class="nav-link rounded" data-bs-toggle="tab" aria-selected="true" id="v-pills-<?= no_space($row->e_menu); ?>-tab" data-bs-toggle="pill" type="button" role="tab" aria-controls="v-pills-<?= no_space($row->e_menu); ?>" aria-selected="false"><?= $row->e_menu; ?></a> -->
                                                                            <a href="<?= base_url($row->e_folder . '/control'); ?>" class="dropdown-item rounded"><?= $row->e_menu; ?></a>
                                                                <?php }
                                                                    }
                                                                } ?>
                                                                <!-- <button class="nav-link" id="v-pills-profile-tab" data-bs-toggle="pill" data-bs-target="#v-pills-profile" type="button" role="tab" aria-controls="v-pills-profile" aria-selected="false">Profile</button> -->
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-7 mb-3 mb-lg-0">
                                                        <div class="fw-bold border-bottom pb-2 mb-2">Sub Menu</div>

                                                        <div class="tab-content" id="v-pills-tabContent">
                                                            <?php if (get_sub_menu($key->i_menu)->getNumRows() > 0) {
                                                                foreach (get_sub_menu($key->i_menu)->getResult() as $row) { ?>
                                                                    <div class="tab-pane fade" id="v-pills-<?= no_space($row->e_menu); ?>" role="tabpanel" aria-labelledby="v-pills-<?= no_space($row->e_menu); ?>-tab">
                                                                        <?php if (get_sub_menu($row->i_menu)->getNumRows() > 0) {
                                                                            foreach (get_sub_menu($row->i_menu)->getResult() as $kuy) { ?>
                                                                                <a href="<?= base_url($kuy->e_folder . '/control'); ?>" class="dropdown-item rounded"><?= $kuy->e_menu; ?></a>
                                                                        <?php }
                                                                        } ?>
                                                                    </div>
                                                            <?php }
                                                            } ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                <?php }
                                    }
                                } ?>

                                
                            </div>
                        </div>
                    </div>
                </li>
                
                <li class="nav-item nav-item-dropdown-xl dropdown">
                    <a href="#" class="navbar-nav-link dropdown-toggle rounded" data-bs-toggle="dropdown">
                        <i class="ph-arrows-clockwise me-2"></i>
                        Switch
                    </a>

                    <div class="dropdown-menu dropdown-menu-end">
                        <div class="dropdown-submenu dropdown-submenu-start">
                            <a href="#" class="dropdown-item dropdown-toggle">
                                <i class="ph-swap me-2"></i>
                                <?= lang('App.Department'); ?>
                            </a>
                            <div class="dropdown-menu">
                                <?php if (get_department()) {
                                    if (get_department()->getNumRows() > 0) {
                                        foreach (get_department()->getResult() as $key) { ?>
                                            <!-- <a href="#" class="dropdown-item department <?php if (session('i_department') == $key->i_department) { ?> active <?php } ?>" onclick="set_department('<?= $key->i_department; ?>','<?= $key->e_department_name; ?>'); return false;"><?= $key->e_department_name; ?></a> -->
                                            <a href="#" class="dropdown-item department <?php if (session('i_department') == $key->i_department) { ?> active <?php } ?>" data-id="<?= $key->i_department; ?>" data-name="<?= $key->e_department_name; ?>"><?= $key->e_department_name; ?></a>
                                <?php }
                                    }
                                } ?>
                            </div>
                        </div>
                        <div class="dropdown-submenu dropdown-submenu-start">
                            <a href="#" class="dropdown-item dropdown-toggle">
                                <i class="ph-swatches me-2"></i>
                                <?= lang('App.Level'); ?>
                            </a>
                            <div class="dropdown-menu">
                                <?php if (get_level()) {
                                    if (get_level()->getNumRows() > 0) {
                                        foreach (get_level()->getResult() as $key) { ?>
                                            <a href="#" class="dropdown-item level <?php if (session('i_level') == $key->i_level) { ?> active <?php } ?>" data-id="<?= $key->i_level; ?>" data-name="<?= $key->e_level_name; ?>"><?= $key->e_level_name; ?></a>
                                        <?php }
                                    } else { ?>
                                        <a href="<?= base_url(); ?>" class="dropdown-item">Set Department</a>
                                <?php }
                                } ?>
                            </div>
                        </div>
                        <div class="dropdown-submenu dropdown-submenu-start">
                            <a href="#" class="dropdown-item dropdown-toggle">
                                <i class="ph-flag me-2"></i>
                                <?= lang('App.Language'); ?>
                            </a>
                            <div class="dropdown-menu">
                                <!-- <a href="#" class="dropdown-item english language <?php if (session('language') == 'en') { ?> active <?php } ?>" data-id="en" onclick="set_language('en'); return false;"><img src="<?= base_url('public/global_assets/images/lang/gb.svg'); ?>" width="24" height="24" class="img-flag me-2" alt=""> English</a> -->
                                <a href="#" class="dropdown-item english language <?php if (session('language') == 'en') { ?> active <?php } ?>" data-id="en"><img src="<?= base_url('public/global_assets/images/lang/gb.svg'); ?>" width="24" height="24" class="img-flag me-2" alt=""> English</a>
                                <a href="#" class="dropdown-item indonesia language <?php if (session('language') == 'id') { ?> active <?php } ?>" data-id="id"><img src="<?= base_url('public/global_assets/images/lang/id.svg'); ?>" width="24" height="24" class="img-flag me-2" alt=""> Indonesia</a>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>

        <ul class="nav gap-1 flex-xl-1 justify-content-end order-0 order-xl-1">
            <?php /*
            <li class="nav-item nav-item-dropdown-xl dropdown">
                <a href="#" class="navbar-nav-link navbar-nav-link-icon rounded-pill" data-bs-toggle="dropdown">
                    <i class="ph-squares-four"></i>
                </a>

                <div class="dropdown-menu dropdown-menu-end dropdown-menu-scrollable-sm wmin-lg-600 p-0">
                    <div class="d-flex align-items-center border-bottom p-3">
                        <h6 class="mb-0">Browse apps</h6>
                        <a href="#" class="ms-auto">
                            View all
                            <i class="ph-arrow-circle-right ms-1"></i>
                        </a>
                    </div>

                    <div class="row row-cols-1 row-cols-sm-2 g-0">
                        <div class="col">
                            <button type="button" class="dropdown-item text-wrap h-100 align-items-start border-end-sm border-bottom p-3">
                                <div>
                                    <img src="<?= base_url('public/global_assets/images/demo/logos/1.svg'); ?>" class="h-40px mb-2" alt="">
                                    <div class="fw-semibold my-1">Customer data platform</div>
                                    <div class="text-muted">Unify customer data from multiple sources</div>
                                </div>
                            </button>
                        </div>

                        <div class="col">
                            <button type="button" class="dropdown-item text-wrap h-100 align-items-start border-bottom p-3">
                                <div>
                                    <img src="<?= base_url('public/global_assets/images/demo/logos/2.svg'); ?>" class="h-40px mb-2" alt="">
                                    <div class="fw-semibold my-1">Data catalog</div>
                                    <div class="text-muted">Discover, inventory, and organize data assets</div>
                                </div>
                            </button>
                        </div>

                        <div class="col">
                            <button type="button" class="dropdown-item text-wrap h-100 align-items-start border-end-sm border-bottom border-bottom-sm-0 rounded-bottom-start p-3">
                                <div>
                                    <img src="<?= base_url('public/global_assets/images/demo/logos/3.svg'); ?>" class="h-40px mb-2" alt="">
                                    <div class="fw-semibold my-1">Data governance</div>
                                    <div class="text-muted">The collaboration hub and data marketplace</div>
                                </div>
                            </button>
                        </div>

                        <div class="col">
                            <button type="button" class="dropdown-item text-wrap h-100 align-items-start rounded-bottom-end p-3">
                                <div>
                                    <img src="<?= base_url('public/global_assets/images/demo/logos/4.svg'); ?>" class="h-40px mb-2" alt="">
                                    <div class="fw-semibold my-1">Data privacy</div>
                                    <div class="text-muted">Automated provisioning of non-production datasets</div>
                                </div>
                            </button>
                        </div>
                    </div>
                </div>
            </li>
            */ ?>

            <li class="nav-item">
                <a href="#" class="navbar-nav-link navbar-nav-link-icon rounded-pill" data-bs-toggle="offcanvas" data-bs-target="#notifications">
                    <i class="ph-bell"></i>
                    <span class="badge bg-yellow text-black position-absolute top-0 end-0 translate-middle-top zindex-1 rounded-pill mt-1 me-1">2</span>
                </a>
            </li>

            <li class="nav-item nav-item-dropdown-xl dropdown">
                <a href="#" class="navbar-nav-link align-items-center rounded-pill p-1" data-bs-toggle="dropdown">
                    <div class="status-indicator-container">
                        <img src="<?= base_url('public/global_assets/images/demo/users/face11.jpg'); ?>" class="w-32px h-32px rounded-pill" alt="">
                        <span class="status-indicator bg-success"></span>
                    </div>
                    <span class="d-none d-md-inline-block mx-md-2"><?= session('e_user_name') ?></span>
                </a>
                <div class="dropdown-menu dropdown-menu-end">
                    <?php /*
                    <a href="#" class="dropdown-item">
                        <i class="ph-user-circle me-2"></i>
                        My profile
                    </a>
                    <a href="#" class="dropdown-item">
                        <i class="ph-currency-circle-dollar me-2"></i>
                        My subscription
                    </a>
                    <a href="#" class="dropdown-item">
                        <i class="ph-shopping-cart me-2"></i>
                        My orders
                    </a>
                    <a href="#" class="dropdown-item">
                        <i class="ph-envelope-open me-2"></i>
                        My inbox
                        <span class="badge bg-primary rounded-pill ms-auto">26</span>
                    </a>
                    <div class="dropdown-divider"></div>
                    */ ?>
                    <a href="#" class="dropdown-item">
                        <i class="ph-gear me-2"></i>
                        Account settings
                    </a>
                    <a href="<?= base_url('auth/logout'); ?>" class="dropdown-item">
                        <i class="ph-sign-out me-2"></i>
                        Logout
                    </a>
                </div>
            </li>
        </ul>
    </div>
</div>
<!-- /main navbar -->