<!-- Core JS files -->
<script src="<?= base_url('public/global_assets/js/demo_configurator.js'); ?>"></script>
<script src="<?= base_url('public/global_assets/js/bootstrap/bootstrap.bundle.min.js'); ?>"></script>
<!-- /core JS files -->
<script src="<?= base_url('public/assets/js/app.js'); ?>"></script>
<script>
    // $('.select-search').select2();
    var base_url = '<?= base_url(); ?>';
    var current_link = "<?= session('current_link'); ?>";
</script>
<script src="<?= base_url('public/assets/js/custom.js'); ?>"></script>