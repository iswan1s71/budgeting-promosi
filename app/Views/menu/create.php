<?= $this->extend('template/page_layout') ?>

<?= $this->section('content') ?>

<!-- Content -->
<div class="card">
    <div class="card-header">
        <input type="hidden" id="path" value="<?= $folder; ?>">
        <h5 class="mb-0"><i class="ph-plus-circle me-2"></i><?= lang('App.' . $title); ?></h5>
    </div>

    <div class="card-body border-top">
        <form action="#" class="needs-validation" novalidate>
            <fieldset class="mb-3">
                <legend class="fs-base fw-bold border-bottom pb-2 mb-3">Menu</legend>

                <div class="mb-3">
                    <label class="form-label"><?= lang('App.Parent'); ?>:</label>
                    <select name="i_parent" data-action="<?= base_url($folder . '/control/get_menu'); ?>" data-placeholder="<?= lang('App.Parent'); ?>" required class="form-control select-single">
                        <option value=""></option>
                    </select>
                    <div class="invalid-feedback">Invalid feedback</div>
                    <div class="valid-feedback">Valid feedback</div>
                </div>

                <div class="mb-3">
                    <label class="form-label"><?= lang('App.Menu Code'); ?> :</label>
                    <input type="number" class="form-control" placeholder="<?= lang('App.Menu Code'); ?>" name="i_menu" maxlength="10" autocomplete="off" required autofocus>
                    <div class="invalid-feedback">Invalid feedback</div>
                    <div class="valid-feedback">Valid feedback</div>
                </div>

                <div class="mb-3">
                    <label class="form-label"><?= lang('App.Menu Name'); ?> :</label>
                    <input type="text" class="form-control text-capitalize" placeholder="<?= lang('App.Menu Name'); ?>" name="e_menu" maxlength="30" autocomplete="off" required>
                    <div class="invalid-feedback">Invalid feedback</div>
                    <div class="valid-feedback">Valid feedback</div>
                </div>

                <div class="mb-3">
                    <label class="form-label"><?= lang('App.Order'); ?> No :</label>
                    <input type="number" class="form-control text-capitalize" placeholder="<?= lang('App.Order'); ?>" name="n_urut" maxlength="10" autocomplete="off" required>
                </div>

                <div class="mb-3">
                    <label class="form-label"><?= lang('App.Directory'); ?> :</label>
                    <input type="text" class="form-control text-lowercase" placeholder="<?= lang('App.Directory'); ?>" name="e_folder" maxlength="30" autocomplete="off" required>
                    <div class="invalid-feedback">Invalid feedback</div>
                    <div class="valid-feedback">Valid feedback</div>
                </div>

                <div class="mb-3">
                    <label class="form-label"><?= lang('App.Icon'); ?> :</label>
                    <input type="text" class="form-control text-lowercase" placeholder="<?= lang('App.Icon'); ?>" name="icon" maxlength="30" required>
                    <div class="invalid-feedback">Invalid feedback</div>
                    <div class="valid-feedback">Valid feedback</div>
                </div>
            </fieldset>

            <?php /*
            <fieldset>
                <legend class="fs-base fw-bold border-bottom pb-2 mb-3">Administrator Role</legend>

                <div class="mb-3">
                    <label class="form-label"><?= lang('App.Role Access'); ?> (<code>Administrator</code>):</label>
                    <select multiple="multiple" class="form-control multi-select" data-placeholder="<?= lang('App.Role Access'); ?>" required name="i_access[]">
                        <option value=""></option>
                        <?php if ($access->getNumRows() > 0) {
                            foreach ($access->getresult() as $key) { ?>
                                <option value="<?= $key->i_access; ?>"><?= $key->e_access_name; ?></option>
                        <?php }
                        } ?>
                    </select>
                    <div class="invalid-feedback">Invalid feedback</div>
                    <div class="valid-feedback">Valid feedback</div>
                </div>
            </fieldset>
            */ ?>

            <hr>
            <div class="text-end">
                <div class="d-flex justify-content-between align-items-center">
                    <button type="reset" class="btn btn-warning btn-icon">Reset<i class="ph-arrows-counter-clockwise ms-2"></i></button>
                    <div class="d-inline-flex">
                        <a href="<?= base_url($folder.'/control');?>" class="btn btn-pink">Back<i class="ph-arrow-u-up-left ms-2"></i></a>
                        <button type="submit" class="btn btn-primary ms-3">Submit <i class="ph-paper-plane-tilt ms-2"></i></button>
                    </div>
                </div>
                <!-- <button type="submit" class="btn btn-primary">Submit form <i class="ph-paper-plane-tilt ms-2"></i></button> -->
            </div>
        </form>
    </div>
</div>

<!-- Content -->
<?= $this->endSection() ?>