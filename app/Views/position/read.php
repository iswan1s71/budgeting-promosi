<?= $this->extend('layout/page_layout') ?>

<?= $this->section('content') ?>
<!-- Dashboard content -->
<!-- Ajax sourced data -->
<div class="card">
    <div class="card-header header-elements-inline">
        <h5 class="card-title"><i class="icon-file-text2 mr-2"></i><?= lang('App.' . $title); ?></h5>
        <div class="header-elements">
            <div class="list-icons">
                <a href="#" hidden class="list-icons-item animation" data-animation="fadeInDown"><i class="icon-play3"></i></a>
                <a class="list-icons-item" data-action="collapse"></a>
                <a class="list-icons-item" data-action="reload"></a>
                <a class="list-icons-item" data-action="remove"></a>
            </div>
        </div>
    </div>
    <hr class="mt-0 mb-0">
    <!-- <div class="card-body">
        DataTables has the ability to read data from virtually any <code>JSON</code> data source that can be obtained by <code>Ajax</code>. This can be done, in its most simple form, by setting the <code>ajax</code> option to the address of the <code>JSON</code> data source. The example below shows DataTables loading data for a table from <code>arrays</code> as the data source (object parameters can also be used through the <code>columns.data</code> option).
    </div> -->
    <div class="table-responsive">
        <table class="table table-sm table-columned" id="serverside">
            <input type="hidden" id="path" value="<?= $folder; ?>">
            <?php if (check_role($i_menu, 1)->getnumRows() > 0) {
                $menu = $i_menu;
            } else {
                $menu = "";
            } ?>
            <input type="hidden" id="i_menu" value="<?= $menu; ?>">
            <thead>
                <tr class="table-border-double">
                    <th>#</th>
                    <th><?= lang('App.Position Code'); ?></th>
                    <th><?= lang('App.Position Name'); ?></th>
                    <th><?= lang('App.Position Level'); ?></th>
                    <th><?= lang('App.Department Name'); ?></th>
                    <th><?= lang('App.Division Name'); ?></th>
                    <th><?= lang('App.Support'); ?></th>
                    <th><?= lang('App.Status'); ?></th>
                    <th><?= lang('App.Update'); ?></th>
                    <?php if (check_role($i_menu, 3)->getnumRows() > 0) { ?>
                        <th><?= lang('App.Action'); ?></th>
                    <?php } ?>
                </tr>
            </thead>
        </table>
    </div>
</div>
<!-- /ajax sourced data -->
<!-- /dashboard content -->

<!-- Theme JS files -->
<?= $this->endSection() ?>