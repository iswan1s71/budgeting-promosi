<?= $this->extend('layout/page_layout') ?>

<?= $this->section('content') ?>
<!-- Content -->

<div class="row">
    <div class="col-md-12">

        <!-- Basic layout-->
        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title"><i class="icon-file-plus mr-2"></i><?= lang('App.' . $title); ?></h5>
                <div class="header-elements">
                    <div class="list-icons">
                        <a href="#" hidden class="list-icons-item animation" data-animation="fadeInDown"><i class="icon-play3"></i></a>
                        <a class="list-icons-item" data-action="collapse"></a>
                        <a class="list-icons-item" data-action="reload"></a>
                        <a class="list-icons-item" data-action="remove"></a>
                    </div>
                </div>
            </div>
            <hr class="mt-0 mb-0">
            <div class="card-body">
                <input type="hidden" id="path" value="<?= $folder; ?>">
                <form class="form-validate-jquery">
                    <div class="form-group">
                        <label><?= lang('App.Company Type Name'); ?> :</label>
                        <input type="text" name="e_name" autofocus class="form-control text-capitalize" placeholder="<?= lang('App.Position Level Name'); ?>" required>
                    </div>
                    <div>
                        <button type="button" class="btn bg-slate-800 btn-sm submit"><?= lang('App.Save'); ?> <i class="icon-paperplane ml-2"></i></button>
                        <a href="<?= base_url($folder . '/control'); ?>" type="reset" class="btn bg-pink-400 btn-sm ml-2" id="reset"><?= lang('App.Back'); ?> <i class="icon-reload-alt ml-2"></i></a>
                    </div>
                </form>
            </div>
        </div>
        <!-- /basic layout -->

    </div>
</div>

<!-- Content -->
<?= $this->endSection() ?>