<?= $this->extend('template/page_layout') ?>

<?= $this->section('content') ?>
<!-- Content -->

<div class="row">
    <div class="col-md-12">

        <!-- Basic layout-->
        <div class="card">
            <div class="card-header">
                <h5 class="card-title"><i class="icon-file-plus mr-2"></i><?= lang('App.' . $title); ?></h5>
                <div class="header-elements">
                    <div class="list-icons">
                        <a href="#" hidden class="list-icons-item animation" data-animation="fadeInDown"><i class="icon-play3"></i></a>
                        <a class="list-icons-item" data-action="collapse"></a>
                        <a class="list-icons-item" data-action="reload"></a>
                        <a class="list-icons-item" data-action="remove"></a>
                    </div>
                </div>
            </div>
            <hr class="mt-0 mb-0">
            <div class="card-body">
                <input type="hidden" id="path" value="<?= $folder; ?>">
                <form class="form-validate-jquery">
                    <div class="form-group">
                        <label><?= lang('App.Department Code'); ?> :</label>
                        <input type="text" name="i_code" class="form-control text-uppercase" placeholder="<?= lang('App.Department Code'); ?>" required>
                    </div>
                    <div class="form-group">
                        <label><?= lang('App.Department Name'); ?> :</label>
                        <input type="text"  class="form-control text-capitalize" placeholder="<?= lang('App.Department Name'); ?>" required>
                    </div>
                    <div>
                        <button type="button" class="btn bg-slate-800 btn-sm submit"><?= lang('App.Save'); ?> <i class="icon-paperplane ml-2"></i></button>
                        <a href="<?= base_url($folder . '/control'); ?>" type="reset" class="btn bg-pink-400 btn-sm ml-2" id="reset"><?= lang('App.Back'); ?> <i class="icon-reload-alt ml-2"></i></a>
                    </div>
                </form>
            </div>
        </div>
        <!-- /basic layout -->

    </div>
</div>

<div class="card shadow-lg  ">
    <div class="card-header">
        <h5 class="mb-0"><?= lang('App.' . $title); ?></h5>
    </div>

    <form class="needs-validation" novalidate>
        <div class="card-body">
            
            <div class="fw-bold border-bottom pb-2 mb-1">Form Input</div>

            <div class="col-lg-12 mb-2">
                <label class="col-form-label"><?= lang('App.Department Name'); ?> <span class="text-danger">*</span></label>
                <div>
                    <input type="text" name="e_name" autofocus class="form-control text-capitalize" required placeholder="Input <?= lang('App.Department Name'); ?>">
                    <div class="invalid-feedback">Invalid feedback</div>
                    <div class="valid-feedback">Valid feedback</div>
                </div>
            </div>

        </div>

        <div class="card-footer text-end">
            <div class="row">
                <div class="col-lg-4">
                    <button type="submit" class="btn btn btn-flat-primary w-100">Submit <i class="ph-paper-plane-tilt ms-2"></i></button>
                </div>
                <div class="col-lg-4">
                    <button type="reset" class="btn btn btn-flat-warning w-100">Reset <i class="ph-eraser ms-2"></i></button>
                </div>
                <div class="col-lg-4">
                    <a href="<?= base_url($folder . '/control'); ?>" type="reset" class="btn btn btn-flat-dark w-100"><?= lang('App.Back'); ?> <i class="ph-arrow-u-up-left ms-2"></i></a>
                </div>
            </div>
        </div>
    </form>
</div>
<!-- Content -->
<?= $this->endSection() ?>