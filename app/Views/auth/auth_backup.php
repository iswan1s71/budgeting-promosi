<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Authentication | Login</title>

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="<?= base_url('public/global_assets/css/icons/icomoon/styles.min.css'); ?>" rel="stylesheet" type="text/css">
	<link href="<?= base_url('public/assets/css/bootstrap.min.css'); ?>" rel="stylesheet" type="text/css">
	<link href="<?= base_url('public/assets/css/bootstrap_limitless.min.css'); ?>" rel="stylesheet" type="text/css">
	<link href="<?= base_url('public/assets/css/layout.min.css'); ?>" rel="stylesheet" type="text/css">
	<link href="<?= base_url('public/assets/css/components.min.css'); ?>" rel="stylesheet" type="text/css">
	<link href="<?= base_url('public/assets/css/colors.min.css'); ?>" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->
</head>

<body style="background-image: url('<?= base_url('public/global_assets/images/login_bg.jpg');?>'); height: 100%; background-position: center; background-repeat: no-repeat; background-size: cover;">

	<!-- Page content -->
	<div class="page-content">

		<!-- Main content -->
		<div class="content-wrapper">

			<!-- Content area -->
			<div class="content d-flex justify-content-center align-items-center">

				<!-- Login card -->
				<form class="login-form" action="<?= base_url('auth/login'); ?>" method="post">
					<?= csrf_field(); ?>
					<!-- <div class="card mb-0"> -->
					<div class="">
						<div class="card-body border-1 border-slate-800">
							<div class="text-center mb-3">
								<i class="icon-cash3 icon-2x text-primary-800 border-primary-800 border-3 rounded-round p-3 mb-3 mt-1"></i>
								<h5 class="mb-0 font-weight-semibold text-primary-800">Login to your account</h5>
								<!-- <span class="d-block text-muted">Your credentials</span> -->
							</div>
							<?php if (isset($message)) : ?>
								<div class="alert alert-warning alert-dismissible fade show" role="alert">
									<?php echo $message; ?>
									<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								</div>
							<?php endif; ?>
							<div class="form-group form-group-feedback form-group-feedback-left">
								<input type="text" class="form-control" autofocus placeholder="Username" name="i_user_code">
								<div class="form-control-feedback">
									<i class="icon-user text-muted"></i>
								</div>
							</div>

							<div class="form-group form-group-feedback form-group-feedback-left">
								<input type="password" class="form-control" name="password" placeholder="Password">
								<div class="form-control-feedback">
									<i class="icon-lock2 text-muted"></i>
								</div>
							</div>

							<div class="form-group form-group-feedback form-group-feedback-left">
								<input id="colorfake" type="hidden" value="slate-800">
								<select class="form-control form-control-select2" name="color" id="color" data-container-css-class="bg-slate-800" data-placeholder="Pilih Warna Website" data-fouc>
									<option></option>
									<option value="primary-800">Primary</option>
									<option value="danger-800">Danger</option>
									<option value="success-800">Success</option>
									<option value="warning-800">Warning</option>
									<option value="info-800">Info</option>
									<option value="pink-800">Pink</option>
									<option value="violet-800">Violet</option>
									<option value="purple-800">Purple</option>
									<option value="indigo-800">Indigo</option>
									<option value="blue-800">Blue</option>
									<option value="teal-800">Teal</option>
									<option value="green-800">Green</option>
									<option value="orange-800">Orange</option>
									<option value="brown-800">Brown</option>
									<option value="grey-800">Grey</option>
									<option value="slate-800" selected>Slate</option>
								</select>
							</div>

							<!-- <div class="form-group d-flex align-items-center">
								<div class="form-check mb-0">
									<label class="form-check-label">
										<input type="checkbox" name="remember" class="form-input-styled" checked data-fouc>
										Remember
									</label>
								</div>

								<a href="login_password_recover.html" class="ml-auto">Forgot password?</a>
							</div> -->

							<div class="form-group">
								<button type="submit" class="btn bg-primary-800 btn-block">Sign in <i class="icon-circle-right2 ml-2"></i></button>
							</div>
						</div>
					</div>
				</form>
				<!-- /login card -->

			</div>
			<!-- /content area -->

		</div>
		<!-- /main content -->

	</div>
	<!-- /page content -->


</body>
<!-- Core JS files -->
<script src="<?= base_url('public/global_assets/js/main/jquery.min.js'); ?>"></script>
<script src="<?= base_url('public/global_assets/js/main/bootstrap.bundle.min.js'); ?>"></script>
<script src="<?= base_url('public/global_assets/js/plugins/loaders/blockui.min.js'); ?>"></script>
<!-- /core JS files -->

<!-- Theme JS files -->
<script src="<?= base_url('public/global_assets/js/plugins/forms/styling/uniform.min.js'); ?>"></script>
<script src="<?= base_url('public/assets/js/app.js'); ?>"></script>
<script src="<?= base_url('public/global_assets/js/demo_pages/login.js'); ?>"></script>
<script src="<?= base_url('public/assets/js/auth/auth.js'); ?>"></script>
<script src="<?= base_url('public/global_assets/js/plugins/forms/validation/validate.min.js');?>"></script>
<script src="<?= base_url('public/global_assets/js/plugins/forms/selects/select2.min.js');?>"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<!-- /theme JS files -->

</html>