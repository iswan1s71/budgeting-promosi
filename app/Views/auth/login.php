<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Budgeting - Promosi</title>

	<!-- Global stylesheets -->
	<link href="<?= base_url('public/assets/fonts/inter/inter.css') ?>" rel="stylesheet" type="text/css">
	<link href="<?= base_url('public/assets/icons/phosphor/styles.min.css') ?>" rel="stylesheet" type="text/css">
	<link href="<?= base_url('public/assets/css/all.min.css') ?>" id="stylesheet" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<!-- <script src="public/assets/demo/demo_configurator.js"></script> -->
	<!-- <script src="public/assets/js/bootstrap/bootstrap.bundle.min.js"></script> -->
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<!-- <script src="pulblic/assets/js/app.js"></script> -->
	<!-- /theme JS files -->

</head>

<body><div class="page-content">

<!-- Main content -->
<div class="content-wrapper">

    <!-- Inner content -->
    <div class="content-inner">

        <!-- Content area -->
        <div class="content d-flex justify-content-center align-items-center">

            <!-- Login form -->
            <form class="login-form" action="<?= base_url('auth/login'); ?>" method="post">
                <div class="card mb-0">
                    <div class="card-body">
                        <div class="text-center mb-4">
                            <div class="d-inline-flex align-items-center justify-content-center">
                                <img src="<?= base_url('public/assets/images/gif/loading.gif') ?>" class="h-48px" alt="">
                            </div>
                            <h4 class="mb-0">Budgeting Promosi</h4>
                            <span class="d-block text-muted">Enter your credentials below</span>
                        </div>

                        <?php if (isset($message)) : ?>
                            <div class="alert bg-danger text-white alert-icon-end alert-dismissible fade show border-0">
                                <span class="alert-icon bg-black bg-opacity-20">
                                    <i class="ph-x-circle"></i>
                                </span>
                                <span class="fw-semibold"><?php echo $message; ?></span>
                            </div>
                        <?php endif; ?>

                        <div class="mb-3">
                            <label class="form-label">Username</label>
                            <div class="form-control-feedback form-control-feedback-start">
                                <input type="text" required autofocus class="form-control" placeholder="Username" name="i_user_code">
                                <div class="form-control-feedback-icon">
                                    <i class="ph-user-circle text-muted"></i>
                                </div>
                            </div>
                        </div>

                        <div class="mb-3">
                            <label class="form-label">Password</label>
                            <div class="form-control-feedback form-control-feedback-start">
                                <input type="password" class="form-control" required name="password" placeholder="Password">
                                <div class="form-control-feedback-icon">
                                    <i class="ph-lock text-muted"></i>
                                </div>
                            </div>
                        </div>

                        <div class="mb-3">
                            <button type="submit" class="btn btn-primary w-100">Sign in</button>
                        </div>
                        
                    </div>
                </div>
            </form>
            <!-- /login form -->

        </div>
        <!-- /content area -->

    </div>
    <!-- /inner content -->

</div>
<!-- /main content -->

</div>
</body>
</html>
