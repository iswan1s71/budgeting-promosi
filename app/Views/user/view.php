<?= $this->extend('template/page_layout') ?>

<?= $this->section('content') ?>
<!-- Content -->


<div class="row">
    <div class="col-md-12">

        <!-- Basic layout-->
        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title"><i class="icon-file-check mr-2"></i><?= lang('App.' . $title); ?></h5>
                <div class="header-elements">
                    <div class="list-icons">
                        <a href="#" hidden class="list-icons-item animation" data-animation="fadeInRight"><i class="icon-play3"></i></a>
                        <a class="list-icons-item" data-action="collapse"></a>
                        <a class="list-icons-item" data-action="reload"></a>
                        <a class="list-icons-item" data-action="remove"></a>
                    </div>
                </div>
            </div>

            <div class="card-body border-top">
                <form action="#" class="needs-validation" novalidate>
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group mb-3">
                                <label class="form-label">Nama:</label>
                                <input type="text" value="<?= $model->e_user_name ?>" class="form-control col" readonly>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group mb-3">
                                <label class="form-label">User:</label>
                                <input type="text" value="<?= $model->i_user_code ?>" class="form-control col" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group mb-3">
                                <label class="form-label">Level:</label>
                                <select id="levels" name="levels[]" class="form-control select2" placeholder="Pilih Perusahaan" multiple=true readonly>
                                    <?php $list = \App\Models\Level\Models::get_list();
        
                                    foreach ($list->getResult() as $option) { ?>
        
                                        <?php foreach ($all_user_level->getResult() as $userLevel) { ?>
        
                                            <?php if ($userLevel->i_level == $option->i_level) { ?>
        
                                                <option value="<?= $option->i_level ?>" selected><?= $option->e_level_name ?></option>
        
                                            <?php } ?>
        
                                        <?php } ?>
        
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group mb-3">
                                <label class="form-label">Perusahaan :</label>
                                <select id="companies" name="companies[]" class="form-control select2" placeholder="Pilih Perusahaan" multiple=true readonly>
                                    <?php $list = \App\Models\Company\Models::get_list();
        
                                    foreach ($list->getResult() as $option) { ?>
        
                                        <?php foreach ($all_user_company->getResult() as $userCompany) { ?>
        
                                            <?php if ($userCompany->i_company == $option->i_company) { ?>
        
                                                <option value="<?= $option->i_company ?>" selected><?= $option->e_company_name ?></option>
        
                                            <?php } ?>
        
                                        <?php } ?>
        
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-6">
                            <div class="form-group mb-3">
                                <label class="form-label">Status:</label>
                                <?php $text = ($model->f_active == 't') ? 'Active' : 'Inactive'; ?>
                                <input type="text" value="<?= $text ?>" class="form-control col" readonly>
                            </div>
                        </div>
                        
                        <div class="col-6">
                            <div class="form-group mb-3">
                                <label class="form-label">Password:</label>
                                <input type="password" name="password" value="<?= $model->e_user_password ?>" class="form-control col" readonly>
                            </div>
                        </div>
                    </div>                    
                    
                    <hr>
                    <div class="text-end">
                        <div class="d-flex justify-content-between align-items-center">                                
                            <div class="d-inline-flex">
                                <a href="<?= base_url($folder.'/control');?>" class="btn btn-pink">Back<i class="ph-arrow-u-up-left ms-2"></i></a>
                                <?php $id = encrypt_url($model->i_user); ?>
                                <a href="<?= base_url($folder.'/control/update?id=' . $id );?>" class="btn btn-primary ms-2">Edit<i class="ph-pencil ms-2"></i></a>
                            </div>
                        </div>
                        <!-- <button type="submit" class="btn btn-primary">Submit form <i class="ph-paper-plane-tilt ms-2"></i></button> -->
                    </div>
                </form>
            </div>                
        </div>

    </div>
</div>

<!-- Content -->
<?= $this->endSection() ?>