<?= $this->extend('template/page_layout') ?>

<?= $this->section('content') ?>

<!-- Content -->
<div class="card">
    <div class="card-header">
        <input type="hidden" id="path" value="<?= $folder; ?>">
        <h5 class="mb-0"><i class="ph-plus-circle me-2"></i><?= lang('App.' . $title); ?></h5>
    </div>

    <div class="card-body border-top">
        <div class="row mb-3">
            <div class="col-12">
                <div class="" style="text-align: right;">
                    <?php 
                    $badge = "<span class='badge bg-danger' style='margin-left: 0.25rem;'>Error</span>";
                    $connection = \App\Models\Hris\Models::get();

                    if ($connection != null) {
                        $badge = "<span class='badge bg-success' style='margin-left: 0.25rem;'>Success</span>";
                    }

                    ?>
                    Connection to HRIS Database <?= $badge ?>
                </div>
            </div>
        </div>

        <form action="#" class="needs-validation" novalidate>
            <fieldset class="mb-3">
                <legend class="fs-base fw-bold border-bottom pb-2 mb-3">Form</legend>

                <div class="mb-3">
                    <label class="form-label">Cari User:</label>
                    <select name="iduser" id="iduser" required class="form-control" required>
                        <option value=""></option>
                    </select>
                    <div class="invalid-feedback">Invalid feedback</div>
                    <div class="valid-feedback">Valid feedback</div>
                </div>                
            </fieldset>

            <fieldset class="" id="fieldset-detail-user">
            </fieldset>

            <fieldset class="mb-3 d-none" id="fieldset-level">
                <div class="row">
                    <div class="col-6">
                        <div class="form-group mb-3">
                            <label class="form-label">Level :</label>
                            <select id="levels" name="levels[]" class="form-control select2" placeholder="Pilih Level" multiple=true required>
                                <?php $list = \App\Models\Level\Models::get_list();
                                foreach ($list->getResult() as $option) { ?>
                                        <option value="<?= $option->i_level ?>"><?= $option->e_level_name ?></option>
                                <?php } ?>
                            </select>
                            <div class="invalid-feedback">Invalid feedback</div>
                            <div class="valid-feedback">Valid feedback</div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label class="form-label">Perusahaan :</label>
                            <select id="companies" name="companies[]" class="form-control select2" placeholder="Pilih Perusahaan" multiple=true required>                        
                                <?php $list = \App\Models\Company\Models::get_list();
                                foreach ($list->getResult() as $option) { ?>
                                        <option value="<?= $option->i_company ?>"><?= $option->e_company_name ?></option>
                                <?php } ?>
                            </select>
                            <div class="invalid-feedback">Invalid feedback</div>
                            <div class="valid-feedback">Valid feedback</div>
                        </div>
                    </div>
                </div>   

                <div class="row">
                    <div class="col-12">
                        <div class="form-group mb-3">
                            <label class="form-label">New Password :</label>
                            <input type="password" name="password" id="password" class="form-control" placeholder="Password" required>
                            <div class="invalid-feedback">Invalid feedback</div>
                            <div class="valid-feedback">Valid feedback</div>
                        </div>
                    </div>
                </div>  
            </fieldset>

            <hr>
            <div class="text-end">
                <div class="d-flex justify-content-between align-items-center">
                    <button type="reset" class="btn btn-warning btn-icon">Reset<i class="ph-arrows-counter-clockwise ms-2"></i></button>
                    <div class="d-inline-flex">
                        <a href="<?= base_url($folder.'/control');?>" class="btn btn-pink">Back<i class="ph-arrow-u-up-left ms-2"></i></a>
                        <button type="submit" class="btn btn-primary ms-3">Submit <i class="ph-paper-plane-tilt ms-2"></i></button>
                    </div>
                </div>
                <!-- <button type="submit" class="btn btn-primary">Submit form <i class="ph-paper-plane-tilt ms-2"></i></button> -->
            </div>
        </form>
    </div>
</div>

<!-- Content -->
<?= $this->endSection() ?>