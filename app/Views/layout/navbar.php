<div class="navbar navbar-expand-md navbar-light fixed-top">

    <!-- Header with logos -->
    <div class="navbar-header navbar-dark d-none d-md-flex align-items-md-center">
        <div class="navbar-brand navbar-brand-md" style="padding-top: 0.3rem; padding-bottom: 0.00002rem;">
            <a href="<?= base_url(); ?>" class="d-inline-block">
                <img src="<?= base_url('public/global_assets/images/logo_light.png'); ?>" alt="" style="height: 2.5rem;width: 6.5rem;" class="mb-2">
            </a>
        </div>

        <div class="navbar-brand navbar-brand-xs" style="padding-top: 0.3rem; padding-bottom: 0.00002rem;">
            <a href="<?= base_url(); ?>" class="d-inline-block">
                <img src="<?= base_url('public/global_assets/images/logo_icon_light.png'); ?>" alt="" style="height: 2.5rem;width: 2.5rem;" class="mb-2">
            </a>
        </div>
    </div>
    <!-- /header with logos -->


    <!-- Mobile controls -->
    <div class="d-flex flex-1 d-md-none">
        <div class="navbar-brand mr-auto" style="padding-top: 0.3rem; padding-bottom: 0.00002rem;">
            <a href="<?= base_url(); ?>" class="d-inline-block">
                <img src="<?= base_url('public/global_assets/images/logo_light_white.png'); ?>" alt="" style="height: 2.5rem;width: 6.5rem;" class="mb-2">
            </a>
        </div>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
            <i class="icon-tree5"></i>
        </button>

        <button class="navbar-toggler sidebar-mobile-main-toggle" type="button">
            <i class="icon-paragraph-justify3"></i>
        </button>
    </div>
    <!-- /mobile controls -->


    <!-- Navbar content -->
    <div class="collapse navbar-collapse" id="navbar-mobile">
        <ul class="navbar-nav">
            <li class="nav-item">
                <?php if (session('sidebar') == 'sidebar-xs') { ?>
                    <a href="#" onclick="set_collapse(''); return false;" class="navbar-nav-link sidebar-control sidebar-main-toggle d-none d-md-block">
                        <i class="icon-paragraph-justify3"></i>
                    </a>
                <?php } else { ?>
                    <a href="#" onclick="set_collapse('sidebar-xs'); return false;" class="navbar-nav-link sidebar-control sidebar-main-toggle d-none d-md-block">
                        <i class="icon-paragraph-justify3"></i>
                    </a>
                <?php } ?>
            </li>

            <li class="nav-item dropdown">
                <?php if (session('e_department_name') != '') { ?>
                    <a href="#" class="navbar-nav-link dropdown-toggle" data-toggle="dropdown"><?= session('e_department_name');?></a>
                <?php } else { ?>
                    <a href="#" class="navbar-nav-link dropdown-toggle" data-toggle="dropdown"><?= lang('App.Select Department');?></a>
                <?php } ?>
                <div class="dropdown-menu dropdown-menu-right dropdown-scroll">
                    <?php if (get_department()) {
                        if (get_department()->getNumRows() > 0) {
                            foreach (get_department()->getResult() as $key) { ?>
                                <a href="#" class="dropdown-item <?php if (session('i_department') == $key->i_department) { ?> active <?php } ?>" onclick="set_department('<?= $key->i_department;?>','<?= $key->e_department_name;?>'); return false;"><?= $key->e_department_name; ?></a>
                    <?php }
                        }
                    } ?>
                </div>
            </li>

            <!-- <?php var_dump(check_level(session('i_level'))->getNumRows());?> -->
            <li class="nav-item dropdown">
                <?php if (check_level(session('i_level'))->getNumRows() > 0) { ?>
                    <a href="#" class="navbar-nav-link dropdown-toggle" data-toggle="dropdown"><?= session('e_level_name');?></a>
                <?php } else { ?>
                    <a href="#" class="navbar-nav-link dropdown-toggle" data-toggle="dropdown"><?= lang('App.Select Level');?></a>
                <?php } ?>
                <div class="dropdown-menu dropdown-menu-right dropdown-scroll">
                    <?php if (get_level()) {
                        if (get_level()->getNumRows() > 0) {
                            foreach (get_level()->getResult() as $key) { ?>
                                <a href="#" class="dropdown-item <?php if (session('i_level') == $key->i_level) { ?> active <?php } ?>" onclick="set_level('<?= $key->i_level;?>','<?= $key->e_level_name;?>'); return false;"><?= $key->e_level_name; ?></a>
                    <?php }
                        }
                    } ?>
                </div>
            </li>
        </ul>
        <span class="badge bg-pink-400 badge-pill ml-md-3 mr-md-auto">16 orders</span>


        <ul class="navbar-nav">
            <li class="nav-item dropdown">
                <?php if (session('language') == 'en') { ?>
                    <a href="#" class="navbar-nav-link dropdown-toggle" data-toggle="dropdown"><img src="<?= base_url('public/global_assets/images/lang/gb.png'); ?>" class="img-flag mr-2" alt="">English</a>
                <?php } else { ?>
                    <a href="#" class="navbar-nav-link dropdown-toggle" data-toggle="dropdown"><img src="<?= base_url('public/global_assets/images/lang/id.gif'); ?>" class="img-flag mr-2" alt="">Indonesia</a>
                <?php } ?>

                <div class="dropdown-menu dropdown-menu-right">
                    <a href="#" class="dropdown-item english <?php if (session('language') == 'en') { ?> active <?php } ?>" onclick="set_language('en'); return false;"><img src="<?= base_url('public/global_assets/images/lang/gb.png'); ?>" class="img-flag" alt=""> English</a>
                    <a href="#" class="dropdown-item indonesia <?php if (session('language') == 'id') { ?> active <?php } ?>" onclick="set_language('id'); return false;"><img src="<?= base_url('public/global_assets/images/lang/id.gif'); ?>" class="img-flag" alt=""> Indonesia</a>
                </div>
            </li>

            <li class="nav-item dropdown">
                <a href="#" class="navbar-nav-link dropdown-toggle caret-0" data-toggle="dropdown">
                    <i class="icon-bubbles4"></i>
                    <span class="d-md-none ml-2">Messages</span>
                    <span class="badge badge-mark border-pink-400 ml-auto ml-md-0"></span>
                </a>

                <div class="dropdown-menu dropdown-menu-right dropdown-content wmin-md-350">
                    <div class="dropdown-content-header">
                        <span class="font-weight-semibold">Messages</span>
                        <a href="#" class="text-default"><i class="icon-compose"></i></a>
                    </div>

                    <div class="dropdown-content-body dropdown-scrollable">
                        <ul class="media-list">
                            <li class="media">
                                <div class="mr-3 position-relative">
                                    <img src="<?= base_url('public/global_assets/images/placeholders/placeholder.jpg'); ?>" width="36" height="36" class="rounded-circle" alt="">
                                </div>

                                <div class="media-body">
                                    <div class="media-title">
                                        <a href="#">
                                            <span class="font-weight-semibold">James Alexander</span>
                                            <span class="text-muted float-right font-size-sm">04:58</span>
                                        </a>
                                    </div>

                                    <span class="text-muted">who knows, maybe that would be the best thing for me...</span>
                                </div>
                            </li>

                            <li class="media">
                                <div class="mr-3 position-relative">
                                    <img src="<?= base_url('public/global_assets/images/placeholders/placeholder.jpg'); ?>" width="36" height="36" class="rounded-circle" alt="">
                                </div>

                                <div class="media-body">
                                    <div class="media-title">
                                        <a href="#">
                                            <span class="font-weight-semibold">Margo Baker</span>
                                            <span class="text-muted float-right font-size-sm">12:16</span>
                                        </a>
                                    </div>

                                    <span class="text-muted">That was something he was unable to do because...</span>
                                </div>
                            </li>

                            <li class="media">
                                <div class="mr-3">
                                    <img src="<?= base_url('public/global_assets/images/placeholders/placeholder.jpg'); ?>" width="36" height="36" class="rounded-circle" alt="">
                                </div>
                                <div class="media-body">
                                    <div class="media-title">
                                        <a href="#">
                                            <span class="font-weight-semibold">Jeremy Victorino</span>
                                            <span class="text-muted float-right font-size-sm">22:48</span>
                                        </a>
                                    </div>

                                    <span class="text-muted">But that would be extremely strained and suspicious...</span>
                                </div>
                            </li>

                            <li class="media">
                                <div class="mr-3">
                                    <img src="<?= base_url('public/global_assets/images/placeholders/placeholder.jpg'); ?>" width="36" height="36" class="rounded-circle" alt="">
                                </div>
                                <div class="media-body">
                                    <div class="media-title">
                                        <a href="#">
                                            <span class="font-weight-semibold">Beatrix Diaz</span>
                                            <span class="text-muted float-right font-size-sm">Tue</span>
                                        </a>
                                    </div>

                                    <span class="text-muted">What a strenuous career it is that I've chosen...</span>
                                </div>
                            </li>

                            <li class="media">
                                <div class="mr-3">
                                    <img src="<?= base_url('public/global_assets/images/placeholders/placeholder.jpg'); ?>" width="36" height="36" class="rounded-circle" alt="">
                                </div>
                                <div class="media-body">
                                    <div class="media-title">
                                        <a href="#">
                                            <span class="font-weight-semibold">Richard Vango</span>
                                            <span class="text-muted float-right font-size-sm">Mon</span>
                                        </a>
                                    </div>

                                    <span class="text-muted">Other travelling salesmen live a life of luxury...</span>
                                </div>
                            </li>
                        </ul>
                    </div>

                    <div class="dropdown-content-footer bg-light">
                        <a href="#" class="text-grey mr-auto">All messages</a>
                        <div>
                            <a href="#" class="text-grey" data-popup="tooltip" title="Mark all as read"><i class="icon-radio-unchecked"></i></a>
                            <a href="#" class="text-grey ml-2" data-popup="tooltip" title="Settings"><i class="icon-cog3"></i></a>
                        </div>
                    </div>
                </div>
            </li>

            <li class="nav-item dropdown dropdown-user">
                <a href="#" class="navbar-nav-link d-flex align-items-center dropdown-toggle" data-toggle="dropdown">
                    <img src="<?= base_url('public/global_assets/images/placeholders/placeholder.jpg'); ?>" class="rounded-circle mr-2" height="34" alt="">
                    <span><?= session('e_user_name'); ?></span>
                </a>

                <div class="dropdown-menu dropdown-menu-right">
                    <a href="#" class="dropdown-item"><i class="icon-user-plus"></i> My profile</a>
                    <a href="#" class="dropdown-item"><i class="icon-coins"></i> My balance</a>
                    <a href="#" class="dropdown-item"><i class="icon-comment-discussion"></i> Messages <span class="badge badge-pill bg-indigo-400 ml-auto">58</span></a>
                    <div class="dropdown-divider"></div>
                    <a href="#" class="dropdown-item"><i class="icon-cog5"></i> Account settings</a>
                    <a href="<?= base_url('auth/logout'); ?>" class="dropdown-item"><i class="icon-switch2"></i> Logout</a>
                </div>
            </li>
        </ul>
    </div>
    <!-- /navbar content -->

</div>