<div class="page-header">
    <!-- <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Home</span> - Dashboard</h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>

        <div class="header-elements d-none text-center text-md-left mb-3 mb-md-0">
            <div class="btn-group">
                <button type="button" class="btn bg-indigo-400"><i class="icon-stack2 mr-2"></i> New report</button>
                <button type="button" class="btn bg-indigo-400 dropdown-toggle" data-toggle="dropdown"></button>
                <div class="dropdown-menu dropdown-menu-right">
                    <div class="dropdown-header">Actions</div>
                    <a href="#" class="dropdown-item"><i class="icon-file-eye"></i> View reports</a>
                    <a href="#" class="dropdown-item"><i class="icon-file-plus"></i> Edit reports</a>
                    <a href="#" class="dropdown-item"><i class="icon-file-stats"></i> Statistics</a>
                    <div class="dropdown-header">Export</div>
                    <a href="#" class="dropdown-item"><i class="icon-file-pdf"></i> Export to PDF</a>
                    <a href="#" class="dropdown-item"><i class="icon-file-excel"></i> Export to CSV</a>
                </div>
            </div>
        </div>
    </div> -->
    <div class="mb-3 border-top-1 border-top-primary">
        <div class="page-header page-header-light" style="border-left: 1px solid #ddd; border-right: 1px solid #ddd; margin-bottom: 0;">
            <div class="page-header-content header-elements-md-inline">
                <!-- <div class="page-title"> -->
                <div class="breadcrumb">
                    <h6>
                        <i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold"><?= lang('App.'.breadcrumb_title());?></span> - <?= lang('App.'.breadcrumb_head());?>
                        <!-- <small class="d-block text-muted">Basic breadcrumb inside page header</small> -->
                    </h6>
                </div>

                <div class="header-elements py-0">
                    <div class="breadcrumb">
                        <?php if (breadcrumb()->getNumRows()==3) {?>
                            <a href="<?= base_url();?>" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                            <a href="<?= base_url();?>" class="breadcrumb-item"><?= lang('App.'.breadcrumb()->getResultArray()[0]['e_menu']);?></a>
                            <a href="<?= base_url();?>" class="breadcrumb-item"><?= lang('App.'.breadcrumb()->getResultArray()[1]['e_menu']);?></a>
                            <span class="breadcrumb-item active"><?= lang('App.'.breadcrumb()->getResultArray()[2]['e_menu']);?></span>
                        <?php }elseif(breadcrumb()->getNumRows()==2){?>
                            <a href="<?= base_url();?>" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                            <a href="<?= base_url(breadcrumb()->getResultArray()[0]['e_folder']);?>" class="breadcrumb-item"><?= lang('App.'.breadcrumb()->getResultArray()[0]['e_menu']);?></a>  
                            <span class="breadcrumb-item active"><?= lang('App.'.breadcrumb()->getResultArray()[1]['e_menu']);?></span>                          
                        <?php }elseif(breadcrumb()->getNumRows()==1){?>
                            <a href="<?= base_url();?>" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                            <span class="breadcrumb-item active"><?= lang('App.'.breadcrumb()->getResultArray()[0]['e_menu']);?></span>
                        <?php }elseif(breadcrumb()->getNumRows()==0){?>
                            <a href="<?= base_url();?>" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                            <span class="breadcrumb-item active"><?= lang('App.Dashboard');?></span>
                        <?php } ?>                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>