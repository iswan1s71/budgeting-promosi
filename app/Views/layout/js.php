<!-- Core JS files -->
<script src="<?= base_url('public/global_assets/js/main/jquery.min.js'); ?>"></script>
<script src="<?= base_url('public/global_assets/js/main/bootstrap.bundle.min.js'); ?>"></script>
<script src="<?= base_url('public/global_assets/js/plugins/loaders/blockui.min.js'); ?>"></script>
<script src="<?= base_url('public/global_assets/js/plugins/ui/perfect_scrollbar.min.js'); ?>"></script>
<!-- <script src="<?= base_url('public/global_assets/js/plugins/forms/selects/select2.min.js');?>"></script> -->
<!-- /core JS files -->

<script src="<?= base_url('public/assets/js/app.js'); ?>"></script>
<script>
    // $('.select-search').select2();
    var base_url = '<?= base_url(); ?>';
    var current_link = "<?= session('current_link'); ?>";
</script>
<script src="<?= base_url('public/assets/js/custom.js'); ?>"></script>