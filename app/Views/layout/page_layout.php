<!DOCTYPE html>
<html lang="<?= session('language');?>">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Budgeting Finance | Application</title>
	<?= $this->include('layout/css') ?>
	<?= put_headers();?>
</head>

<body class="navbar-top <?= session('sidebar');?>">

	<!-- Main navbar -->
	<?= $this->include('layout/navbar') ?>
	<!-- /main navbar -->


	<!-- Page content -->
	<div class="page-content">

		<!-- Main sidebar -->
		<?= $this->include('layout/sidebar') ?>
		<!-- /main sidebar -->


		<!-- Main content -->
		<div class="content-wrapper">

			<!-- Page header -->
			<?= $this->include('layout/header') ?>
			<!-- /page header -->


			<!-- Content area -->
			<div class="content pt-0">
				<?= $this->renderSection('content') ?>
			</div>
			<!-- /content area -->


			<!-- Footer -->
			<?= $this->include('layout/footer') ?>
			<!-- /footer -->

		</div>
		<!-- /main content -->

	</div>
	<!-- /page content -->

</body>

<!-- Theme JS files -->
<?= $this->include('layout/js') ?>
<?= put_footer();?>
<!-- /theme JS files -->

</html>