	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="<?= base_url('public/global_assets/css/icons/icomoon/styles.min.css'); ?>" rel="stylesheet" type="text/css">
	<link href="<?= base_url('public/global_assets/css/extras/animate.min.css'); ?>" rel="stylesheet" type="text/css">
	<link href="<?= base_url('public/assets/css/bootstrap.min.css'); ?>" rel="stylesheet" type="text/css">
	<link href="<?= base_url('public/assets/css/bootstrap_limitless.min.css'); ?>" rel="stylesheet" type="text/css">
	<link href="<?= base_url('public/assets/css/layout.min.css'); ?>" rel="stylesheet" type="text/css">
	<link href="<?= base_url('public/assets/css/components.min.css'); ?>" rel="stylesheet" type="text/css">
	<link href="<?= base_url('public/assets/css/colors.min.css'); ?>" rel="stylesheet" type="text/css">
	<link href="<?= base_url('public/assets/css/global.css'); ?>" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->