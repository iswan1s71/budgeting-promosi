<div class="sidebar sidebar-dark sidebar-main sidebar-fixed sidebar-expand-md">
    <!-- Sidebar mobile toggler -->
    <div class="sidebar-mobile-toggler text-center">
        <a href="#" class="sidebar-mobile-main-toggle">
            <i class="icon-arrow-left8"></i>
        </a>
        Navigation
        <a href="#" class="sidebar-mobile-expand">
            <i class="icon-screen-full"></i>
            <i class="icon-screen-normal"></i>
        </a>
    </div>
    <!-- /sidebar mobile toggler -->
    <!-- Sidebar content -->
    <div class="sidebar-content">
        <!-- User menu -->
        <div class="sidebar-user">
            <div class="card-body">
                <div class="media">
                    <div class="mr-3">
                        <a href="#"><img src="<?= base_url('public/global_assets/images/placeholders/placeholder.jpg'); ?>" width="38" height="38" class="rounded-circle" alt=""></a>
                    </div>
                    <div class="media-body">
                        <div class="media-title font-weight-semibold"><?= session('e_user_name'); ?></div>
                        <div class="font-size-xs opacity-50">
                            <i class="icon-pin font-size-sm"></i> &nbsp;Santa Ana, CA
                        </div>
                    </div>
                    <div class="ml-3 align-self-center">
                        <a href="#" class="text-white"><i class="icon-cog3"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <!-- /user menu -->
        <!-- Main navigation -->
        <div class="card card-sidebar-mobile">
            <ul class="nav nav-sidebar" data-nav-type="accordion">
                <!-- Main -->
                <li class="nav-item-header">
                    <div class="text-uppercase font-size-xs line-height-xs">Main Menu</div>
                    <i class="icon-menu" title="Main"></i>
                </li>

                <?php 
                echo '<pre>';
                var_dump(get_menu()->getResult());
                echo '</pre>';
                
                ?>

                <?php foreach (get_menu()->getResult() as $key) {
                        if ($key->e_folder == '#') { ?>
                            <li class="nav-item nav-item-submenu <?php if (session('idmenu_1') == $key->i_menu) echo "nav-item-open"; ?>">
                                <a href="#" class="nav-link"><i class="<?= $key->icon; ?>" data-popup="tooltip" title="<?= lang('App.' . $key->e_menu); ?>"></i><span data-popup="tooltip" title="<?= lang('App.' . $key->e_menu); ?>"><?= lang('App.' . $key->e_menu); ?></span></a>
                                <ul class="nav nav-group-sub" data-submenu-title="<?= $key->e_menu; ?>" style="display: <?php if (session('idmenu_1') == $key->i_menu) { ?> block <?php } else { ?> none <?php } ?>;">
                                    <?php foreach (get_sub_menu($key->i_menu)->getResult() as $row) {
                                            if ($row->e_folder == '#') { ?>
                                                <li class="nav-item nav-item-submenu <?php if (session('idmenu_2') == $row->i_menu) echo "nav-item-open"; ?>">
                                                    <a href="#" class="nav-link"><i class="<?= $row->icon; ?>" data-popup="tooltip" title="<?= lang('App.' . $row->e_menu); ?>"></i> <span data-popup="tooltip" title="<?= lang('App.' . $row->e_menu); ?>"><?= lang('App.' . $row->e_menu); ?></span></a>
                                                    <ul class="nav nav-group-sub" data-submenu-title="<?= $row->e_menu; ?>" style="display: <?php if (session('idmenu_2') == $row->i_menu) { ?> block <?php } else { ?> none <?php } ?>;">
                                                        
                                                        <?php foreach (get_sub_menu($row->i_menu)->getResult() as $kuy) { ?>
                                                                <li class="nav-item"><a href="#" onclick="set_activemenu('<?= $key->i_menu; ?>','<?= $row->i_menu; ?>','<?= $kuy->i_menu; ?>', '<?= $kuy->e_folder; ?>'); return true;" class="nav-link <?php if (session('idmenu_3') == $kuy->i_menu) echo 'active'; ?>"><i class="<?= $kuy->icon; ?>" data-popup="tooltip" title="<?= lang('App.' . $kuy->e_menu); ?>"></i> <span data-popup="tooltip" title="<?= lang('App.' . $kuy->e_menu); ?>"><?= lang('App.' . $kuy->e_menu); ?></span></a></li>
                                                        <?php } ?>

                                                    </ul>
                                                </li>
                            </li>
                                        <?php } else { ?>
                                                <li class="nav-item">
                                                    <a class="nav-link <?php if (session('idmenu_3') == $row->i_menu) echo 'active'; ?>" href="#" onclick="set_activemenu('<?= $key->i_menu; ?>','<?= 'skip'; ?>','<?= $row->i_menu; ?>', '<?= $row->e_folder; ?>'); return true;">
                                                        <i class="<?= $row->icon; ?>" data-popup="tooltip" title="<?= lang('App.' . $row->e_menu); ?>"></i>
                                                        <span data-popup="tooltip" title="<?= lang('App.' . $row->e_menu); ?>"><?= lang('App.' . $row->e_menu); ?></span></a></li>

                                        <?php } ?>
                                    <?php } ?>
                                </ul>
                            </li>
                        <?php } else { ?>
                            <li class="nav-item">
                                <?php $control = $key->e_folder == 'main' ? 'home' : '/control'; ?>
                                <a href="<?= base_url($key->e_folder . $control); ?>" class="nav-link"><i class="<?= $key->icon; ?>" data-popup="tooltip" title="<?= lang('App.' . $key->e_menu); ?>"></i><span data-popup="tooltip" title="<?= lang('App.' . $key->e_menu); ?>"><?= lang('App.' . $key->e_menu); ?></span></a>
                            </li>
                        <?php } ?> 
                <?php } ?>
<li class="nav-item">
    <a href="<?= base_url('auth/logout'); ?>" class="nav-link"><i class="icon-switch2" data-popup="tooltip" title="<?= lang('App.Logout'); ?>"></i><span data-popup="tooltip" title="<?= lang('App.Logout'); ?>"><?= lang('App.Logout'); ?></span></a>
</li>
<!-- /layout -->
</ul>
        </div>
        <!-- /main navigation -->
    </div>
    <!-- /sidebar content -->
</div>