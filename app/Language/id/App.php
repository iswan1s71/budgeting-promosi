<?php

return [
   'Dashboard' => 'Beranda',
   'Setting' => 'Pengaturan',
   'Master Data' => 'Master Data',
   'Access' => 'Akses',
   'Level' => 'Tingkatan',
   'Department' => 'Departemen',
   'Language' => 'Bahasa',
   'Menu' => 'Menu',
   'Role Access' => 'Hak Akses',
   'User' => 'Pengguna',
   'Log Activity' => 'Aktifitas',
   'Company' => 'Perusahaan',
   'Logout' => 'Keluar',
   'Select Department' => 'Pilih Departemen',
];