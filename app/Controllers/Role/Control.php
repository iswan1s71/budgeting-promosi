<?php

namespace App\Controllers\Role;

use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use Psr\Log\LoggerInterface;
use App\Controllers\BaseController;


class Control extends BaseController
{
    protected $folder;
    protected $title;
    protected $icon;
    protected $i_menu;
    protected $color;
    protected $model;

    /** Constructor */
    public function initController(RequestInterface $request, ResponseInterface $response, LoggerInterface $logger)
    {
        // Do Not Edit This Line
        parent::initController($request, $response, $logger);
        if ($this->session->i_user === NULL) {
            $response->redirect(base_url('auth')); // or use $this->response->redirect(base_url('login'));
        }

        $this->folder = getSegment(1);

        $data = check_folder($this->folder, 2);

        if ($data === NULL) {
            $response->redirect(base_url());
        } 

        $this->folder = $data->getRow()->e_folder;
        $this->title  = $data->getRow()->e_menu;
        $this->icon   = $data->getRow()->icon;
        $this->i_menu = $data->getRow()->i_menu;

        $this->color = $this->session->color;
        $this->model = new \App\Models\Role\Models();
    }

    /** Default Controllers */
    public function index()
    {
        // add_css(
        //     array(
        //         '/public/global_assets/css/extras/animate.min.css',
        //     )
        // );

        add_js(
            array(
                '/public/assets/js/jquery.min.js',
                '/public/global_assets/js/vendor/tables/datatables/datatables.min.js',
                '/public/global_assets/js/vendor/tables/datatables/extensions/buttons.min.js',
                '/public/global_assets/js/vendor/tables/datatables/extensions/natural_sort.js',
                '/public/global_assets/js/vendor/notifications/sweet_alert.min.js',
                '/public/global_assets/js/vendor/forms/selects/select2.min.js',
                '/public/assets/js/role/read.js',
                '/public/assets/js/sweet_custom.js',
            )
        );

        $data = array(
            'title'  => $this->title,
            'folder' => $this->folder,
            'icon'   => $this->icon,
            'i_menu' => $this->i_menu,
        );
        $this->mmaster->log("Open Menu $this->title");
        _view($this->folder, '/read', $data);
    }

    /** Datatable */
    public function serverside()
    {
        echo $this->model->serverside($this->folder, $this->i_menu);
    }

    /** Update Form */
    public function update()
    {
        $data = check_role($this->i_menu, 3);
        if ($data) {
            if ($data->getnumRows() <= 0) {
                return redirect()->to(base_url());
            }
        }

        add_js(
            array(
                '/public/assets/js/jquery.min.js',
                '/public/global_assets/js/vendor/tables/datatables/datatables.min.js',
                '/public/global_assets/js/vendor/tables/datatables/extensions/buttons.min.js',
                '/public/global_assets/js/vendor/tables/datatables/extensions/natural_sort.js',
                '/public/global_assets/js/vendor/notifications/sweet_alert.min.js',
                '/public/global_assets/js/vendor/forms/selects/select2.min.js',
                '/public/assets/js/' . $this->folder . '/update.js',
                '/public/assets/js/sweet_custom.js',
            )
        );

        $id = decrypt_url(getSegment(4));

        $data = array(
            // 'title'  => $this->title,
            // 'folder' => $this->folder,
            // 'icon'   => $this->icon,
            // 'level'  => $this->mmaster->get_level($id)->getRow(),
            // 'menu'   => $this->model->get_menu($id),
            // 'submenu' => $this->model->get_sub_menu($id),
            'title'   => $this->title,
            'folder'  => $this->folder,
            'icon'    => $this->icon,
            'level'   => $this->mmaster->get_level($id)->getRow(),
            'data'    => $this->model->get_data($id),
            'access'  => $this->model->get_access($id),
            'id' => $id
        );
        $this->mmaster->log("Open Form Update $this->title");
        _view($this->folder, '/update', $data);
    }

    /** Update Data */
    public function act_update()
    {
        $data = check_role($this->i_menu, 3);
        if ($data) {
            if ($data->getnumRows() <= 0) {
                return redirect()->to(base_url());
            }
        }

        if (!$this->validate([
            'i_level' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'Level required.'
                ]
            ],            
        ])) {
            $data = array(
                'message' => $this->validator->listErrors(),
                'success' => FALSE,
                'ready'   => FALSE,
            );
        } else {
            $i_level = $this->request->getVar('i_level');
            $array_access = $this->request->getVar('access');            

            /** Transaction */
            $this->db->transStart();
            
            $this->model->delete_user_role($i_level);

            foreach ($array_access as $key => $access) {
                $i_menu = $key;
                foreach ($access as $i_access => $value) {
                    $this->model->insert_user_role($i_menu, $i_level, $i_access);
                }
            }

            // $this->model->act_update($i_level, $i_sub_menu);
            $this->db->transComplete();
            if ($this->db->transStatus() === false) {
                $data = array(
                    'message' => NULL,
                    'success' => FALSE,
                    'ready'   => FALSE,
                    'data'    => "$this->title : $i_level",
                );
            } else {
                $this->mmaster->log("Edit Data $this->title Name : $i_level");
                $data = array(
                    'message' => NULL,
                    'success' => TRUE,
                    'ready'   => FALSE,
                    'data'    => "$this->title : $i_level",
                );
            }
        }
        echo json_encode($data);
    }

    /** View Form */
    public function view()
    {
        $data = check_role($this->i_menu, 2);
        if ($data) {
            if ($data->getnumRows() <= 0) {
                return redirect()->to(base_url());
            }
        }

        add_js(
            array(
                '/public/global_assets/js/plugins/forms/styling/uniform.min.js',
                '/public/assets/js/' . $this->folder . '/view.js',
            )
        );

        $id = decrypt_url(getSegment(4));
        $data = array(
            'title'   => $this->title,
            'folder'  => $this->folder,
            'icon'    => $this->icon,
            'level'   => $this->mmaster->get_level($id)->getRow(),
            'data'    => $this->model->get_data($id),
            'access'  => $this->model->get_access($id),
        );
        $this->mmaster->log("Open Form View $this->title");
        _view($this->folder, '/view', $data);
    }
}
