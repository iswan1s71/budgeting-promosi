<?php

namespace App\Controllers\Menu;

use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use Psr\Log\LoggerInterface;
use App\Controllers\BaseController;

class Control extends BaseController
{
    protected $folder;
    protected $title;
    protected $icon;
    protected $i_menu;
    protected $color;
    protected $model;

    /** Constructor */
    public function initController(RequestInterface $request, ResponseInterface $response, LoggerInterface $logger)
    {
        // Do Not Edit This Line
        parent::initController($request, $response, $logger);
        if ($this->session->i_user === NULL) {
            $response->redirect(base_url('auth')); // or use $this->response->redirect(base_url('login'));
        }

        $this->folder = getSegment(1);

        $data = check_folder($this->folder, 2);

        if ($data === NULL) {
            $response->redirect(base_url());
        } 

        $this->folder = $data->getRow()->e_folder;
        $this->title  = $data->getRow()->e_menu;
        $this->icon   = $data->getRow()->icon;
        $this->i_menu = $data->getRow()->i_menu;

        $this->color = $this->session->color;
        $this->model = new \App\Models\Menu\Models();
    }

    /** Default Controllers */
    public function index()
    {
        add_js(
            array(
                '/public/assets/js/jquery.min.js',
                '/public/global_assets/js/vendor/tables/datatables/datatables.min.js',
                '/public/global_assets/js/vendor/tables/datatables/extensions/buttons.min.js',
                '/public/global_assets/js/vendor/tables/datatables/extensions/natural_sort.js',
                '/public/global_assets/js/vendor/notifications/sweet_alert.min.js',
                '/public/global_assets/js/vendor/forms/selects/select2.min.js',
                '/public/assets/js/datatable.js',
                '/public/assets/js/sweet_custom.js',
            )
        );

        $data = array(
            'title'  => $this->title,
            'folder' => $this->folder,
            'icon'   => $this->icon,
            'i_menu' => $this->i_menu,
        );
        $this->mmaster->log("Open Menu $this->title");
        _view($this->folder, '/read', $data);
    }

    /** Datatable */
    public function serverside()
    {
        echo $this->model->serverside($this->folder, $this->i_menu);
    }

    /** Create Form */
    public function create()
    {
        $data = check_role($this->i_menu, 1);
        if ($data) {
            if ($data->getnumRows() <= 0) {
                return redirect()->to(base_url());
            }
        }

        add_js(
            array(
                '/public/assets/js/jquery.min.js',
                '/public/global_assets/js/vendor/notifications/sweet_alert.min.js',
                '/public/global_assets/js/vendor/forms/selects/select2.min.js',
                '/public/assets/js/create.js',
            )
        );

        $data = array(
            'title'  => $this->title,
            'folder' => $this->folder,
            'icon'   => $this->icon,
            'access' => $this->model->get_access(),
        );
        $this->mmaster->log("Open Form Create $this->title");
        _view($this->folder, '/create', $data);
    }

    /** Save Data */
    public function act_create()
    {
        $data = check_role($this->i_menu, 1);
        if ($data) {
            if ($data->getnumRows() <= 0) {
                return redirect()->to(base_url());
            }
        }

        if (!$this->validate([
            'i_parent' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'Parent required.'
                ]
            ],
            'i_menu' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'Menu Code required.'
                ]
            ],
            'e_menu' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'Menu Name required.'
                ]
            ],
            'n_urut' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'Order No required.'
                ]
            ],
            'e_folder' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'Folder required.'
                ]
            ],
            'icon' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'Icon required.'
                ]
            ],
            'i_access' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'Access required.'
                ]
            ],
        ])) {
            $data = array(
                'message' => $this->validator->listErrors(),
                'success' => FALSE,
                'ready'   => FALSE,
            );
        } else {
            $i_parent = capitalize(no_petik($this->request->getVar('i_parent')));
            $i_menu = capitalize(no_petik($this->request->getVar('i_menu')));
            $e_menu = capitalize(no_petik($this->request->getVar('e_menu')));
            $n_urut = no_petik($this->request->getVar('n_urut'));
            $e_folder = lower(no_petik($this->request->getVar('e_folder')));
            $icon = lower(no_petik($this->request->getVar('icon')));
            $i_access = $this->request->getVar('i_access[]');
            /** Check Already */
            $check = $this->model->check($i_menu);
            if ($check->getnumRows() > 0) {
                $data = array(
                    'message' => NULL,
                    'success' => FALSE,
                    'ready'   => TRUE,
                );
            } else {
                /** Transaction */
                $this->db->transStart();
                $this->model->create($i_parent, $i_menu, $e_menu, $n_urut, $e_folder, $icon, $i_access);
                $this->db->transComplete();
                if ($this->db->transStatus() === false) {
                    $data = array(
                        'message' => NULL,
                        'success' => FALSE,
                        'ready'   => FALSE,
                        'data'    => "$this->title : $e_menu",
                    );
                } else {
                    $this->mmaster->log("Save Data $this->title Name : $e_menu");
                    $data = array(
                        'message' => NULL,
                        'success' => TRUE,
                        'ready'   => FALSE,
                        'data'    => "$this->title : $e_menu",
                    );
                }
            }
        }
        return $this->response->setJSON($data);
        // echo json_encode($data);
    }

    /** Get Menu */
	public function get_menu()
    {
       	$filter = [];
		$filter[] = array(
			'id'   => 0,
			'text' => "# - Default",
		);
        $data = $this->model->get_menu(no_petik($this->request->getVar('q')));
        foreach ($data->getResult() as $row) {
            $filter[] = array(
                'id'   => $row->i_menu,
                'text' => $row->i_menu.' - '.$row->e_menu,
            );
        }
        echo json_encode($filter);
            
    }

    /** Update Form */
    public function update()
    {
        $data = check_role($this->i_menu, 3);
        if ($data) {
            if ($data->getnumRows() <= 0) {
                return redirect()->to(base_url());
            }
        }

        add_js(
            array(
                '/public/assets/js/jquery.min.js',
                '/public/global_assets/js/vendor/notifications/sweet_alert.min.js',
                '/public/global_assets/js/vendor/forms/selects/select2.min.js',
                '/public/assets/js/update.js',
            )
        );

        $id = decrypt_url(getSegment(4));

        $data = array(
            'title'    => $this->title,
            'folder'   => $this->folder,
            'icon'     => $this->icon,
            'access'   => $this->model->get_access(),
            'data'     => $this->model->get_edit($id)->getRow(),
        );
        $this->mmaster->log("Open Form Update $this->title");
        _view($this->folder, '/update', $data);
    }

    /** Update Data */
    public function act_update()
    {
        $data = check_role($this->i_menu, 3);
        if ($data) {
            if ($data->getnumRows() <= 0) {
                return redirect()->to(base_url());
            }
        }

        if (!$this->validate([
            'i_parent' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'Parent required.'
                ]
            ],
            'i_menu_old' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'Menu Code Old required.'
                ]
            ],
            'i_menu' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'Menu Code required.'
                ]
            ],
            'e_menu' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'Menu Name required.'
                ]
            ],
            'n_urut' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'Order No required.'
                ]
            ],
            'e_folder' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'Folder required.'
                ]
            ],
            'icon' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'Icon required.'
                ]
            ],
            'i_access' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'Access required.'
                ]
            ],
        ])) {
            $data = array(
                'message' => $this->validator->listErrors(),
                'success' => FALSE,
                'ready'   => FALSE,
            );
        } else {
            $i_parent = capitalize(no_petik($this->request->getVar('i_parent')));
            $i_menu = capitalize(no_petik($this->request->getVar('i_menu')));
            $i_menu_old = capitalize(no_petik($this->request->getVar('i_menu_old')));
            $e_menu = capitalize(no_petik($this->request->getVar('e_menu')));
            $n_urut = no_petik($this->request->getVar('n_urut'));
            $e_folder = lower(no_petik($this->request->getVar('e_folder')));
            $icon = lower(no_petik($this->request->getVar('icon')));
            $i_access = $this->request->getVar('i_access[]');
            /** Check Already */
            $check = $this->model->check_edit($i_menu, $i_menu_old);
            if ($check->getnumRows() > 0) {
                $data = array(
                    'message' => NULL,
                    'success' => FALSE,
                    'ready'   => TRUE,
                );
            } else {
                /** Transaction */
                $this->db->transStart();
                $this->model->act_update($i_parent, $i_menu, $e_menu, $n_urut, $e_folder, $icon, $i_access, $i_menu_old);
                $this->db->transComplete();
                if ($this->db->transStatus() === false) {
                    $data = array(
                        'message' => NULL,
                        'success' => FALSE,
                        'ready'   => FALSE,
                        'data'    => "$this->title : $i_menu",
                    );
                } else {
                    $this->mmaster->log("Edit Data $this->title ID : $i_menu");
                    $data = array(
                        'message' => NULL,
                        'success' => TRUE,
                        'ready'   => FALSE,
                        'data'    => "$this->title : $i_menu",
                    );
                }
            }
        }
        return $this->response->setJSON($data);
    }

    /** Update Status */
    public function change_status()
    {
        /** Cek Hak Akses, Apakah User Bisa Edit */
        $data = check_role($this->i_menu, 3);
        if ($data) {
            if ($data->getnumRows() <= 0) {
                return redirect()->to(base_url());
            }
        }

        $id = decrypt_url($this->request->getVar('id'));
        /** Transactions */
        $this->db->transStart();
        $this->model->change_status($id);
        $this->db->transComplete();
        if ($this->db->transStatus() === false) {
            $data = array(
                'success' => FALSE,
            );
        } else {
            $this->mmaster->log("Change Status $this->title i_level : $id");
            $data = array(
                'success' => TRUE,
            );
        }
        echo json_encode($data);
    }

    /** Delete Data */
    public function act_delete()
    {
        /** Cek Hak Akses, Apakah User Bisa Edit */
        $data = check_role($this->i_menu, 4);
        if ($data) {
            if ($data->getnumRows() <= 0) {
                return redirect()->to(base_url());
            }
        }

        $id = decrypt_url($this->request->getVar('id'));
        /** Transactions */
        $this->db->transStart();
        $this->model->act_delete($id);
        $this->db->transComplete();
        if ($this->db->transStatus() === false) {
            $data = array(
                'success' => FALSE,
            );
        } else {
            $this->mmaster->log("Delete $this->title i_menu : $id");
            $data = array(
                'success' => TRUE,
            );
        }
        echo json_encode($data);
    }

    public function act_soft_delete()
    {
        /** Cek Hak Akses, Apakah User Bisa Edit */
        $data = check_role($this->i_menu, 4);
        if ($data) {
            if ($data->getnumRows() <= 0) {
                return redirect()->to(base_url());
            }
        }

        $id = decrypt_url($this->request->getVar('id'));
        /** Transactions */
        $this->db->transStart();
        $this->model->act_soft_delete($id);
        $this->db->transComplete();
        if ($this->db->transStatus() === false) {
            $data = array(
                'success' => FALSE,
            );
        } else {
            $this->mmaster->log("Delete $this->title i_menu : $id");
            $data = array(
                'success' => TRUE,
            );
        }
        echo json_encode($data);
    }
}
