<?php

namespace App\Controllers\Access;

use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use Psr\Log\LoggerInterface;
use App\Controllers\BaseController;
use App\Models\Access\Models;

class Control extends BaseController
{
    /** Constructor */
    public function initController(RequestInterface $request, ResponseInterface $response, LoggerInterface $logger)
    {
        // Do Not Edit This Line
        parent::initController($request, $response, $logger);
        if ($this->session->i_user === NULL) {
            $response->redirect(base_url('auth')); // or use $this->response->redirect(base_url('login'));
        }

        $this->folder = getSegment(1);
        $data = check_folder($this->folder, 2);
        if ($data === NULL) {
            $response->redirect(base_url());
            $this->folder = '';
            $this->title  = '';
            $this->icon   = '';
            $this->i_menu = '';
        } else {
            $this->folder = $data->getRow()->e_folder;
            $this->title  = $data->getRow()->e_menu;
            $this->icon   = $data->getRow()->icon;
            $this->i_menu = $data->getRow()->i_menu;
        }
        $this->color = $this->session->color;
        $this->model = new Models();
    }

    /** Default Controllers */
    public function index()
    {
        add_js(
            array(
                '/public/assets/js/jquery.min.js',
                '/public/global_assets/js/vendor/tables/datatables/datatables.min.js',
                // '/public/assets/js/' . $this->folder . '/read.js',
                '/public/assets/js/datatable.js',
            )
        );

        $data = array(
            'title'  => $this->title,
            'folder' => $this->folder,
            'i_menu' => $this->i_menu,
            'icon'   => $this->icon,
        );
        $this->mmaster->log("Open Form $this->title");
        _view($this->folder, '/read', $data);
    }

    /** Datatable */
    public function serverside()
    {
        echo $this->model->serverside();
    }
}
