<?php

namespace App\Controllers\Companytype;

use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use Psr\Log\LoggerInterface;
use App\Controllers\BaseController;
use App\Models\Companytype\Models;

class Control extends BaseController
{
    /** Constructor */
    public function initController(RequestInterface $request, ResponseInterface $response, LoggerInterface $logger)
    {
        // Do Not Edit This Line
        parent::initController($request, $response, $logger);
        if ($this->session->i_user === NULL) {
            $response->redirect(base_url('auth')); // or use $this->response->redirect(base_url('login'));
        }

        $this->folder = getSegment(1);
        $data = check_folder($this->folder, 2);
        if ($data === NULL) {
            $response->redirect(base_url());
            $this->folder = '';
            $this->title  = '';
            $this->icon   = '';
            $this->i_menu = '';
        } else {
            $this->folder = $data->getRow()->e_folder;
            $this->title  = $data->getRow()->e_menu;
            $this->icon   = $data->getRow()->icon;
            $this->i_menu = $data->getRow()->i_menu;
        }
        $this->color = $this->session->color;
        $this->model = new Models();
    }

    /** Default Controllers */
    public function index()
    {
        add_js(
            array(
                '/public/global_assets/js/plugins/tables/datatables/datatables.min.js',
                '/public/global_assets/js/plugins/tables/datatables/extensions/buttons.min.js',
                '/public/global_assets/js/plugins/tables/datatables/extensions/natural_sort.js',
                '/public/global_assets/js/plugins/notifications/sweet_alert.min.js',
                '/public/global_assets/js/plugins/forms/selects/select2.min.js',
                '/public/assets/js/' . $this->folder . '/read.js',
            )
        );

        $data = array(
            'title'  => $this->title,
            'folder' => $this->folder,
            'icon'   => $this->icon,
            'i_menu' => $this->i_menu,
        );
        $this->mmaster->log("Open Menu $this->title");
        _view($this->folder, '/read', $data);
    }

    /** Datatable */
    public function serverside()
    {
        echo $this->model->serverside($this->folder, $this->i_menu);
    }

    /** Create Form */
    public function create()
    {
        $data = check_role($this->i_menu, 1);
        if ($data) {
            if ($data->getnumRows() <= 0) {
                return redirect()->to(base_url());
            }
        }

        add_js(
            array(
                '/public/global_assets/js/plugins/forms/validation/validate.min.js',
                '/public/global_assets/js/plugins/forms/styling/uniform.min.js',
                '/public/global_assets/js/plugins/notifications/sweet_alert.min.js',
                '/public/assets/js/' . $this->folder . '/create.js',
            )
        );

        $data = array(
            'title'  => $this->title,
            'folder' => $this->folder,
            'icon'   => $this->icon,
        );
        $this->mmaster->log("Open Form Create $this->title");
        _view($this->folder, '/create', $data);
    }

    /** Save Data */
    public function act_create()
    {
        $data = check_role($this->i_menu, 1);
        if ($data) {
            if ($data->getnumRows() <= 0) {
                return redirect()->to(base_url());
            }
        }

        if (!$this->validate([
            'e_name' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'Level Name required.'
                ]
            ],
        ])) {
            $data = array(
                'message' => $this->validator->listErrors(),
                'success' => FALSE,
                'ready'   => FALSE,
            );
        } else {
            $e_name = capitalize(no_petik($this->request->getVar('e_name')));
            /** Check Already */
            $check = $this->model->check($e_name);
            if ($check->getnumRows() > 0) {
                $data = array(
                    'message' => NULL,
                    'success' => FALSE,
                    'ready'   => TRUE,
                );
            } else {
                /** Transaction */
                $this->db->transStart();
                $this->model->create($e_name);
                $this->db->transComplete();
                if ($this->db->transStatus() === false) {
                    $data = array(
                        'message' => NULL,
                        'success' => FALSE,
                        'ready'   => FALSE,
                        'data'    => "$this->title : $e_name",
                    );
                } else {
                    $this->mmaster->log("Save Data $this->title Name : $e_name");
                    $data = array(
                        'message' => NULL,
                        'success' => TRUE,
                        'ready'   => FALSE,
                        'data'    => "$this->title : $e_name",
                    );
                }
            }
        }
        echo json_encode($data);
    }

    /** Update Form */
    public function update()
    {
        $data = check_role($this->i_menu, 3);
        if ($data) {
            if ($data->getnumRows() <= 0) {
                return redirect()->to(base_url());
            }
        }

        add_js(
            array(
                '/public/global_assets/js/plugins/forms/validation/validate.min.js',
                '/public/global_assets/js/plugins/forms/styling/uniform.min.js',
                '/public/global_assets/js/plugins/notifications/sweet_alert.min.js',
                '/public/assets/js/' . $this->folder . '/update.js',
            )
        );

        $id = decrypt_url(getSegment(4));

        $data = array(
            'title'  => $this->title,
            'folder' => $this->folder,
            'icon'   => $this->icon,
            'data'   => $this->model->get_edit($id)->getRow(),
        );
        $this->mmaster->log("Open Form Update $this->title");
        _view($this->folder, '/update', $data);
    }

    /** Update Data */
    public function act_update()
    {
        $data = check_role($this->i_menu, 3);
        if ($data) {
            if ($data->getnumRows() <= 0) {
                return redirect()->to(base_url());
            }
        }

        if (!$this->validate([
            'e_name' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'Level Name required.'
                ]
            ],
        ])) {
            $data = array(
                'message' => $this->validator->listErrors(),
                'success' => FALSE,
                'ready'   => FALSE,
            );
        } else {
            $id = $this->request->getVar('id');
            $e_name = capitalize(no_petik($this->request->getVar('e_name')));
            $e_name_old = $this->request->getVar('e_name_old');
            /** Check Already */
            $check = $this->model->check_edit($e_name, $e_name_old);
            if ($check->getnumRows() > 0) {
                $data = array(
                    'message' => NULL,
                    'success' => FALSE,
                    'ready'   => TRUE,
                );
            } else {
                /** Transaction */
                $this->db->transStart();
                $this->model->act_update($id, $e_name);
                $this->db->transComplete();
                if ($this->db->transStatus() === false) {
                    $data = array(
                        'message' => NULL,
                        'success' => FALSE,
                        'ready'   => FALSE,
                        'data'    => "$this->title : $e_name",
                    );
                } else {
                    $this->mmaster->log("Edit Data $this->title Name : $e_name");
                    $data = array(
                        'message' => NULL,
                        'success' => TRUE,
                        'ready'   => FALSE,
                        'data'    => "$this->title : $e_name",
                    );
                }
            }
        }
        echo json_encode($data);
    }

    /** Update Status */
    public function change_status()
    {
        /** Cek Hak Akses, Apakah User Bisa Edit */
        $data = check_role($this->i_menu, 3);
        if ($data) {
            if ($data->getnumRows() <= 0) {
                return redirect()->to(base_url());
            }
        }

        $id = decrypt_url($this->request->getVar('id'));
        /** Transactions */
        $this->db->transStart();
        $this->model->change_status($id);
        $this->db->transComplete();
        if ($this->db->transStatus() === false) {
            $data = array(
                'success' => FALSE,
            );
        } else {
            $this->mmaster->log("Change Status $this->title i_level : $id");
            $data = array(
                'success' => TRUE,
            );
        }
        echo json_encode($data);
    }
}
