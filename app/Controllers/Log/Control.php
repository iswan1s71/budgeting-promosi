<?php

namespace App\Controllers\Log;

use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use Psr\Log\LoggerInterface;
use App\Controllers\BaseController;

class Control extends BaseController
{
    /** Constructor */
    public function initController(RequestInterface $request, ResponseInterface $response, LoggerInterface $logger)
    {
        // Do Not Edit This Line
        parent::initController($request, $response, $logger);
        if ($this->session->i_user === NULL) {
            $response->redirect(base_url('auth')); // or use $this->response->redirect(base_url('login'));
        }

        $this->folder = getSegment(1);
        $data = check_folder($this->folder, 2);
        if ($data === NULL) {
            $response->redirect(base_url());
            $this->folder = '';
            $this->title  = '';
            $this->icon   = '';
            $this->i_menu = '';
        } else {
            $this->folder = $data->getRow()->e_folder;
            $this->title  = $data->getRow()->e_menu;
            $this->icon   = $data->getRow()->icon;
            $this->i_menu = $data->getRow()->i_menu;
        }
        $this->color = $this->session->color;
    }

    /** Default Controllers */
    public function index()
    {
        add_js(
            array(
                '/public/global_assets/js/plugins/tables/datatables/datatables.min.js',
                '/public/global_assets/js/plugins/tables/datatables/extensions/buttons.min.js',
                '/public/global_assets/js/plugins/tables/datatables/extensions/natural_sort.js',
                '/public/global_assets/js/plugins/notifications/sweet_alert.min.js',
                '/public/global_assets/js/plugins/forms/selects/select2.min.js',
                '/public/assets/js/' . $this->folder . '/read.js',
            )
        );

        $data = array(
            'title'  => $this->title,
            'folder' => $this->folder,
            'icon'   => $this->icon,
        );
        $this->mmaster->log("Open Form $this->title");
        _view($this->folder, '/read', $data);
    }

    /** Datatable */
    public function serverside()
    {
        echo $this->mmaster->serverside();
    }
}
