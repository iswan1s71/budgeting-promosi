<?php

namespace App\Controllers\User;

use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use Psr\Log\LoggerInterface;
use App\Controllers\BaseController;
use App\Models\Company\Models;
use phpDocumentor\Reflection\PseudoTypes\False_;
use phpDocumentor\Reflection\PseudoTypes\True_;

class Control extends BaseController
{
    protected $folder;
    protected $title;
    protected $icon;
    protected $i_menu;
    protected $color;
    protected $model;

    /** Constructor */
    public function initController(RequestInterface $request, ResponseInterface $response, LoggerInterface $logger)
    {
        // Do Not Edit This Line
        parent::initController($request, $response, $logger);
        if ($this->session->i_user === NULL) {
            $response->redirect(base_url('auth')); // or use $this->response->redirect(base_url('login'));
        }

        $this->folder = getSegment(1);

        $data = check_folder($this->folder, 2);

        if ($data === NULL) {
            $response->redirect(base_url());
        } 

        $this->folder = $data->getRow()->e_folder;
        $this->title  = $data->getRow()->e_menu;
        $this->icon   = $data->getRow()->icon;
        $this->i_menu = $data->getRow()->i_menu;

        $this->color = $this->session->color;
        $this->model = new \App\Models\User\Models();
    }

    /** Default Controllers */
    public function index()
    {
        // add_css(
        //     array(
        //         '/public/global_assets/css/extras/animate.min.css',
        //     )
        // );

        add_js(
            array(
                '/public/assets/js/jquery.min.js',
                '/public/global_assets/js/vendor/tables/datatables/datatables.min.js',
                '/public/global_assets/js/vendor/tables/datatables/extensions/buttons.min.js',
                '/public/global_assets/js/vendor/tables/datatables/extensions/natural_sort.js',
                '/public/global_assets/js/vendor/notifications/sweet_alert.min.js',
                '/public/global_assets/js/vendor/forms/selects/select2.min.js',
                '/public/assets/js/user/read.js',
                '/public/assets/js/sweet_custom.js',
            )
        );

        $data = array(
            'title'  => $this->title,
            'folder' => $this->folder,
            'icon'   => $this->icon,
            'i_menu' => $this->i_menu,
        );
        $this->mmaster->log("Open Menu $this->title");
        _view($this->folder, '/read', $data);
    }

    /** Datatable */
    public function serverside()
    {
        echo $this->model->serverside($this->folder, $this->i_menu);
    }

    /** Create Form */
    public function create()
    {
        $data = check_role($this->i_menu, 1);
        if ($data) {
            if ($data->getnumRows() <= 0) {
                return redirect()->to(base_url());
            }
        }

        add_js(
            array(
                '/public/assets/js/jquery.min.js',
                '/public/global_assets/js/vendor/notifications/sweet_alert.min.js',
                '/public/global_assets/js/vendor/forms/selects/select2.min.js',                
                '/public/assets/js/' . $this->folder . '/create.js?v=' . strtotime(date('Y-m-d H:i:s')),
                '/public/assets/js/create.js',
            )
        );

        $data = array(
            'title'  => $this->title,
            'folder' => $this->folder,
            'icon'   => $this->icon,
        );
        $this->mmaster->log("Open Form Create $this->title");
        _view($this->folder, '/create', $data);
    }

    public function view()
    {
        add_js(
            array(
                '/public/assets/js/jquery.min.js',
                '/public/global_assets/js/vendor/tables/datatables/datatables.min.js',
                '/public/global_assets/js/vendor/tables/datatables/extensions/buttons.min.js',
                '/public/global_assets/js/vendor/tables/datatables/extensions/natural_sort.js',
                '/public/global_assets/js/vendor/notifications/sweet_alert.min.js',
                '/public/global_assets/js/vendor/forms/selects/select2.min.js',
                '/public/assets/js/' . $this->folder . '/view.js',
            )
        );

        $id = decrypt_url($this->request->getVar('id'));
        $model = $this->model->get_edit($id)->getRow();

        $userCompany = new \App\Models\Usercompany\Models();
        $allUserCompany = $userCompany->get_by_i_user($id);

        $userLevel = new \App\Models\Userlevel\Models();
        $allUserLevel = $userLevel->get_by_i_user($id);

        // var_dump($allUserLevel->getResult());

        $data = array(
            'title'  => $this->title,
            'folder' => $this->folder,
            'icon'   => $this->icon,
            'i_menu' => $this->i_menu,
            'model' => $model,
            'all_user_company' => $allUserCompany,
            'all_user_level' => $allUserLevel,
        );
        $this->mmaster->log("Open Menu $this->title");
        _view($this->folder, '/view', $data);
    }

    /** Save Data */
    public function act_create()
    {
        $data = check_role($this->i_menu, 1);
        if ($data) {
            if ($data->getnumRows() <= 0) {
                return redirect()->to(base_url());
            }
        }

        if (!$this->validate([
            'iduser' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'iduser Name required.'
                ]
            ],
        ])) {
            $data = array(
                'message' => $this->validator->listErrors(),
                'success' => FALSE,
                'ready'   => FALSE,
            );
        } else {
            $iduser = $this->request->getPost('iduser');
            $username = $this->request->getPost('username');
            $password = $this->request->getPost('password');
            $nama = $this->request->getPost('nama');
            $e_department_name = $this->request->getPost('e_department_name');
            $e_cabang_name = $this->request->getPost('e_cabang_name');
            $levels = $this->request->getPost('levels');
            $companies = $this->request->getPost('companies');

            /** Check Already */
            $check = $this->model->check($iduser);
            if ($check->getnumRows() > 0) {
                $data = array(
                    'message' => NULL,
                    'success' => FALSE,
                    'ready'   => TRUE,
                );
            } else {
                /** Transaction */
                $password_hashed = encrypt_password($password);
                
                $this->db->transStart();
                $this->model->create($iduser, $username, $password_hashed, $nama);

                foreach ($levels as $level) {
                    $model = new \App\Models\Userlevel\Models();                    
                    $model->create($iduser, $level);
                }

                foreach ($companies as $company) {
                    $model = new \App\Models\Usercompany\Models();                    
                    $model->create($iduser, $company);
                }

                $this->db->transComplete();
                if ($this->db->transStatus() === false) {
                    // $this->response->setStatusCode(230, 'Tardis initiated');
                    $data = array(
                        'message' => NULL,
                        'success' => FALSE,
                        'ready'   => FALSE,
                        'data'    => "$this->title : $username",
                    );
                } else {
                    $this->mmaster->log("Save Data $this->title Name : $username");
                    $data = array(
                        'message' => NULL,
                        'success' => TRUE,
                        'ready'   => FALSE,
                        'data'    => "$this->title : $username",
                    );
                }
            }
        }

        // die('here');
        // echo json_encode($data);
        return $this->response->setJSON($data);
        // return _JSON($data);$this->response->setJSON($data);
    }

    /** Update Form */
    public function update()
    {
        $data = check_role($this->i_menu, 3);
        if ($data) {
            if ($data->getnumRows() <= 0) {
                return redirect()->to(base_url());
            }
        }

        add_js(
            array(
                '/public/assets/js/jquery.min.js',
                '/public/global_assets/js/vendor/notifications/sweet_alert.min.js',
                '/public/global_assets/js/vendor/forms/selects/select2.min.js',
                '/public/global_assets/js/vendor/tables/datatables/datatables.min.js',
                '/public/global_assets/js/vendor/tables/datatables/extensions/buttons.min.js',
                '/public/global_assets/js/vendor/tables/datatables/extensions/natural_sort.js',
                '/public/assets/js/update.js',
                '/public/assets/js/' . $this->folder . '/update.js',
            )
        );

        $id = decrypt_url($this->request->getVar('id'));
        $model = $this->model->get_edit($id)->getRow();

        $userCompany = new \App\Models\Usercompany\Models();
        $allUserCompany = $userCompany->get_by_i_user($id);

        $userLevel = new \App\Models\Userlevel\Models();
        $allUserLevel = $userLevel->get_by_i_user($id);

        $data = array(
            'title'  => $this->title,
            'folder' => $this->folder,
            'icon'   => $this->icon,
            'model' => $model,
            'all_user_company' => $allUserCompany,
            'all_user_level' => $allUserLevel,
        );
        $this->mmaster->log("Open Form Update $this->title");
        _view($this->folder, '/update', $data);
    }

    /** Update Data */
    public function act_update()
    {
        $data = check_role($this->i_menu, 3);
        if ($data) {
            if ($data->getnumRows() <= 0) {
                return redirect()->to(base_url());
            }
        }

        if (!$this->validate([
            'i_user' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'i_user Name required.'
                ]
            ],
        ])) {
            $data = array(
                'message' => $this->validator->listErrors(),
                'success' => FALSE,
                'ready'   => FALSE,
            );
        } else {
            $i_user = $this->request->getPost('i_user');
            $password = $this->request->getPost('password');
            $i_user_code = $this->request->getPost('i_user_code');
            $f_active = $this->request->getPost('f_active');
            $levels = $this->request->getPost('levels');
            $companies = $this->request->getPost('companies');

            $hashed_old_password = \App\Models\User\Models::get_hashed_old_password($i_user);

            if ($password == $hashed_old_password) {
                $password = $hashed_old_password;
            } else {
                /** hash new password */
                $password = encrypt_password($password);
            }      
                    
            if ($f_active == 't') {
                $f_active = true;
            } else {
                $f_active = false;
            }
            
            /** Transaction */
            $this->db->transStart();
            $this->model->act_update($i_user, $i_user_code, $password, $f_active);

            /** delete insert user_level */
            $userLevel = new \App\Models\Userlevel\Models();
            $userLevel->delete_by_i_user($i_user);

            foreach ($levels as $level) {
                $model = new \App\Models\UserLevel\Models();                    
                $model->create($i_user, $level);
            }

            /** delete insert user_company */
            $userCompany = new \App\Models\Usercompany\Models();
            $userCompany->delete_by_i_user($i_user);

            foreach ($companies as $company) {
                $model = new \App\Models\Usercompany\Models();                    
                $model->create($i_user, $company);
            }

            $this->db->transComplete();
            if ($this->db->transStatus() === false) {
                $data = array(
                    'message' => NULL,
                    'success' => FALSE,
                    'ready'   => FALSE,
                    'data'    => "$this->title : $i_user_code",
                );
            } else {
                $this->mmaster->log("Edit Data $this->title Name : $i_user_code");
                $data = array(
                    'message' => NULL,
                    'success' => TRUE,
                    'ready'   => FALSE,
                    'data'    => "$this->title : $i_user_code",
                );
            }
        }
        // echo json_encode($data);
        return $this->response->setJSON($data);
    }

    /** Update Status */
    public function change_status()
    {
        /** Cek Hak Akses, Apakah User Bisa Edit */
        $data = check_role($this->i_menu, 3);
        if ($data) {
            if ($data->getnumRows() <= 0) {
                return redirect()->to(base_url());
            }
        }

        $id = decrypt_url($this->request->getVar('id'));
        /** Transactions */
        $this->db->transStart();
        $this->model->change_status($id);
        $this->db->transComplete();
        if ($this->db->transStatus() === false) {
            $data = array(
                'success' => FALSE,
            );
        } else {
            $this->mmaster->log("Change Status $this->title i_level : $id");
            $data = array(
                'success' => TRUE,
            );
        }
        echo json_encode($data);
    }

    /** Delete Data */
    public function act_delete()
    {
        /** Cek Hak Akses, Apakah User Bisa Edit */
        $data = check_role($this->i_menu, 4);
        if ($data) {
            if ($data->getnumRows() <= 0) {
                return redirect()->to(base_url());
            }
        }

        $id = decrypt_url($this->request->getVar('id'));
        /** Transactions */
        $this->db->transStart();
        $this->model->act_delete($id);
        $this->db->transComplete();
        if ($this->db->transStatus() === false) {
            $data = array(
                'success' => FALSE,
            );
        } else {
            $this->mmaster->log("Delete $this->title i_menu : $id");
            $data = array(
                'success' => TRUE,
            );
        }
        echo json_encode($data);
    }

    public function toggle_status()
    {
        /** Cek Hak Akses, Apakah User Bisa Edit */
        $data = check_role($this->i_menu, 3);
        if ($data) {
            if ($data->getnumRows() <= 0) {
                return redirect()->to(base_url());
            }
        }

        $id = decrypt_url($this->request->getVar('id'));
        /** Transactions */
        $this->db->transStart();
        $this->model->toggle_status($id);
        $this->db->transComplete();
        if ($this->db->transStatus() === false) {
            $data = array(
                'success' => FALSE,
            );
        } else {
            $this->mmaster->log("Change Status $this->title i_level : $id");
            $data = array(
                'success' => TRUE,
            );
        }
        echo json_encode($data);

        return redirect()->to(base_url('user/control'));
    }

    public function get_list_user_hris()
    {
        $q = $this->request->getVar('q');

        $list = \App\Models\Hris\Models::get_list(true, $q);

        return $list;
    }

    public function get_detail_user_hris() 
    {
        $result = [];

        $id = $this->request->getVar('id');

        if ($id != null) {
            $result = \App\Models\Hris\Models::get_detail(true, $id);
        }

        return $result;
    }

    public function get_list_level()
    {
        $list = \App\Models\Level\Models::get_list(true);

        return $list;
    }

    public function delete()
    {
        $id = decrypt_url($this->request->getVar('id'));
        $this->model->act_delete($id);        
    }
}
