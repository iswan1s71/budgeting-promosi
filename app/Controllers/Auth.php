<?php

namespace App\Controllers;
use CodeIgniter\HTTP\RequestInterface; 
use CodeIgniter\HTTP\ResponseInterface; 
use Psr\Log\LoggerInterface;

class Auth extends BaseController
{
    public function initController(RequestInterface $request, ResponseInterface $response, LoggerInterface $logger)
    {
        // Do Not Edit This Line
        parent::initController($request, $response, $logger);
        if ($this->session->i_user !== NULL) {
            $response->redirect(base_url()); // or use $this->response->redirect(base_url('login'));
        }

        $this->i_user = $this->session->i_user;
    }

    public function index()
    {
        return view('auth/auth');
    }

    public function login()
    {
        if (!$this->validate([
            'i_user_code' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'Username Harus diisi'
                ]
            ],
            'password' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'Password Harus diisi'
                ]
            ],
            /* 'color' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'Warna Harus diisi'
                ]
            ], */
        ])) {
            $data['message'] = $this->validator->listErrors();
            echo view('auth/auth', $data);
        } else {
            $i_user_code = $this->request->getVar('i_user_code');
            $e_password = encrypt_password($this->request->getVar('password'));
            $color = $this->request->getVar('color');
            $query = $this->mmaster->check_user($i_user_code, $e_password);
            if ($query !== NULL) {
                $newdata = [
                    'i_user'  => $query['i_user'],
                    'i_user_code'  => $query['i_user_code'],
                    'i_department'  => $query['last_i_department'],
                    'i_level'  => $query['i_level'],
                    'e_user_name'  => $query['e_user_name'],
                    // 'f_super_admin'  => $query['f_super_admin'],
                    'color' => $color,
                    'language' => $this->request->getLocale(),
                ];
                $this->session->set($newdata);
                $this->mmaster->log("Login");
                return redirect()->to(base_url('main'));
            } else {
                $data['message'] = "Sorry, username or password wrong";
                return view('auth/auth', $data);
            }
        }
    }

    public function logout()
    {
        $this->session->destroy();
        $this->mmaster->log("Login");
        return redirect()->to(base_url('auth'));
    }

    public function set_activemenu()
    {
        $data = array(
            'idmenu_1' => $this->request->getVar('idmenu_1'),
            'idmenu_2' => $this->request->getVar('idmenu_2'),
            'idmenu_3' => $this->request->getVar('idmenu_3'),
            'current_link' => $this->request->getVar('folder'),
        );
        $this->session->set($data);
    }

    public function set_collapse()
    {
        $this->session->set('sidebar', $this->request->getVar('sidebar'));
        $this->mmaster->log("Change Sidebar");
    }

    public function set_language()
    {
        $this->session->set('language', $this->request->getVar('language'));
        $this->mmaster->log("Change Language to ".capitalize($this->request->getVar('language')));
    }

    public function set_department()
    {
        $i_department = $this->request->getVar('id');
        $data = array(
            'i_department' => $i_department, 
            'e_department_name' => $this->request->getVar('name'), 
        );
        $this->session->set($data);
        $this->mmaster->set_session_department($this->i_user, $i_department);
        $this->mmaster->log("Change Department to ".capitalize($this->request->getVar('name')));
        echo json_encode('sukses');
    }

    public function set_level()
    {
        $i_level = $this->request->getVar('id');
        $data = array(
            'i_level' => $i_level, 
            'e_level_name' => $this->request->getVar('name'), 
        );
        $this->session->set($data);
        $this->mmaster->set_session_level($this->i_user, $i_level);
        $this->mmaster->log("Change Level to ".capitalize($this->request->getVar('name')));
    }
}
