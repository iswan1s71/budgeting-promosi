<?php

namespace App\Controllers;
use CodeIgniter\HTTP\RequestInterface; 
use CodeIgniter\HTTP\ResponseInterface; 
use Psr\Log\LoggerInterface;

class Main extends BaseController
{
    public function initController(RequestInterface $request, ResponseInterface $response, LoggerInterface $logger)
    {
        // Do Not Edit This Line
        parent::initController($request, $response, $logger);
        if($this->session->i_user === NULL){
            $response->redirect(base_url('auth')); // or use $this->response->redirect(base_url('login'));
        } 
        
    }

    public function index()
    {        
        return view('main');
    }

    public function home()
    {
        return redirect()->to(base_url('/'));
    }

}
