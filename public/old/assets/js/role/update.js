document.addEventListener("DOMContentLoaded", function() {
    $('.listbox-no-selection').bootstrapDualListbox({
        nonSelectedListLabel: 'Non-selected',
        selectedListLabel: 'Selected',
        preserveSelectionOnMove: 'moved',
        moveOnSelect: true,
        bootstrap2compatible: true
    });
    var link = "/" + $("#path").val() + "/control";
    $(".submit").on("click", function() {
        var form = $('.form-validate-jquery').valid();
        if (form) {
            sweet_update(link);
        }
    });
});