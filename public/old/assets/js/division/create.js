document.addEventListener("DOMContentLoaded", function() {
    $('.form-check-input-styled-slate').uniform({
        wrapperClass: 'border-slate-800 text-slate-800'
    });
    $('.form-check-input-styled').uniform();
    var link = "/" + $("#path").val() + "/control";
    $("#i_department").select2({
        width: "100%",
        allowClear: true,
        ajax: {
            url: base_url + link + "/get_department",
            dataType: "json",
            delay: 250,
            data: function(params) {
                var query = {
                    q: params.term,
                };
                return query;
            },
            processResults: function(data) {
                return {
                    results: data,
                };
            },
            cache: false,
        },
    });
    $(".submit").on("click", function() {
        var form = $('.form-validate-jquery').valid();
        if (form) {
            sweet_create(link);
        }
    });
});