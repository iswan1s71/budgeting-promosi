document.addEventListener("DOMContentLoaded", function() {
    var link = '/' + $("#path").val() + "/control/serverside";
    var column = $("#serverside thead tr th").length;
    var order = [3, 'desc'];
    datatable(link, column, order);
});