document.addEventListener("DOMContentLoaded", function() {
    var link = "/" + $("#path").val() + "/control";
    $("#i_department").select2({
        width: "100%",
        allowClear: true,
        ajax: {
            url: base_url + link + "/get_department",
            dataType: "json",
            delay: 250,
            data: function(params) {
                var query = {
                    q: params.term,
                };
                return query;
            },
            processResults: function(data) {
                return {
                    results: data,
                };
            },
            cache: false,
        },
    }).change(function() {
        $("#i_division").val('');
        $("#i_division").html('');
    });
    $("#i_division").select2({
        width: "100%",
        allowClear: true,
        ajax: {
            url: base_url + link + "/get_division",
            dataType: "json",
            delay: 250,
            data: function(params) {
                var query = {
                    q: params.term,
                    i_department: $('#i_department').val(),
                };
                return query;
            },
            processResults: function(data) {
                return {
                    results: data,
                };
            },
            cache: false,
        },
    });
    $("#i_level_position").select2({
        width: "100%",
        allowClear: true,
        ajax: {
            url: base_url + link + "/get_level_position",
            dataType: "json",
            delay: 250,
            data: function(params) {
                var query = {
                    q: params.term,
                };
                return query;
            },
            processResults: function(data) {
                return {
                    results: data,
                };
            },
            cache: false,
        },
    });
    $(".submit").on("click", function() {
        var form = $('.form-validate-jquery').valid();
        if (form) {
            sweet_update(link);
        }
    });
});