document.addEventListener("DOMContentLoaded", function() {
    var link = '/' + $("#path").val() + "/control/serverside";
    var linkcreate = '/' + $("#path").val() + "/control/create";
    var column = $("#serverside thead tr th").length;
    var color = $("#color").val();
    var i_menu = $("#i_menu").val();
    if (i_menu == '') {
        datatable(link, column);
    } else {
        datatable_create(link, column, linkcreate, color);
    }
});