document.addEventListener("DOMContentLoaded", function() {
    $('.form-check-input-styled-slate').uniform({
        wrapperClass: 'border-slate-800 text-slate-800'
    });
    var link = "/" + $("#path").val() + "/control";
    $(".submit").on("click", function() {
        var form = $('.form-validate-jquery').valid();
        if (form) {
            sweet_create(link);
        }
    });
});