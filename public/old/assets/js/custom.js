/* ------------------------------------------------------------------------------
 *
 *  # Custom JS code
 *
 *  Place here all your custom js. Make sure it's loaded after app.js
 *
 * ---------------------------------------------------------------------------- */
// Set Active Menu
function set_activemenu(idmenu_1, idmenu_2, idmenu_3, folder) {
    $.ajax({
        type: "POST",
        data: {
            idmenu_1: idmenu_1,
            idmenu_2: idmenu_2,
            idmenu_3: idmenu_3,
            folder: folder,
        },
        url: base_url + "/auth/set_activemenu",
        dataType: "html",
        success: function(data) {
            window.location = base_url + '/' + folder + '/control';
            localStorage.clear();
        },
        error: function() {
            alert("Error :)");
        },
    });
}


// Set Collapse
function set_collapse(sidebar) {
    $.ajax({
        type: "POST",
        data: {
            sidebar: sidebar
        },
        url: base_url + "/auth/set_collapse",
        dataType: "html",
        success: function(data) {
            if (current_link !== '') {
                window.location = base_url + '/' + current_link + '/control';
            } else {
                window.location = base_url;
            }
        },
        error: function() {
            alert("Error :)");
        },
    });
}

// Set Languange
function set_language(language) {
    $.ajax({
        type: "POST",
        data: {
            language: language
        },
        url: base_url + "/auth/set_language",
        dataType: "html",
        success: function(data) {
            if (current_link !== '') {
                window.location = base_url + '/' + current_link + '/control';
            } else {
                window.location = base_url;
            }
        },
        error: function() {
            alert("Error :)");
        },
    });
}


/* ------------------------------------------------------------------------------
 *
 *  # Layout - fixed navbar and sidebar with custom scrollbar
 *
 *  Demo JS code for layout_fixed_sidebar_custom.html page
 *
 * ---------------------------------------------------------------------------- */


// Setup module
// ------------------------------

var FixedSidebarCustomScroll = function() {


    //
    // Setup module components
    //

    // Perfect scrollbar
    var _componentPerfectScrollbar = function() {
        if (typeof PerfectScrollbar == 'undefined') {
            console.warn('Warning - perfect_scrollbar.min.js is not loaded.');
            return;
        }

        // Initialize
        var ps = new PerfectScrollbar('.sidebar-fixed .sidebar-content', {
            wheelSpeed: 2,
            wheelPropagation: true
        });
    };


    //
    // Return objects assigned to module
    //

    return {
        init: function() {
            _componentPerfectScrollbar();
        }
    }
}();

/* ------------------------------------------------------------------------------
 *
 *  # Form validation
 *
 *  Demo JS code for form_validation.html page
 *
 * ---------------------------------------------------------------------------- */


// Setup module
// ------------------------------

var FormValidation = function() {


    //
    // Setup module components
    //

    // Uniform
    var _componentUniform = function() {
        if (!$().uniform) {
            console.warn('Warning - uniform.min.js is not loaded.');
            return;
        }

        // Initialize
        $('.form-input-styled').uniform({
            fileButtonClass: 'action btn bg-blue'
        });
    };

    // Validation config
    var _componentValidation = function() {
        if (!$().validate) {
            console.warn('Warning - validate.min.js is not loaded.');
            return;
        }

        // Initialize
        var validator = $('.form-validate-jquery').validate({
            ignore: 'input[type=hidden], .select2-search__field', // ignore hidden fields
            errorClass: 'validation-invalid-label',
            /* successClass: 'validation-valid-label', */
            validClass: 'validation-valid-label',
            highlight: function(element, errorClass) {
                $(element).removeClass(errorClass);
            },
            unhighlight: function(element, errorClass) {
                $(element).removeClass(errorClass);
            },
            /* success: function(label) {
                label.addClass('validation-valid-label').text('Success.'); // remove to hide Success message
            }, */

            // Different components require proper error label placement
            errorPlacement: function(error, element) {

                // Unstyled checkboxes, radios
                if (element.parents().hasClass('form-check')) {
                    error.appendTo(element.parents('.form-check').parent());
                }

                // Input with icons and Select2
                else if (element.parents().hasClass('form-group-feedback') || element.hasClass('select2-hidden-accessible')) {
                    error.appendTo(element.parent());
                }

                // Input group, styled file input
                else if (element.parent().is('.uniform-uploader, .uniform-select') || element.parents().hasClass('input-group')) {
                    error.appendTo(element.parent().parent());
                }

                // Other elements
                else {
                    error.insertAfter(element);
                }
            },
            rules: {
                password: {
                    minlength: 5
                },
                repeat_password: {
                    equalTo: '#password'
                },
                email: {
                    email: true
                },
                repeat_email: {
                    equalTo: '#email'
                },
                minimum_characters: {
                    minlength: 10
                },
                maximum_characters: {
                    maxlength: 10
                },
                minimum_number: {
                    min: 10
                },
                maximum_number: {
                    max: 10
                },
                number_range: {
                    range: [10, 20]
                },
                url: {
                    url: true
                },
                date: {
                    date: true
                },
                date_iso: {
                    dateISO: true
                },
                numbers: {
                    number: true
                },
                digits: {
                    digits: true
                },
                creditcard: {
                    creditcard: true
                },
                basic_checkbox: {
                    minlength: 2
                },
                styled_checkbox: {
                    minlength: 2
                },
                switchery_group: {
                    minlength: 2
                },
                switch_group: {
                    minlength: 2
                }
            },
            messages: {
                custom: {
                    required: 'This is a custom error message'
                },
                basic_checkbox: {
                    minlength: 'Please select at least {0} checkboxes'
                },
                styled_checkbox: {
                    minlength: 'Please select at least {0} checkboxes'
                },
                switchery_group: {
                    minlength: 'Please select at least {0} switches'
                },
                switch_group: {
                    minlength: 'Please select at least {0} switches'
                },
                agree: 'Please accept our policy'
            }
        });

        // Reset form
        $('#reset').on('click', function() {
            validator.resetForm();
        });
    };

    //
    // Return objects assigned to module
    //

    return {
        init: function() {
            _componentUniform();
            _componentValidation();
        }
    }
}();

var AnimationsCSS3 = function() {


    //
    // Setup module components
    //

    // CSS3 animations
    var _componentAnimationCSS = function() {

        // Toggle animations
        $('body').on('click', '.animation', function(e) {

            // Get animation class from 'data' attribute
            var animation = $(this).data('animation');

            // Apply animation once per click
            $(this).parents('.card').addClass('animated ' + animation).one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
                $(this).removeClass('animated ' + animation);
            });
            e.preventDefault();
        });
    };


    //
    // Return objects assigned to module
    //

    return {
        init: function() {
            _componentAnimationCSS();
        }
    }
}();

// Initialize module
// ------------------------------

document.addEventListener('DOMContentLoaded', function() {
    FixedSidebarCustomScroll.init();
    FormValidation.init();
    AnimationsCSS3.init();
    $(".animation").click();
});

/* ------------------------------------------------------------------------------
 *
 *  # Select2
 *
 * ---------------------------------------------------------------------------- */
var _componentSelect2 = function() {
    if (!$().select2) {
        console.warn("Warning - select2.min.js is not loaded.");
        return;
    }

    // Initialize
    $(".form-control-select2").select2({
        minimumResultsForSearch: Infinity,
    });

    // Length menu styling
    $(".dataTables_length select").select2({
        minimumResultsForSearch: Infinity,
        dropdownAutoWidth: true,
        width: "auto",
    });
};

/* ------------------------------------------------------------------------------
 *
 *  # Datatable
 *
 * ---------------------------------------------------------------------------- */
function datatable(link, column, order = 0) {
    if (order != 0) {
        var orderby = order;
    } else {
        var orderby = [1, "asc"];
    }
    var t = $("#serverside").DataTable({
        serverSide: true,
        processing: true,
        ajax: {
            url: base_url + link,
            type: "post",
            error: function(data, err) {
                $(".serverside-error").html("");
                $("#serverside tbody").empty();
                $("#serverside").append(
                    '<tr><td class="text-center" colspan="' + column + '">No data available in table</td></tr>'
                );
                $("#serverside_processing").css("display", "none");
            },
        },
        jQueryUI: false,
        sScrollX: "100%",
        bScrollCollapse: false,
        jQueryUI: false,
        autoWidth: false,
        /* autoWidth: true,
        scrollX: true,
        fixedColumns: true, */
        lengthMenu: [
            [10, 25, 50, -1],
            [10, 25, 50, "All"],
        ],
        pageLength: 10,
        order: [orderby],
        columnDefs: [{
                targets: [0, column - 1],
                width: "5%",
                orderable: false,
                /* className: "text-center", */
            },
            {
                targets: [0],
                width: "3%",
                className: "text-right",
            },
        ],
        pagingType: "full_numbers",
        dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
        language: {
            infoPostFix: "",
            search: "<span>Search :</span> _INPUT_",
            searchPlaceholder: "Type to Search..",
            info: "Showing _START_ to _END_ of _TOTAL_ entries",
            infoFiltered: "(filtered from _MAX_ total entries)",
            lengthMenu: "<span>Show : </span> _MENU_",
            url: "",
            paginate: {
                first: "First",
                last: "Last",
                next: $("html").attr("dir") == "rtl" ? "&larr;" : "&rarr;",
                previous: $("html").attr("dir") == "rtl" ? "&rarr;" : "&larr;",
            },
        },
        bStateSave: true,
        fnStateSave: function(oSettings, oData) {
            localStorage.setItem("offersDataTables", JSON.stringify(oData));
        },
        fnStateLoad: function(oSettings) {
            return JSON.parse(localStorage.getItem("offersDataTables"));
        },
        drawCallback: function(settings) {
            _componentSelect2();
        },
    });
    t.on("draw.dt", function() {
        var info = t.page.info();
        t.column(0, {
            search: "applied",
            order: "applied",
            page: "applied"
        }).nodes().each(function(cell, i) {
            cell.innerHTML = i + 1 + info.start;
        });
    });
    $("div.dataTables_filter input", t.table().container()).focus();
}

function datatable_create(link, column, linkcreate, color, order = 0) {
    if (order != 0) {
        var orderby = order;
    } else {
        var orderby = [1, "asc"];
    }
    var t = $("#serverside").DataTable({
        buttons: [{
            text: '<i class="icon-database-add mr-2"></i>Data',
            className: "btn bg-slate-800 text-slate-800 border-slate-800",
            action: function(e, dt, node, config) {
                window.location.href = base_url + linkcreate;
            },
        }, ],
        serverSide: true,
        processing: true,
        ajax: {
            url: base_url + link,
            type: "post",
            error: function(data, err) {
                $(".serverside-error").html("");
                $("#serverside tbody").empty();
                $("#serverside").append(
                    '<tr><td class="text-center" colspan="' + column + '">No data available in table</td></tr>'
                );
                $("#serverside_processing").css("display", "none");
            },
        },
        jQueryUI: false,
        sScrollX: "100%",
        bScrollCollapse: false,
        autoWidth: false,
        /* autoWidth: true, */
        /* scrollX: true, */
        /* fixedColumns: true, */
        lengthMenu: [
            [10, 25, 50, -1],
            [10, 25, 50, "All"],
        ],
        pageLength: 10,
        order: [orderby],
        columnDefs: [{
                targets: [0, column - 1],
                width: "5%",
                orderable: false,
                /* className: "text-center", */
            },
            {
                targets: [0],
                width: "3%",
                className: "text-right",
            },
        ],
        pagingType: "full_numbers",
        dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
        language: {
            infoPostFix: "",
            search: "<span>Search :</span> _INPUT_",
            searchPlaceholder: "Type to Search..",
            info: "Showing _START_ to _END_ of _TOTAL_ entries",
            infoFiltered: "(filtered from _MAX_ total entries)",
            lengthMenu: "<span>Show : </span> _MENU_",
            url: "",
            paginate: {
                first: "First",
                last: "Last",
                next: $("html").attr("dir") == "rtl" ? "&larr;" : "&rarr;",
                previous: $("html").attr("dir") == "rtl" ? "&rarr;" : "&larr;",
            },
        },
        bStateSave: true,
        fnStateSave: function(oSettings, oData) {
            localStorage.setItem("offersDataTables", JSON.stringify(oData));
        },
        fnStateLoad: function(oSettings) {
            return JSON.parse(localStorage.getItem("offersDataTables"));
        },
        drawCallback: function(settings) {
            _componentSelect2();
        },
    });
    t.on("draw.dt", function() {
        var info = t.page.info();
        t.column(0, {
            search: "applied",
            order: "applied",
            page: "applied"
        }).nodes().each(function(cell, i) {
            cell.innerHTML = i + 1 + info.start;
        });
    });
    $("div.dataTables_filter input", t.table().container()).focus();
}

/** SweetAlert Add Data */
function sweet_create(link) {
    var swalInit = swal.mixin({
        buttonsStyling: false,
        confirmButtonClass: "btn btn-sm btn-outline bg-success-800 text-success-800 border-success-800",
        cancelButtonClass: "btn btn-sm btn-outline bg-slate-800 text-slate-800 border-slate-800",
        confirmButtonText: '<i class="icon-thumbs-up3"></i> Yes',
        cancelButtonText: '<i class="icon-thumbs-down3"></i> No',
    });
    swalInit({
        title: "Are you sure?",
        text: "This data will be saved :)",
        type: "question",
        showCancelButton: true,
        buttonsStyling: false,
    }).then(function(result) {
        if (result.value) {
            $.ajax({
                type: "POST",
                data: $("form").serialize(),
                url: base_url + link + "/act_create",
                dataType: "json",
                beforeSend: function() {
                    $(".navbar-top").block({
                        /* message: '<div class="spinner-grow text-primary"></div><div class="spinner-grow text-success"></div><div class="spinner-grow text-teal"></div><div class="spinner-grow text-info"></div><div class="spinner-grow text-warning"></div><div class="spinner-grow text-orange"></div><div class="spinner-grow text-danger"></div><div class="spinner-grow text-secondary"></div><div class="spinner-grow text-dark"></div><div class="spinner-grow text-muted"></div><br><h1 class="text-muted d-block">P l e a s e &nbsp;&nbsp; W a i t</h1>', */
                        /* message: '<img src="../assets/image/Preloader_2.gif" alt="loading" /><h1 class="text-muted d-block">L o a d i n g</h1>', */
                        message: '<img src="' + base_url + '/public/global_assets/images/loading/loading4.gif" alt="loading" /><h1 class="text-muted d-block">L o a d i n g</h1>',
                        centerX: false,
                        centerY: false,
                        overlayCSS: {
                            backgroundColor: "#fff",
                            opacity: 0.8,
                            cursor: "wait",
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: "none",
                        },
                    });
                },
                success: function(data) {
                    if (data.success == true && data.ready == false && data.message == null) {
                        swalInit("Success!", "Data " + data.data + " saved successfully :)", "success").then(
                            function(result) {
                                window.location = base_url + link;
                            }
                        );
                    } else if (data.success == false && data.ready == true && data.message == null) {
                        swalInit("Sorry :(", "The data already exists :(", "error");
                    } else if (data.success == false && data.ready == false && data.message != null) {
                        swalInit({
                            title: '<span class="font-weight-light">Data failed to save :(</span>',
                            type: 'error',
                            html: '<div class="alert text-danger-800 alpha-danger border-0 alert-dismissible">' + data.message + '</div>',
                            showCloseButton: true,
                            showCancelButton: false,
                            focusConfirm: false,
                            confirmButtonText: '<i class="icon-thumbs-down2"></i>',
                            confirmButtonClass: "btn btn-sm btn-outline bg-danger-800 text-danger-800 border-danger-800",
                        });
                    } else {
                        swalInit("Sorry :(", "Data failed to save :(", "error");
                    }
                    $(".navbar-top").unblock();
                },
                error: function() {
                    swalInit("Sorry", "Data failed to save :(", "error");
                    $(".navbar-top").unblock();
                },
            });
        }
    });
}

/** SweetAlert Edit Data */
function sweet_update(link) {
    var swalInit = swal.mixin({
        buttonsStyling: false,
        confirmButtonClass: "btn btn-sm btn-outline bg-success-800 text-success-800 border-success-800",
        cancelButtonClass: "btn btn-sm btn-outline bg-slate-800 text-slate-800 border-slate-800",
        confirmButtonText: '<i class="icon-thumbs-up3"></i> Yes',
        cancelButtonText: '<i class="icon-thumbs-down3"></i> No',
    });
    swalInit({
        title: "Are you sure?",
        text: "This data will be saved :)",
        type: "question",
        showCancelButton: true,
        buttonsStyling: false,
    }).then(function(result) {
        if (result.value) {
            $.ajax({
                type: "POST",
                data: $("form").serialize(),
                url: base_url + link + "/act_update",
                dataType: "json",
                beforeSend: function() {
                    $(".navbar-top").block({
                        message: '<img src="' + base_url + '/public/global_assets/images/loading/loading4.gif" alt="loading" /><h1 class="text-muted d-block">L o a d i n g</h1>',
                        centerX: false,
                        centerY: false,
                        overlayCSS: {
                            backgroundColor: "#fff",
                            opacity: 0.8,
                            cursor: "wait",
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: "none",
                        },
                    });
                },
                success: function(data) {
                    if (data.success == true && data.ready == false && data.message == null) {
                        swalInit("Success!", "Data " + data.data + " update successfully :)", "success").then(
                            function(result) {
                                window.location = base_url + link;
                            }
                        );
                    } else if (data.success == false && data.ready == true && data.message == null) {
                        swalInit("Sorry :(", "The data already exists :(", "error");
                    } else if (data.success == false && data.ready == false && data.message != null) {
                        swalInit({
                            title: '<span class="font-weight-light">Data failed to update :(</span>',
                            type: 'error',
                            html: '<div class="alert text-danger-800 alpha-danger border-0 alert-dismissible">' + data.message + '</div>',
                            showCloseButton: true,
                            showCancelButton: false,
                            focusConfirm: false,
                            confirmButtonText: '<i class="icon-thumbs-down2"></i>',
                            confirmButtonClass: "btn btn-sm btn-outline bg-danger-800 text-danger-800 border-danger-800",
                        });
                    } else {
                        swalInit("Sorry :(", "Data failed to update :(", "error");
                    }
                    $(".navbar-top").unblock();
                },
                error: function() {
                    swalInit("Sorry", "Data failed to update :(", "error");
                    $(".navbar-top").unblock();
                },
            });
        }
    });
}

/** Update Status */
function change_status(link, id) {
    var swalInit = swal.mixin({
        buttonsStyling: false,
        confirmButtonClass: "btn bg-slate-800",
        cancelButtonClass: "btn btn-pink-400",
    });

    $.ajax({
        type: "POST",
        data: {
            id: id,
        },
        url: base_url + link + "/change_status",
        dataType: "json",
        beforeSend: function() {
            $(".navbar-top").block({
                /* message: '<div class="spinner-grow text-primary"></div><div class="spinner-grow text-success"></div><div class="spinner-grow text-teal"></div><div class="spinner-grow text-info"></div><div class="spinner-grow text-warning"></div><div class="spinner-grow text-orange"></div><div class="spinner-grow text-danger"></div><div class="spinner-grow text-secondary"></div><div class="spinner-grow text-dark"></div><div class="spinner-grow text-muted"></div><br><h1 class="text-muted d-block">P l e a s e &nbsp;&nbsp; W a i t</h1>', */
                message: '<img src="' + base_url + '/public/global_assets/images/loading/loading4.gif" alt="loading" /><h1 class="text-muted d-block">L o a d i n g</h1>',
                centerX: false,
                centerY: false,
                overlayCSS: {
                    backgroundColor: "#fff",
                    opacity: 0.8,
                    cursor: "wait",
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: "none",
                },
            });
        },
        success: function(data) {
            if (data.success == true) {
                swalInit("Success!", "Data update successfully :)", "success").then(
                    function(result) {
                        window.location = base_url + link;
                    }
                );
            } else {
                swalInit("Sorry :(", "Data failed to update :(", "error");
            }
            $(".navbar-top").unblock();
        },
        error: function() {
            swalInit("Sorry", "Data failed to update :(", "error");
            $(".navbar-top").unblock();
        },
    });
}

/* Cancel */
function sweet_delete(link, id) {
    var swalInit = swal.mixin({
        buttonsStyling: false,
        confirmButtonClass: "btn btn-sm btn-outline bg-success-800 text-success-800 border-success-800",
        cancelButtonClass: "btn btn-sm btn-outline bg-danger-800 text-danger-800 border-danger-800",
        confirmButtonText: '<i class="icon-thumbs-up3"></i> Yes',
        cancelButtonText: '<i class="icon-thumbs-down3"></i> No',
    });
    swalInit({
        title: "Are you sure?",
        text: "This data will be canceled :)",
        type: "error",
        showCancelButton: true,
        buttonsStyling: false,
    }).then(function(result) {
        if (result.value) {
            $.ajax({
                type: "POST",
                data: {
                    id: id,
                },
                url: base_url + link + "/act_delete",
                dataType: "json",
                beforeSend: function() {
                    $(".navbar-top").block({
                        message: '<img src="' + base_url + '/public/global_assets/images/loading/loading4.gif" alt="loading" /><h1 class="text-muted d-block">L o a d i n g</h1>',
                        centerX: false,
                        centerY: false,
                        overlayCSS: {
                            backgroundColor: "#fff",
                            opacity: 0.8,
                            cursor: "wait",
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: "none",
                        },
                    });
                },
                success: function(data) {
                    if (data.success == true) {
                        swalInit("Success!", "Data delete successfully :)", "success").then(
                            function(result) {
                                window.location = base_url + link;
                            }
                        );
                    } else {
                        swalInit("Sorry :(", "Data failed to deleted :(", "error");
                    }
                    $(".navbar-top").unblock();
                },
                error: function() {
                    swalInit("Sorry", "Data failed to delete :(", "error");
                    $(".navbar-top").unblock();
                },
            });
        }
    });
}

function set_department(id, name) {
    $.ajax({
        type: "POST",
        data: {
            id: id,
            name: name,
        },
        url: base_url + "/auth/set_department",
        dataType: "html",
        success: function(data) {
            window.location = base_url;
        },
        error: function() {
            alert("Error :)");
        },
    });
}

function set_level(id, name) {
    $.ajax({
        type: "POST",
        data: {
            id: id,
            name: name,
        },
        url: base_url + "/auth/set_level",
        dataType: "html",
        success: function(data) {
            window.location = base_url;
        },
        error: function() {
            alert("Error :)");
        },
    });
}

function reformat(input) {
    var num = input.value.replace(/\,/g, "");
    if (!isNaN(num)) {
        if (num.indexOf(".") > -1) {
            num = num.split(".");
            num[0] = num[0]
                .toString()
                .split("")
                .reverse()
                .join("")
                .replace(/(?=\d*\.?)(\d{3})/g, "$1,")
                .split("")
                .reverse()
                .join("")
                .replace(/^[\,]/, "");
            if (num[1].length > 4) {
                alert("maksimum 4 desimal !!!");
                num[1] = num[1].substring(0, num[1].length - 1);
            }
            input.value = num[0] + "." + num[1];
        } else {
            input.value = num
                .toString()
                .split("")
                .reverse()
                .join("")
                .replace(/(?=\d*\.?)(\d{3})/g, "$1,")
                .split("")
                .reverse()
                .join("")
                .replace(/^[\,]/, "");
        }
    } else {
        alert("input harus numerik !!!");
        input.value = input.value.substring(0, input.value.length - 1);
    }
}

function formatulang(a) {
    var s = a.replace(/\,/g, "");
    return s;
}

function formatcemua(input) {
    var num = input.toString();
    if (!isNaN(num)) {
        if (num.indexOf(".") > -1) {
            num = num.split(".");
            num[0] = num[0]
                .toString()
                .split("")
                .reverse()
                .join("")
                .replace(/(?=\d*\.?)(\d{3})/g, "$1,")
                .split("")
                .reverse()
                .join("")
                .replace(/^[\,]/, "");
            if (num[1].length > 2) {
                while (num[1].length > 2) {
                    num[1] = num[1].substring(0, num[1].length - 1);
                }
            }
            input = num[0];
        } else {
            input = num
                .toString()
                .split("")
                .reverse()
                .join("")
                .replace(/(?=\d*\.?)(\d{3})/g, "$1,")
                .split("")
                .reverse()
                .join("")
                .replace(/^[\,]/, "");
        }
    }
    return input;
}

String.prototype.replaceAll = function(stringToFind, stringToReplace) {
    if (stringToFind === stringToReplace) return this;
    var temp = this;
    var index = temp.indexOf(stringToFind);
    while (index != -1) {
        temp = temp.replace(stringToFind, stringToReplace);
        index = temp.indexOf(stringToFind);
    }
    return temp;
};

function showCalendar(id, minDate = 999, maxDate = 999) {
    if (minDate == 999) {
        minDate = "-5y";
    } else if (minDate == null) {
        minDate = null;
    } else {
        if (today.getDay() - minDate == 0) {
            minDate = minDate + 1;
            minDate = "-" + minDate + "d";
        } else {
            minDate = "-" + minDate + "d";
        }
    }

    if (maxDate == 999) {
        maxDate = "+5y";
    } else {
        if (today.getDay() + maxDate == 0) {
            maxDate = maxDate + 1;
            maxDate = "+" + maxDate + "d";
        } else {
            maxDate = "+" + maxDate + "d";
        }
    }

    //alert(minDate);
    $(id).datepicker({
        clearBtn: true,
        language: "id",
        locale: "id",
        autoclose: true,
        todayHighlight: true,
        format: "dd-mm-yyyy",
        todayBtn: "linked",
        daysOfWeekDisabled: [0],
        daysOfWeekHighlighted: [0],
        startDate: new Date($("#closing").val()),
        endDate: maxDate,
        datesDisabled: holiday,
    });
}