document.addEventListener("DOMContentLoaded", function() {
    var link = "/" + $("#path").val() + "/control";
    $('.form-control-select2').select2({
        minimumResultsForSearch: Infinity
    });
    $("#i_parent").select2({
        width: "100%",
        allowClear: true,
        ajax: {
            url: base_url + link + "/get_menu",
            dataType: "json",
            delay: 250,
            data: function(params) {
                var query = {
                    q: params.term,
                };
                return query;
            },
            processResults: function(data) {
                return {
                    results: data,
                };
            },
            cache: false,
        },
    });

    $(".submit").on("click", function() {
        var form = $('.form-validate-jquery').valid();
        if (form) {
            sweet_update(link);
        }
    });
});