// Initialize module
// ------------------------------
document.addEventListener('DOMContentLoaded', function() {
    let link = '/' + $("#path").val() + "/control/serverside";
    // let linkcreate = '/' + $("#path").val() + "/control/create";
    let linkcreate = '/' + $("#path").val() + "/control/act_create";
    let column = $("#serverside thead tr th").length;
    let color = $("#color").val();
    let i_menu = $("#i_menu").val();
    if (i_menu == '') {
        DataTableRead(link, column);
    } else {
        DatatableButtonsCreate(link, column, linkcreate, color);
    }
});
