document.addEventListener("DOMContentLoaded", function() {
    var link = "/" + $("#path").val() + "/control";
    $(".submit").on("click", function() {
        var form = $('.form-validate-jquery').valid();
        if (form) {
            sweet_update(link);
        }
    });


    $('#f_support').select2({
        minimumResultsForSearch: Infinity,
    });
    $('#f_active').select2({
        minimumResultsForSearch: Infinity,
    });
});