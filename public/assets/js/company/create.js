document.addEventListener("DOMContentLoaded", function() {
    var link = "/" + $("#path").val() + "/control";
    $(".submit").on("click", function() {
        var form = $('.form-validate-jquery').valid();
        if (form) {
            sweet_create(link);
        }
    });
});