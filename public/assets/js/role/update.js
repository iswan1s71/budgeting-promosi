document.addEventListener("DOMContentLoaded", function() {
    
    var link = "/" + $("#path").val() + "/control";
    $(".submit").on("click", function() {
        
        sweet_update(link);
    });

    $('.cb-grant-row').each(function() {
        
        $(this).on('change', function() {
            
            let row = $(this).closest('tr');

            if ($(this).is(':checked')) {
                row.find('input[type="checkbox"]').each(function() {
                    $(this).prop('checked', true)
                })
            } else {
                row.find('input[type="checkbox"]').each(function() {
                    $(this).prop('checked', false)
                })
            }       
            
        })
    })

    $('#cb-grant-all-row').on('change', function() {
        if ($(this).is(':checked')) {
            checkboxAllChecked();
        } else {
            checkboxAllUnchecked();
        }
    })

    function checkboxAllChecked() {
        $('tbody input[type="checkbox"]').each(function() {
            $(this).prop('checked', true)
        })
    }

    function checkboxAllUnchecked() {
        $('tbody input[type="checkbox"]').each(function() {
            $(this).prop('checked', false)
        })
    }
});