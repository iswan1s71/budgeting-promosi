document.addEventListener("DOMContentLoaded", function() {
    
    var link = "/" + $("#path").val() + "/control";
    $(".submit").on("click", function() {        
        sweet_update(link);
    });

    $('#levels').select2();

    $('#companies').select2();

    $('#f_active').select2({
        minimumResultsForSearch: Infinity,
    });

});