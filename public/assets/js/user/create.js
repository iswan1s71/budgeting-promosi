document.addEventListener("DOMContentLoaded", function() {
    var link = "/" + $("#path").val() + "/control";

    $("#iduser").select2({
        width: "100%",
        allowClear: true,
        ajax: {
            url: base_url + link + "/get_list_user_hris",
            dataType: "json",
            delay: 250,
            data: function(params) {
                var query = {
                    q: params.term,
                };
                return query;
            },
            processResults: function(data) {
                return {
                    results: data,
                };
            },
            cache: false,
        },
    }).change(function() {
        const id = $(this).val();
        getDetailUser(id);


        $('#levels').select2().val(null).trigger('change');
        $('#companies').select2().val(null).trigger('change');
    });

    $('#companies').select2();

    function createInputDetailUser(id, username, e_user_password, e_department_name, e_cabang_name, nama) {
        let inputs = `<input type="hidden" id="id" name="id" class="form-control" value="${id}" readonly>
                        <input type="hidden" id="password" name="e_user_password" class="form-control" value="${e_user_password}" readonly>

                    <div class="row">
                        <div class="col-6">
                            <div class="form-group mb-3">
                                <label class="form-label">Nama:</label>
                                <input type="text" id="nama" name="nama" class="form-control" value="${nama}" readonly>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group mb-3">
                                <label class="form-label">Username:</label>
                                <input type="text" id="username" name="username" class="form-control" value="${username}" readonly>
                            </div>
                        </div>
                    </div>                    
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group mb-3">
                                <label class="form-label">Departemen :</label>
                                <input type="text" id="e_department_name" name="e_department_name" class="form-control" value="${e_department_name}" readonly>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group mb-3">
                                <label class="form-label">Cabang :</label>
                                <input type="text" id="e_cabang_name" name="e_cabang_name" class="form-control" value="${e_cabang_name}" readonly>
                            </div>
                        </div>
                    </div>`;
                    
        return inputs;
    }

    function toggleCardDetailUser(data) {
        $("#fieldset-detail-user").empty();
        const inputs = createInputDetailUser(data?.id, data?.username, data?.password, data?.e_department_name, data?.e_cabang_name, data?.nama);
        $("#fieldset-detail-user").append(inputs);
    }

    function showInputLevel() {
        $('#fieldset-level').removeClass('d-none');
        $('#i_level').select2();
    }

    function getDetailUser(id) {
        $.ajax({
            url: base_url + link + "/get_detail_user_hris",
            type: "POST",
            data: {
                id: id
            },
            success: function(result) {
                console.log(result);
                const data = JSON.parse(result);
                toggleCardDetailUser(data);

                showInputLevel();
            }
        })
    }

    // function toggleStatus() {
    //     $confirm = window.confirm('Are you sure?');
    //     if ($confirm) {
    //         console.log('yes');
    //     } else {
    //         console.log('no');
    //     }
    // }

    // $('.btn-toggle-status').each(function() {
    //     $(this).on('click', function() {
    //         toggleStatus();
    //     })
    // })

    // $(".submit").on("click", function() {
    //     var form = $('.form-validate-jquery').valid();
    //     if (form) {
    //         sweet_create(link);
    //     }
    // });

});