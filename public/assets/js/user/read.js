class Data {
    constructor(link, column) {
        this.link = link;
        this.column = column;
        self = this;
    }

    change_status() {
        const btn = document.querySelectorAll(".status");
        if(btn) {
            btn.forEach(function(swalCloseElement) {
                swalCloseElement.addEventListener("click", function(e) {
                    const id = e.target.dataset.id;
                    const folder = e.target.dataset.folder;
                    xmlhttp.onload = function() {
                        swal.fire({
                            title: "Good job!",
                            text: "You clicked the button!",
                            icon: "success",
                            showCloseButton: true,
                            confirmButtonClass: "btn btn-primary",
                            // showConfirmButton:false,
                        }).then(function(result) {
                            $("#serverside").DataTable().ajax.reload();
                            // window.location = base_url + folder;
                        });
                    };
                    xmlhttp.open("GET", base_url + folder + "/change_status/?id=" + id);
                    xmlhttp.send();
                });
            });
        }
    };

    // Datatable With Button Create and Sweart Alert
    DataTable_Create(link_create){
        if(!$().DataTable) {
            console.warn("Warning - datatables.min.js is not loaded.");
            return;
        }
        // Setting datatable defaults
        $.extend($.fn.dataTable.defaults, {
            autoWidth: false,
            dom: '<"datatable-header justify-content-start"f<"ms-sm-auto"l><"ms-sm-3"B>><"datatable-scroll-wrap"t><"datatable-footer"ip>',
            language: {
                search: '<span class="me-3">Filter:</span> <div class="form-control-feedback form-control-feedback-end flex-fill">_INPUT_<div class="form-control-feedback-icon"><i class="ph-magnifying-glass opacity-50"></i></div></div>',
                searchPlaceholder: "Type to filter...",
                lengthMenu: '<span class="me-3">Show:</span> _MENU_',
                paginate: {
                    first: "First",
                    last: "Last",
                    next: document.dir == "rtl" ? "&larr;" : "&rarr;",
                    previous: document.dir == "rtl" ? "&rarr;" : "&larr;",
                },
            },
        });
        
        // Custom button
        var tabel = $("#serverside").DataTable({
            buttons: [{
                text: "<i class='ph-plus me-1'></i>Data",
                className: "btn bg-pink border-white",
                action: function(e, dt, node, config) {
                    window.location.href = base_url + link_create;
                },
            }, ],
            serverSide: true,
            processing: true,
            ajax: {
                url: base_url + this.link,
                type: "post",
                error: function(data, err) {
                    $(".serverside-error").html("");
                    $("#serverside tbody").empty();
                    $("#serverside").append('<tr><td class="text-center" colspan="' + column + '">No data available in table</td></tr>');
                    $("#serverside_processing").css("display", "none");
                },
            },
            lengthMenu: [
                [10, 25, 50, -1],
                [10, 25, 50, "All"],
            ],
            pageLength: 10,
            // order: [orderby],
            columnDefs: [{
                targets: [0, this.column - 1],
                width: "5%",
                orderable: false,
                /* className: "text-center", */
            }, {
                targets: [0],
                width: "3%",
                className: "text-right",
            }, ],
            bStateSave: true,
            fnStateSave: function(oSettings, oData) {
                localStorage.setItem("offersDataTables", JSON.stringify(oData));
            },
            fnStateLoad: function(oSettings) {
                return JSON.parse(localStorage.getItem("offersDataTables"));
            },
            drawCallback: function(settings) {
                self.change_status();
            },
        });
        tabel.on("draw.dt", function() {
            var info = tabel.page.info();
            tabel.column(0, {
                search: "applied",
                order: "applied",
                page: "applied",
            }).nodes().each(function(cell, i) {
                cell.innerHTML = i + 1 + info.start;
            });
        });
        $("div.dataTables_filter input", tabel.table().container()).focus();
    }

    // Datatable Without Button Create
    DataTable_Read(){
        if(!$().DataTable) {
            console.warn("Warning - datatables.min.js is not loaded.");
            return;
        }
        // Setting datatable defaults
        $.extend($.fn.dataTable.defaults, {
            autoWidth: false,
            dom: '<"datatable-header justify-content-start"f<"ms-sm-auto"l><"ms-sm-3"B>><"datatable-scroll-wrap"t><"datatable-footer"ip>',
            language: {
                search: '<span class="me-3">Filter:</span> <div class="form-control-feedback form-control-feedback-end flex-fill">_INPUT_<div class="form-control-feedback-icon"><i class="ph-magnifying-glass opacity-50"></i></div></div>',
                searchPlaceholder: "Type to filter...",
                lengthMenu: '<span class="me-3">Show:</span> _MENU_',
                paginate: {
                    first: "First",
                    last: "Last",
                    next: document.dir == "rtl" ? "&larr;" : "&rarr;",
                    previous: document.dir == "rtl" ? "&rarr;" : "&larr;",
                },
            },
        });
    
        var tabel = $("#serverside").DataTable({
            serverSide: true,
            processing: true,
            ajax: {
                url: base_url + this.link,
                type: "post",
                error: function(data, err) {
                    $(".serverside-error").html("");
                    $("#serverside tbody").empty();
                    $("#serverside").append('<tr><td class="text-center" colspan="' + column + '">No data available in table</td></tr>');
                    $("#serverside_processing").css("display", "none");
                },
            },
            lengthMenu: [
                [10, 25, 50, -1],
                [10, 25, 50, "All"],
            ],
            pageLength: 10,
            // order: [orderby],
            columnDefs: [{
                targets: [0, this.column - 1],
                width: "5%",
                orderable: false,
                /* className: "text-center", */
            }, {
                targets: [0],
                width: "3%",
                className: "text-right",
            }, ],
            bStateSave: true,
            fnStateSave: function(oSettings, oData) {
                localStorage.setItem("offersDataTables", JSON.stringify(oData));
            },
            fnStateLoad: function(oSettings) {
                return JSON.parse(localStorage.getItem("offersDataTables"));
            },
            drawCallback: function(settings) {
                change_status();
                edit_data();
            },
        });
        tabel.on("draw.dt", function() {
            var info = tabel.page.info();
            tabel.column(0, {
                search: "applied",
                order: "applied",
                page: "applied",
            }).nodes().each(function(cell, i) {
                cell.innerHTML = i + 1 + info.start;
            });
        });
        $("div.dataTables_filter input", tabel.table().container()).focus();
    }
}

// Initialize module
// ------------------------------
document.addEventListener('DOMContentLoaded', function() {
    let link = '/' + document.querySelector('#path').value + "/control/serverside";
    let link_create = '/' + document.querySelector('#path').value + "/control/create";
    let column = document.querySelector('#serverside').rows[0].cells.length;
    let i_menu = document.querySelector("#i_menu").value;
    let MyData = new Data(link, column);
    MyData.DataTable_Create(link_create);
});