/* ------------------------------------------------------------------------------
 *
 *  # Custom JS code
 *
 *  Place here all your custom js. Make sure it's loaded after app.js
 *
 * ---------------------------------------------------------------------------- */

var timerInterval;
const xmlhttp = new XMLHttpRequest();
const set_department = function() {
    const btn = document.querySelectorAll(".department");
    btn.forEach(function(href) {
        href.addEventListener("click", function(e) {
            const id = e.target.dataset.id;
            const name = e.target.dataset.name;
            xmlhttp.onload = function() {
                // console.log(this.responseText);
                window.location = base_url;
            };
            xmlhttp.open("GET", base_url + "/auth/set_department/?id=" + id + "&name = " + name);
            xmlhttp.send();
        });
    });
};

// Set Level
const set_level = function() {
    const btn = document.querySelectorAll(".level");
    btn.forEach(function(href) {
        href.addEventListener("click", function(e) {
            const id = e.target.dataset.id;
            const name = e.target.dataset.name;
            xmlhttp.onload = function() {
                window.location = base_url;
            };
            xmlhttp.open("GET", base_url + "/auth/set_level/?id=" + id + "&name = " + name);
            xmlhttp.send();
        });
    });
};

// Set Language
const set_language = function() {
    const btn = document.querySelectorAll(".language");
    btn.forEach(function(href) {
        href.addEventListener("click", function(e) {
            const id = e.target.dataset.id;
            xmlhttp.onload = function() {
                // console.log(this.responseText);
                // document.getElementById("txtHint").innerHTML = this.responseText;
                window.location = base_url;
            };
            xmlhttp.open("GET", base_url + "/auth/set_language/?language=" + id);
            xmlhttp.send();
        });
    });
};

// Change Status
const change_status = function() {
    const btn = document.querySelectorAll(".status");
    if (btn) {
        btn.forEach(function(swalCloseElement) {
            swalCloseElement.addEventListener("click", function(e) {
                /* swal.fire({
                    title: "Good job!",
                    text: "You clicked the button!",
                    icon: "success",
                    showCloseButton: true,
                    showConfirmButton:false,
                }); */
                const id = e.target.dataset.id;
                const folder = e.target.dataset.folder;
                xmlhttp.onload = function() {
                    swal.fire({
                        title: "Good job!",
                        text: "You clicked the button!",
                        icon: "success",
                        showCloseButton: true,
                        confirmButtonClass: "btn btn-primary",
                        // showConfirmButton:false,
                    }).then(function(result) {
                        $("#serverside").DataTable().ajax.reload();
                        // window.location = base_url + folder;
                    });
                };
                xmlhttp.open("GET", base_url + folder + "/change_status/?id=" + id);
                xmlhttp.send();
            });
        });
    }
};

const edit_data = function() {
    // console.log(link);
    const pensil = document.querySelectorAll(".edit_data");
    if (pensil) {
        pensil.forEach(function(swalCloseElement) {
            swalCloseElement.addEventListener("click", function(e) {
                const id = e.target.dataset.id;
                const name = e.target.dataset.name;
                const url = e.target.dataset.url;

                // console.log(id, name);
                const swalInit = swal.mixin({
                    buttonsStyling: false,
                    customClass: {
                        confirmButton: "btn btn-primary",
                        cancelButton: "btn btn-pink",
                        denyButton: "btn btn-light",
                        input: "form-control",
                    },
                });
                swalInit.fire({
                    title: "Department name",
                    input: "text",
                    inputValue: name,
                    inputPlaceholder: "Department name",
                    showCancelButton: true,
                    inputValidator: function(value) {
                        return !value && "You need to write something!";
                    },
                }).then(function(result) {
                    if (result.value) {
                        const value = result.value;
                        xmlhttp.onload = function() {
                            let timerInterval;
                            swalInit.fire({
                                title: "Success!",
                                text: "Success update data " + value,
                                icon: "success",
                                timer: 1500,
                                timerProgressBar: true,
                                didOpen: function() {
                                    Swal.showLoading();
                                    const b = Swal.getHtmlContainer().querySelector("b");
                                    timerInterval = setInterval(() => {
                                        b.textContent = Swal.getTimerLeft();
                                    }, 100);
                                },
                                willClose: function() {
                                    clearInterval(timerInterval);
                                },
                            }).then(function(result) {
                                if (result.dismiss === Swal.DismissReason.timer) {
                                    // console.log('I was closed by the timer')
                                    $("#serverside").DataTable().ajax.reload();
                                }
                            });
                        };
                        xmlhttp.open("GET", url + "/?id=" + id + "&e_name=" + value);
                        // xmlhttp.open("GET", base_url + "/?id="+e_name=" + value);
                        xmlhttp.send();
                    }
                });
            });
        });
    }
};
// Datatable 
const DataTableRead = function(link, column, order = 0) {
    if (!$().DataTable) {
        console.warn("Warning - datatables.min.js is not loaded.");
        return;
    }
    if (order != 0) {
        var orderby = order;
    } else {
        var orderby = [1, "asc"];
    }
    // Setting datatable defaults
    $.extend($.fn.dataTable.defaults, {
        autoWidth: false,
        dom: '<"datatable-header justify-content-start"f<"ms-sm-auto"l><"ms-sm-3"B>><"datatable-scroll-wrap"t><"datatable-footer"ip>',
        language: {
            search: '<span class="me-3">Filter:</span> <div class="form-control-feedback form-control-feedback-end flex-fill">_INPUT_<div class="form-control-feedback-icon"><i class="ph-magnifying-glass opacity-50"></i></div></div>',
            searchPlaceholder: "Type to filter...",
            lengthMenu: '<span class="me-3">Show:</span> _MENU_',
            paginate: {
                first: "First",
                last: "Last",
                next: document.dir == "rtl" ? "&larr;" : "&rarr;",
                previous: document.dir == "rtl" ? "&rarr;" : "&larr;",
            },
        },
    });

    var tabel = $("#serverside").DataTable({
        serverSide: true,
        processing: true,
        ajax: {
            url: base_url + link,
            type: "post",
            error: function(data, err) {
                $(".serverside-error").html("");
                $("#serverside tbody").empty();
                $("#serverside").append('<tr><td class="text-center" colspan="' + column + '">No data available in table</td></tr>');
                $("#serverside_processing").css("display", "none");
            },
        },
        lengthMenu: [
            [10, 25, 50, -1],
            [10, 25, 50, "All"],
        ],
        pageLength: 10,
        order: [orderby],
        columnDefs: [{
            targets: [0, column - 1],
            width: "5%",
            orderable: false,
            /* className: "text-center", */
        }, {
            targets: [0],
            width: "3%",
            className: "text-right",
        }, ],
        bStateSave: true,
        fnStateSave: function(oSettings, oData) {
            localStorage.setItem("offersDataTables", JSON.stringify(oData));
        },
        fnStateLoad: function(oSettings) {
            return JSON.parse(localStorage.getItem("offersDataTables"));
        },
        drawCallback: function(settings) {
            change_status();
            edit_data();
        },
    });
    tabel.on("draw.dt", function() {
        var info = tabel.page.info();
        tabel.column(0, {
            search: "applied",
            order: "applied",
            page: "applied",
        }).nodes().each(function(cell, i) {
            cell.innerHTML = i + 1 + info.start;
        });
    });
    $("div.dataTables_filter input", tabel.table().container()).focus();
}

// Datatable Create
const DatatableButtonsCreate = function(link, column, linkcreate, color, order = 0) {
    if (!$().DataTable) {
        console.warn("Warning - datatables.min.js is not loaded.");
        return;
    }
    // Setting datatable defaults
    $.extend($.fn.dataTable.defaults, {
        autoWidth: false,
        dom: '<"datatable-header justify-content-start"f<"ms-sm-auto"l><"ms-sm-3"B>><"datatable-scroll-wrap"t><"datatable-footer"ip>',
        language: {
            search: '<span class="me-3">Filter:</span> <div class="form-control-feedback form-control-feedback-end flex-fill">_INPUT_<div class="form-control-feedback-icon"><i class="ph-magnifying-glass opacity-50"></i></div></div>',
            searchPlaceholder: "Type to filter...",
            lengthMenu: '<span class="me-3">Show:</span> _MENU_',
            paginate: {
                first: "First",
                last: "Last",
                next: document.dir == "rtl" ? "&larr;" : "&rarr;",
                previous: document.dir == "rtl" ? "&rarr;" : "&larr;",
            },
        },
    });
    if (order != 0) {
        var orderby = order;
    } else {
        var orderby = [1, "asc"];
    }
    // Custom button
    var tabel = $("#serverside").DataTable({
        buttons: [{
            text: "<i class='ph-plus me-1'></i>Data",
            className: "btn bg-pink border-white",
            action: function(e, dt, node, config) {
                // window.location.href = base_url + linkcreate;
                /* swal.fire({
                    title: "Good job!",
                    text: "You clicked the button!",
                    icon: "success",
                    showCloseButton: true,
                    confirmButtonClass : 'btn btn-primary',
                    // showConfirmButton:false,
                }); */
                const swalInit = swal.mixin({
                    buttonsStyling: false,
                    customClass: {
                        confirmButton: "btn btn-primary",
                        cancelButton: "btn btn-pink",
                        denyButton: "btn btn-light",
                        input: "form-control",
                    },
                });
                swalInit.fire({
                    title: "Department name",
                    input: "text",
                    inputPlaceholder: "Department name",
                    showCancelButton: true,
                    inputValidator: function(value) {
                        return !value && "You need to write something!";
                    },
                }).then(function(result) {
                    if (result.value) {
                        /* swalInit.fire({
                            icon: 'success',
                            html: 'Hi, ' + result.value
                        }); */
                        const value = result.value;
                        xmlhttp.onload = function() {
                            // console.log(this.responseText);
                            // document.getElementById("txtHint").innerHTML = this.responseText;
                            // window.location = base_url;
                            // $('#serverside').DataTable().ajax.reload();
                            // swalInit.fire({
                            //     title: "Success!",
                            //     text: "Success save data " + value,
                            //     icon: "success",
                            //     showCloseButton: true,
                            //     // showConfirmButton:false,
                            // }).then(
                            //     function (result) {
                            //         $('#serverside').DataTable().ajax.reload();
                            //         // window.location = base_url + folder;
                            //     }
                            // );
                            swalInit.fire({
                                title: "Success!",
                                text: "Success save data " + value,
                                icon: "success",
                                timer: 1500,
                                timerProgressBar: true,
                                didOpen: function() {
                                    Swal.showLoading();
                                    const b = Swal.getHtmlContainer().querySelector("b");
                                    timerInterval = setInterval(() => {
                                        b.textContent = Swal.getTimerLeft();
                                    }, 100);
                                },
                                willClose: function() {
                                    clearInterval(timerInterval);
                                },
                            }).then(function(result) {
                                if (result.dismiss === Swal.DismissReason.timer) {
                                    // console.log('I was closed by the timer')
                                    $("#serverside").DataTable().ajax.reload();
                                }
                            });
                        };
                        xmlhttp.open("GET", base_url + linkcreate + "/?e_name=" + value);
                        xmlhttp.send();
                    }
                });
            },
        }, ],
        serverSide: true,
        processing: true,
        ajax: {
            url: base_url + link,
            type: "post",
            error: function(data, err) {
                $(".serverside-error").html("");
                $("#serverside tbody").empty();
                $("#serverside").append('<tr><td class="text-center" colspan="' + column + '">No data available in table</td></tr>');
                $("#serverside_processing").css("display", "none");
            },
        },
        lengthMenu: [
            [10, 25, 50, -1],
            [10, 25, 50, "All"],
        ],
        pageLength: 10,
        order: [orderby],
        columnDefs: [{
            targets: [0, column - 1],
            width: "5%",
            orderable: false,
            /* className: "text-center", */
        }, {
            targets: [0],
            width: "3%",
            className: "text-right",
        }, ],
        bStateSave: true,
        fnStateSave: function(oSettings, oData) {
            localStorage.setItem("offersDataTables", JSON.stringify(oData));
        },
        fnStateLoad: function(oSettings) {
            return JSON.parse(localStorage.getItem("offersDataTables"));
        },
        drawCallback: function(settings) {
            change_status();
            edit_data();
        },
    });
    tabel.on("draw.dt", function() {
        var info = tabel.page.info();
        tabel.column(0, {
            search: "applied",
            order: "applied",
            page: "applied",
        }).nodes().each(function(cell, i) {
            cell.innerHTML = i + 1 + info.start;
        });
    });
    $("div.dataTables_filter input", tabel.table().container()).focus();
};

// Create Submit Form
const Create = function(link) {
    var forms = document.querySelectorAll('.needs-validation');
    forms.forEach(function(form) {
        form.addEventListener('submit', function(e) {
            e.preventDefault();
            e.stopPropagation();
            if (form.checkValidity()) {
                const swalInit = swal.mixin({
                    buttonsStyling: false,
                    customClass: {
                        confirmButton: "btn btn-primary",
                        cancelButton: "btn btn-pink",
                        denyButton: "btn btn-light",
                        input: "form-control",
                    },
                });
                swalInit.fire({
                    title: 'Are you sure?',
                    text: "Add new data",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    cancelButtonText: 'No',
                    buttonsStyling: false,
                    customClass: {
                        confirmButton: 'btn btn-success',
                        cancelButton: 'btn btn-danger'
                    }
                }).then(async function(result) {
                    if (result.value) {
                        try {
                            let req = await fetch(base_url + '/' + link + '/act_create', {
                                method: 'POST',
                                body: new FormData(e.target),
                            });
                            let res = await req.json();
                            if(res.success === true && res.ready == false && res.message == null) {
                                const swalInit = swal.mixin({
                                    buttonsStyling: false,
                                    customClass: {
                                        confirmButton: "btn btn-primary",
                                        cancelButton: "btn btn-pink",
                                        denyButton: "btn btn-light",
                                        input: "form-control",
                                    },
                                });
                                let timerInterval;
                                swalInit.fire({
                                    title: "Success!",
                                    text: "Success save data",
                                    icon: "success",
                                    timer: 1500,
                                    timerProgressBar: true,
                                }).then(function(result) {
                                    window.location = base_url + link;
                                });
                            }else if(res.success == false && res.ready == true && res.message == null) {
                                swalInit.fire("Sorry :(", "The data already exists :(", "error");
                            }else if(res.success == false && res.ready == false && res.message != null) {
                                swalInit.fire({
                                    title: '<span class="font-weight-light">Data failed to save :(</span>',
                                    icon: 'error',
                                    html: '<div class="alert alert-danger alert-dismissible fade show">' + res.message + '</div>',
                                    showCloseButton: true,
                                    showCancelButton: false,
                                    focusConfirm: false,
                                    confirmButtonText: '<i class="ph-thumbs-up"></i>',
                                    // confirmButton: "btn btn-sm btn-outline btn-danger text-white border-danger",
                                });
                            }else{
                                swalInit.fire("Sorry :(", "Data failed to save :(", "error");
                            }
                        } catch (err) {
                            console.log(err);
                        }
                    }
                });

            }
            form.classList.add('was-validated');
        }, false);
    });
}

// Create Submit Form
const Edit = function(link) {
    var forms = document.querySelectorAll('.needs-validation');
    forms.forEach(function(form) {
        form.addEventListener('submit', function(e) {
            e.preventDefault();
            e.stopPropagation();
            if (form.checkValidity()) {
                const swalInit = swal.mixin({
                    buttonsStyling: false,
                    customClass: {
                        confirmButton: "btn btn-primary",
                        cancelButton: "btn btn-pink",
                        denyButton: "btn btn-light",
                        input: "form-control",
                    },
                });
                swalInit.fire({
                    title: 'Are you sure?',
                    text: "Update data",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    cancelButtonText: 'No',
                    buttonsStyling: false,
                    customClass: {
                        confirmButton: 'btn btn-success',
                        cancelButton: 'btn btn-danger'
                    }
                }).then(async function(result) {
                    if (result.value) {
                        try {
                            let req = await fetch(base_url + '/' + link + '/act_update', {
                                method: 'POST',
                                body: new FormData(e.target),
                            });
                            let res = await req.json();
                            if(res.success === true && res.ready == false && res.message == null) {
                                const swalInit = swal.mixin({
                                    buttonsStyling: false,
                                    customClass: {
                                        confirmButton: "btn btn-primary",
                                        cancelButton: "btn btn-pink",
                                        denyButton: "btn btn-light",
                                        input: "form-control",
                                    },
                                });
                                let timerInterval;
                                swalInit.fire({
                                    title: "Success!",
                                    text: "Success update data",
                                    icon: "success",
                                    timer: 1500,
                                    timerProgressBar: true,
                                }).then(function(result) {
                                    window.location = base_url + link;
                                });
                            }else if(res.success == false && res.ready == true && res.message == null) {
                                swalInit.fire("Sorry :(", "The data already exists :(", "error");
                            }else if(res.success == false && res.ready == false && res.message != null) {
                                swalInit.fire({
                                    title: '<span class="font-weight-light">Data failed to save :(</span>',
                                    icon: 'error',
                                    html: '<div class="alert alert-danger alert-dismissible fade show">' + res.message + '</div>',
                                    showCloseButton: true,
                                    showCancelButton: false,
                                    focusConfirm: false,
                                    confirmButtonText: '<i class="ph-thumbs-up"></i>',
                                    // confirmButton: "btn btn-sm btn-outline btn-danger text-white border-danger",
                                });
                            }else{
                                swalInit.fire("Sorry :(", "Data failed to update :(", "error");
                            }
                        } catch (err) {
                            console.log(err);
                        }
                    }
                });

            }
            form.classList.add('was-validated');
        }, false);
    });
}

// select2
const Select2 = function() {
    $('.multi-select').select2();
    $(".select-single").select2({
        width: "100%",
        allowClear: true,
        minimumInputLength: 0,
        ajax: {
            url: function() {
                return $(this).data('action');
            },
            dataType: "json",
            delay: 250,
            data: function(params) {
                var query = {
                    q: params.term,
                };
                return query;
            },
            processResults: function(data) {
                return {
                    results: data,
                };
            },
            cache: true,
        },
    });
};

// Load Content
document.addEventListener("DOMContentLoaded", function() {
    set_department();
    set_level();
    set_language();
});