class CRUD {
    constructor(link, link_create, column) {
        this.link = link;
        this.link_create = link_create;
        this.column = column;
        self = this;
    }

    // Datatable With Button Create and Modals
    DataTable_Modal_Create() {
        if (!$().DataTable) {
            console.warn("Warning - datatables.min.js is not loaded.");
            return;
        }
        // Setting datatable defaults
        $.extend($.fn.dataTable.defaults, {
            autoWidth: false,
            dom: '<"datatable-header justify-content-start"f<"ms-sm-auto"l><"ms-sm-3"B>><"datatable-scroll-wrap"t><"datatable-footer"ip>',
            language: {
                search: '<span class="me-3">Filter:</span> <div class="form-control-feedback form-control-feedback-end flex-fill">_INPUT_<div class="form-control-feedback-icon"><i class="ph-magnifying-glass opacity-50"></i></div></div>',
                searchPlaceholder: "Type to filter...",
                lengthMenu: '<span class="me-3">Show:</span> _MENU_',
                paginate: {
                    first: "First",
                    last: "Last",
                    next: document.dir == "rtl" ? "&larr;" : "&rarr;",
                    previous: document.dir == "rtl" ? "&rarr;" : "&larr;",
                },
            },
        });

        // Custom button
        var tabel = $("#serverside").DataTable({
            buttons: [{
                text: "<i class='ph-plus me-1'></i>Data",
                className: "btn bg-pink border-white",
                /* attr:  {
                    'data-bs-toggle': 'modal',
                    'data-bs-target': '#modal'
                }, */
                action: function (e, dt, node, config) {
                    $(".modal_form_create").modal('show');
                },
            },],
            serverSide: true,
            processing: true,
            ajax: {
                url: base_url + this.link,
                type: "post",
                error: function (data, err) {
                    $(".serverside-error").html("");
                    $("#serverside tbody").empty();
                    $("#serverside").append('<tr><td class="text-center" colspan="' + column + '">No data available in table</td></tr>');
                    $("#serverside_processing").css("display", "none");
                },
            },
            lengthMenu: [
                [10, 25, 50, -1],
                [10, 25, 50, "All"],
            ],
            pageLength: 10,
            // order: [orderby],
            columnDefs: [{
                targets: [0, this.column - 1],
                width: "5%",
                orderable: false,
                /* className: "text-center", */
            }, {
                targets: [0],
                width: "3%",
                className: "text-right",
            },],
            bStateSave: true,
            fnStateSave: function (oSettings, oData) {
                localStorage.setItem("offersDataTables", JSON.stringify(oData));
            },
            fnStateLoad: function (oSettings) {
                return JSON.parse(localStorage.getItem("offersDataTables"));
            },
            drawCallback: function (settings) {
                change_status();
                self.Get_Data_Edit();
                // edit_data();
            },
        });
        tabel.on("draw.dt", function () {
            var info = tabel.page.info();
            tabel.column(0, {
                search: "applied",
                order: "applied",
                page: "applied",
            }).nodes().each(function (cell, i) {
                cell.innerHTML = i + 1 + info.start;
            });
        });
        $("div.dataTables_filter input", tabel.table().container()).focus();
    }

    // Datatable With Button Create and Sweart Alert
    DataTable_Alert_Create() {
        if (!$().DataTable) {
            console.warn("Warning - datatables.min.js is not loaded.");
            return;
        }
        // Setting datatable defaults
        $.extend($.fn.dataTable.defaults, {
            autoWidth: false,
            dom: '<"datatable-header justify-content-start"f<"ms-sm-auto"l><"ms-sm-3"B>><"datatable-scroll-wrap"t><"datatable-footer"ip>',
            language: {
                search: '<span class="me-3">Filter:</span> <div class="form-control-feedback form-control-feedback-end flex-fill">_INPUT_<div class="form-control-feedback-icon"><i class="ph-magnifying-glass opacity-50"></i></div></div>',
                searchPlaceholder: "Type to filter...",
                lengthMenu: '<span class="me-3">Show:</span> _MENU_',
                paginate: {
                    first: "First",
                    last: "Last",
                    next: document.dir == "rtl" ? "&larr;" : "&rarr;",
                    previous: document.dir == "rtl" ? "&rarr;" : "&larr;",
                },
            },
        });

        // Custom button
        var tabel = $("#serverside").DataTable({
            buttons: [{
                text: "<i class='ph-plus me-1'></i>Data",
                className: "btn bg-pink border-white",
                action: function (e, dt, node, config) {
                    const swalInit = swal.mixin({
                        buttonsStyling: false,
                        customClass: {
                            confirmButton: "btn btn-primary",
                            cancelButton: "btn btn-pink",
                            denyButton: "btn btn-light",
                            input: "form-control",
                        },
                    });
                    swalInit.fire({
                        title: "Department name",
                        input: "text",
                        inputPlaceholder: "Department name",
                        showCancelButton: true,
                        inputValidator: function (value) {
                            return !value && "You need to write something!";
                        },
                    }).then(function (result) {
                        if (result.value) {
                            const value = result.value;
                            xmlhttp.onload = function () {
                                swalInit.fire({
                                    title: "Success!",
                                    text: "Success save data " + value,
                                    icon: "success",
                                    timer: 1500,
                                    timerProgressBar: true,
                                    didOpen: function () {
                                        Swal.showLoading();
                                        const b = Swal.getHtmlContainer().querySelector("b");
                                        timerInterval = setInterval(() => {
                                            b.textContent = Swal.getTimerLeft();
                                        }, 100);
                                    },
                                    willClose: function () {
                                        clearInterval(timerInterval);
                                    },
                                }).then(function (result) {
                                    if (result.dismiss === Swal.DismissReason.timer) {
                                        // console.log('I was closed by the timer')
                                        $("#serverside").DataTable().ajax.reload();
                                    }
                                });
                            };
                            xmlhttp.open("GET", base_url + this.link_create + "/?e_name=" + value);
                            xmlhttp.send();
                        }
                    });
                },
            },],
            serverSide: true,
            processing: true,
            ajax: {
                url: base_url + this.link,
                type: "post",
                error: function (data, err) {
                    $(".serverside-error").html("");
                    $("#serverside tbody").empty();
                    $("#serverside").append('<tr><td class="text-center" colspan="' + column + '">No data available in table</td></tr>');
                    $("#serverside_processing").css("display", "none");
                },
            },
            lengthMenu: [
                [10, 25, 50, -1],
                [10, 25, 50, "All"],
            ],
            pageLength: 10,
            // order: [orderby],
            columnDefs: [{
                targets: [0, this.column - 1],
                width: "5%",
                orderable: false,
                /* className: "text-center", */
            }, {
                targets: [0],
                width: "3%",
                className: "text-right",
            },],
            bStateSave: true,
            fnStateSave: function (oSettings, oData) {
                localStorage.setItem("offersDataTables", JSON.stringify(oData));
            },
            fnStateLoad: function (oSettings) {
                return JSON.parse(localStorage.getItem("offersDataTables"));
            },
            drawCallback: function (settings) {
                change_status();
                edit_data();
            },
        });
        tabel.on("draw.dt", function () {
            var info = tabel.page.info();
            tabel.column(0, {
                search: "applied",
                order: "applied",
                page: "applied",
            }).nodes().each(function (cell, i) {
                cell.innerHTML = i + 1 + info.start;
            });
        });
        $("div.dataTables_filter input", tabel.table().container()).focus();
    }

    // Datatable Without Button Create
    DataTable_Read() {
        if (!$().DataTable) {
            console.warn("Warning - datatables.min.js is not loaded.");
            return;
        }
        // Setting datatable defaults
        $.extend($.fn.dataTable.defaults, {
            autoWidth: false,
            dom: '<"datatable-header justify-content-start"f<"ms-sm-auto"l><"ms-sm-3"B>><"datatable-scroll-wrap"t><"datatable-footer"ip>',
            language: {
                search: '<span class="me-3">Filter:</span> <div class="form-control-feedback form-control-feedback-end flex-fill">_INPUT_<div class="form-control-feedback-icon"><i class="ph-magnifying-glass opacity-50"></i></div></div>',
                searchPlaceholder: "Type to filter...",
                lengthMenu: '<span class="me-3">Show:</span> _MENU_',
                paginate: {
                    first: "First",
                    last: "Last",
                    next: document.dir == "rtl" ? "&larr;" : "&rarr;",
                    previous: document.dir == "rtl" ? "&rarr;" : "&larr;",
                },
            },
        });

        var tabel = $("#serverside").DataTable({
            serverSide: true,
            processing: true,
            ajax: {
                url: base_url + this.link,
                type: "post",
                error: function (data, err) {
                    $(".serverside-error").html("");
                    $("#serverside tbody").empty();
                    $("#serverside").append('<tr><td class="text-center" colspan="' + column + '">No data available in table</td></tr>');
                    $("#serverside_processing").css("display", "none");
                },
            },
            lengthMenu: [
                [10, 25, 50, -1],
                [10, 25, 50, "All"],
            ],
            pageLength: 10,
            // order: [orderby],
            columnDefs: [{
                targets: [0, this.column - 1],
                width: "5%",
                orderable: false,
                /* className: "text-center", */
            }, {
                targets: [0],
                width: "3%",
                className: "text-right",
            },],
            bStateSave: true,
            fnStateSave: function (oSettings, oData) {
                localStorage.setItem("offersDataTables", JSON.stringify(oData));
            },
            fnStateLoad: function (oSettings) {
                return JSON.parse(localStorage.getItem("offersDataTables"));
            },
            drawCallback: function (settings) {
                change_status();
                edit_data();
            },
        });
        tabel.on("draw.dt", function () {
            var info = tabel.page.info();
            tabel.column(0, {
                search: "applied",
                order: "applied",
                page: "applied",
            }).nodes().each(function (cell, i) {
                cell.innerHTML = i + 1 + info.start;
            });
        });
        $("div.dataTables_filter input", tabel.table().container()).focus();
    }

    Valid() {
        var forms = document.querySelectorAll('.needs-validation-create');
        // Loop over them and prevent submission
        forms.forEach(function (form) {
            form.addEventListener('submit', function (e) {
                e.preventDefault();
                e.stopPropagation();
                if (!form.checkValidity()) {
                    console.log('kampret');
                } else {
                    // self.Create();
                    let btn = document.querySelectorAll(".submit");
                    btn.forEach(function (b) {
                        console.log(b.dataset.submit);
                        /* const type = e.target.dataset.submit;
                        if (type==='create') {
                            self.Create();
                        }else{
                            self.Edit();
                        } */
                    }, false);
                    // console.log(btn.dataset.submit);
                    /*btn.forEach(function(b) {
                        b.addEventListener("click", function(e) {
                            const type = e.target.dataset.submit;
                            if (type==='create') {
                                self.Create();
                            }else{
                                self.Edit();
                            }
                        }, false);
                    }); */
                }
                form.classList.add('was-validated');
            }, false);
        });
    }

    Create(link_create) {
        var forms = document.querySelectorAll('.needs-validation-create');
        forms.forEach(function (form) {
            form.addEventListener('submit', async function (e) {
                e.preventDefault();
                e.stopPropagation();
                if (form.checkValidity()) {
                    try {
                        let req = await fetch(base_url + '/' + link_create, {
                            method: 'POST',
                            body: new FormData(e.target),
                        });
                        let res = await req.json();
                        if (res.success === true) {
                            $('.modal_form_create').modal('hide');
                            const swalInit = swal.mixin({
                                buttonsStyling: false,
                                customClass: {
                                    confirmButton: "btn btn-primary",
                                    cancelButton: "btn btn-pink",
                                    denyButton: "btn btn-light",
                                    input: "form-control",
                                },
                            });
                            let timerInterval;
                            swalInit.fire({
                                title: "Success!",
                                text: "Success save data",
                                icon: "success",
                                timer: 1500,
                                timerProgressBar: true,
                                /* didOpen: function () {
                                    Swal.showLoading();
                                    const b = Swal.getHtmlContainer().querySelector("b");
                                    timerInterval = setInterval(() => {
                                        b.textContent = Swal.getTimerLeft();
                                    }, 100);
                                },
                                willClose: function () {
                                    clearInterval(timerInterval);
                                }, */
                            }).then(function (result) {
                                if (result.dismiss === Swal.DismissReason.timer) {
                                    $("#serverside").DataTable().ajax.reload();
                                    document.querySelector(".needs-validation-create").reset();
                                }
                            });

                            // bootbox.alert({
                            //     label: 'Success!',
                            //     title: 'Success!',
                            //     className: 'btn-success',
                            //     message: 'This is a great success!',
                            //     callback: function() {
                            //         $("#serverside").DataTable().ajax.reload();
                            //         $('.modal_form_create').modal('hide');       
                            //         document.getElementsByClassName("needs-validation-create").reset();
                            //     }
                            // });
                            // bootbox.dialog({
                            //     message: 'I am a custom dialog',
                            //     title: 'Custom title',
                            //     buttons: {
                            //         success: {
                            //             label: 'Success!',
                            //             className: 'btn-success',
                            //             callback: function() {
                            //                 bootbox.alert({
                            //                     title: 'Success!',
                            //                     message: 'This is a great success!'
                            //                 });
                            //             }
                            //         },
                            //         danger: {
                            //             label: 'Danger!',
                            //             className: 'btn-danger',
                            //             callback: function() {
                            //                 bootbox.alert({
                            //                     title: 'Ohh noooo!',
                            //                     message: 'Uh oh, look out!'
                            //                 });
                            //             }
                            //         },
                            //         main: {
                            //             label: 'Click ME!',
                            //             className: 'btn-primary',
                            //             callback: function() {
                            //                 bootbox.alert({
                            //                     title: 'Hooray!',
                            //                     message: 'Something awesome just happened!'
                            //                 });
                            //             }
                            //         }
                            //     }
                            // });
                        }
                    }
                    catch (err) {
                        console.log(err);
                    }
                }
                form.classList.add('was-validated');
            }, false);
        });
    }

    Get_Data_Edit(){
        const data = document.querySelectorAll('.edit_data');
        if(data) {
            data.forEach(function(row) {
                row.addEventListener("click", function(e) {
                    const id = e.target.dataset.id;
                    const name = e.target.dataset.name;
                    const note = e.target.dataset.note;
                    $(".modal_form_edit").modal('show');
                    document.querySelector('.id').value = id;
                    document.querySelector('.name').value = name;
                    document.querySelector('.note').value = note;
                });
            });
        }
    }

    Edit(link_update) {
        var forms = document.querySelectorAll('.needs-validation-edit');
        forms.forEach(function (form) {
            form.addEventListener('submit', async function (e) {
                e.preventDefault();
                e.stopPropagation();
                if (form.checkValidity()) {
                    try {
                        let req = await fetch(base_url + '/' + link_update, {
                            method: 'POST',
                            body: new FormData(e.target),
                        });
                        let res = await req.json();
                        if (res.success === true) {
                            $('.modal_form_edit').modal('hide');
                            const swalInit = swal.mixin({
                                buttonsStyling: false,
                                customClass: {
                                    confirmButton: "btn btn-primary",
                                    cancelButton: "btn btn-pink",
                                    denyButton: "btn btn-light",
                                    input: "form-control",
                                },
                            });
                            let timerInterval;
                            swalInit.fire({
                                title: "Success!",
                                text: "Success update data",
                                icon: "success",
                                timer: 1500,
                                showConfirmButton: false,
                                timerProgressBar: true,
                            }).then(function (result) {
                                // if (result.dismiss === Swal.DismissReason.timer) {
                                    $("#serverside").DataTable().ajax.reload();
                                    document.querySelector(".needs-validation-edit").reset();
                                // }
                            });
                        }else{
                            throw res;
                        }
                    }
                    catch (err) {
                        console.log(err);
                    }
                }
                form.classList.add('was-validated');
            }, false);
        });
    }
}

// Initialize module
// ------------------------------
document.addEventListener('DOMContentLoaded', function () {
    let link = '/' + $("#path").val() + "/control/serverside";
    let link_create = '/' + $("#path").val() + "/control/act_create";
    let link_update = '/' + $("#path").val() + "/control/act_update";
    let i_menu = document.querySelector("#i_menu").value;
    let modal = document.querySelector(".modol");
    // console.log(i_menu, modal);
    let MyData = new CRUD(link, link_create);
    if (i_menu === null || i_menu == '' || typeof (i_menu) === 'undefined') {
        MyData.DataTable_Read();
    } else {
        if (modal) {
            MyData.DataTable_Modal_Create();
        } else {
            MyData.DataTable_Alert_Create();
        }
    }

    // var forms = document.querySelectorAll('.needs-validation');
    //     // Loop over them and prevent submission
    //     forms.forEach(function(form) {
    //         form.addEventListener('submit', function(e) {
    //             console.log('kampret');
    //             if (!form.checkValidity()) {
    //                 e.preventDefault();
    //                 e.stopPropagation();
    //             }
    //             form.classList.add('was-validated');
    //         }, false);
    // });
    MyData.Create(link_create);
    MyData.Edit(link_update);
    /* let btn = document.querySelectorAll(".submit");
    btn.forEach(function(b) {
        b.addEventListener("click", function(e) {
            const type = e.target.dataset.submit;
            if (type==='create') {
                MyData.Create();
            }else{
                MyData.Edit();
            }
        }, false);
    }); */
});
