const swalButtonStyling = {
    buttonsStyling: false,
    confirmButtonClass: "btn btn-sm btn-pink text-white",
    cancelButtonClass: "btn btn-sm btn-secondary",
    confirmButtonText: '<i class="ph-check"></i> Yes',
    cancelButtonText: '<i class="ph-x"></i> No',
} 

// Action soft delete
function sweet_delete_soft(link, id) {
    var swalInit = swal.mixin(swalButtonStyling);

    return new swalInit({
        title: "Are you sure?",
        text: "This data will be canceled :)",
        type: "error",
        showCancelButton: true,
        buttonsStyling: false,
    }).then(function(result) {
        if (result.value) {
            $.ajax({
                type: "POST",
                data: {
                    id: id,
                },
                url: base_url + link + "/act_soft_delete",
                dataType: "json",
                success: function(data) {
                    if (data.success == true) {
                        return new swalInit("Success!", "Data delete successfully :)", "success").then(
                            function(result) {
                                window.location = base_url + link;
                            }
                        );
                    }

                    return new swalInit("Sorry :(", "Data failed to deleted :(", "error");
                },
            });
        }
    });
}


// Action soft delete
function sweet_delete(link, id) {
    var swalInit = swal.mixin(swalButtonStyling);

    return new swalInit({
        title: "Are you sure?",
        text: "This data will be canceled :)",
        type: "error",
        showCancelButton: true,
        buttonsStyling: false,
    }).then(function(result) {
        if (result.value) {
            $.ajax({
                type: "POST",
                data: {
                    id: id,
                },
                url: base_url + link + "/act_delete",
                dataType: "json",
                success: function(data) {
                    if (data.success == true) {
                        return new swalInit("Success!", "Data delete successfully :)", "success").then(
                            function(result) {
                                window.location = base_url + link;
                            }
                        );
                    }

                    return new swalInit("Sorry :(", "Data failed to deleted :(", "error");
                },
            });
        }
    });
}

function sweet_update(link) {    
    var swalInit = swal.mixin(swalButtonStyling);

    return new swalInit({
        title: "Are you sure?",
        text: "This data will be saved :)",
        type: "question",
        showCancelButton: true,
        buttonsStyling: false,
    }).then(function(result) {
        if (result.value) {
            $.ajax({
                type: "POST",
                data: $("form").serialize(),
                url: base_url + link + "/act_update",
                dataType: "json",                
                success: function(data) {
                    if (data.success == true && data.ready == false && data.message == null) {
                        return new swalInit("Success!", "Data " + data.data + " update successfully :)", "success")
                                .then(function(result) {
                                    window.location = base_url + link;
                                }
                        );
                    } 

                    return new swalInit("Sorry :(", "Data failed to update :(", "error");
                },
            });
        }
    });
}

