PGDMP                         z            Budgeting_Finance %   10.17 (Ubuntu 10.17-0ubuntu0.18.04.1)    10.17 �    �           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false            �           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false            �           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                       false                        2615    2200    public    SCHEMA        CREATE SCHEMA public;
    DROP SCHEMA public;
             postgres    false            �           0    0    SCHEMA public    COMMENT     6   COMMENT ON SCHEMA public IS 'standard public schema';
                  postgres    false    3            �            1259    1213129    ci_sessions    TABLE     �   CREATE TABLE public.ci_sessions (
    id character varying(128) NOT NULL,
    ip_address inet NOT NULL,
    "timestamp" timestamp with time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    data bytea DEFAULT '\x'::bytea NOT NULL
);
    DROP TABLE public.ci_sessions;
       public         postgres    false    3            �            1259    1214876    ci_sessions_3    TABLE     �   CREATE TABLE public.ci_sessions_3 (
    id character varying(128) NOT NULL,
    ip_address character varying(45) NOT NULL,
    "timestamp" bigint DEFAULT 0 NOT NULL,
    data text DEFAULT ''::text NOT NULL
);
 !   DROP TABLE public.ci_sessions_3;
       public         postgres    false    3            �            1259    1213437    tm_log    TABLE     �   CREATE TABLE public.tm_log (
    i_log integer NOT NULL,
    i_user integer NOT NULL,
    ip_address character varying(16) NOT NULL,
    "time" timestamp without time zone DEFAULT now() NOT NULL,
    activity text
);
    DROP TABLE public.tm_log;
       public         postgres    false    3            �            1259    1213435    tm_log_i_log_seq    SEQUENCE     �   CREATE SEQUENCE public.tm_log_i_log_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.tm_log_i_log_seq;
       public       postgres    false    3    204            �           0    0    tm_log_i_log_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE public.tm_log_i_log_seq OWNED BY public.tm_log.i_log;
            public       postgres    false    203            �            1259    1213702    tm_user_cover    TABLE     �   CREATE TABLE public.tm_user_cover (
    i_user_cover integer NOT NULL,
    i_level integer NOT NULL,
    i_department integer NOT NULL,
    i_category integer NOT NULL
);
 !   DROP TABLE public.tm_user_cover;
       public         postgres    false    3            �            1259    1213700    tm_user_cover_i_user_cover_seq    SEQUENCE     �   CREATE SEQUENCE public.tm_user_cover_i_user_cover_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 5   DROP SEQUENCE public.tm_user_cover_i_user_cover_seq;
       public       postgres    false    221    3            �           0    0    tm_user_cover_i_user_cover_seq    SEQUENCE OWNED BY     a   ALTER SEQUENCE public.tm_user_cover_i_user_cover_seq OWNED BY public.tm_user_cover.i_user_cover;
            public       postgres    false    220            �            1259    1214682    tm_user_department_role    TABLE     �   CREATE TABLE public.tm_user_department_role (
    i_user_department_role integer NOT NULL,
    i_user integer NOT NULL,
    i_department integer NOT NULL,
    i_level integer NOT NULL
);
 +   DROP TABLE public.tm_user_department_role;
       public         postgres    false    3            �            1259    1214680 2   tm_user_department_role_i_user_department_role_seq    SEQUENCE     �   CREATE SEQUENCE public.tm_user_department_role_i_user_department_role_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 I   DROP SEQUENCE public.tm_user_department_role_i_user_department_role_seq;
       public       postgres    false    225    3            �           0    0 2   tm_user_department_role_i_user_department_role_seq    SEQUENCE OWNED BY     �   ALTER SEQUENCE public.tm_user_department_role_i_user_department_role_seq OWNED BY public.tm_user_department_role.i_user_department_role;
            public       postgres    false    224            �            1259    1213731    tm_user_role    TABLE     �   CREATE TABLE public.tm_user_role (
    i_user_role integer NOT NULL,
    i_menu integer NOT NULL,
    i_level integer NOT NULL,
    i_department integer NOT NULL,
    i_access integer NOT NULL
);
     DROP TABLE public.tm_user_role;
       public         postgres    false    3            �            1259    1213729    tm_user_role_i_user_role_seq    SEQUENCE     �   CREATE SEQUENCE public.tm_user_role_i_user_role_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 3   DROP SEQUENCE public.tm_user_role_i_user_role_seq;
       public       postgres    false    223    3            �           0    0    tm_user_role_i_user_role_seq    SEQUENCE OWNED BY     ]   ALTER SEQUENCE public.tm_user_role_i_user_role_seq OWNED BY public.tm_user_role.i_user_role;
            public       postgres    false    222            �            1259    1213154 	   tr_access    TABLE     �   CREATE TABLE public.tr_access (
    i_access integer NOT NULL,
    e_access_name character varying(20) DEFAULT NULL::character varying,
    CONSTRAINT c_tr_access CHECK ((NOT (btrim((e_access_name)::text) = ''::text)))
);
    DROP TABLE public.tr_access;
       public         postgres    false    3            �            1259    1213152    tr_access_i_access_seq    SEQUENCE     �   CREATE SEQUENCE public.tr_access_i_access_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.tr_access_i_access_seq;
       public       postgres    false    198    3            �           0    0    tr_access_i_access_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE public.tr_access_i_access_seq OWNED BY public.tr_access.i_access;
            public       postgres    false    197            �            1259    1213595    tr_category    TABLE     V  CREATE TABLE public.tr_category (
    i_category integer NOT NULL,
    e_category_name character varying(500),
    f_active boolean DEFAULT true,
    d_entry timestamp without time zone DEFAULT now() NOT NULL,
    d_update timestamp without time zone,
    CONSTRAINT c_tr_category CHECK ((NOT (btrim((e_category_name)::text) = ''::text)))
);
    DROP TABLE public.tr_category;
       public         postgres    false    3            �            1259    1213593    tr_category_i_category_seq    SEQUENCE     �   CREATE SEQUENCE public.tr_category_i_category_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 1   DROP SEQUENCE public.tr_category_i_category_seq;
       public       postgres    false    3    215            �           0    0    tr_category_i_category_seq    SEQUENCE OWNED BY     Y   ALTER SEQUENCE public.tr_category_i_category_seq OWNED BY public.tr_category.i_category;
            public       postgres    false    214            �            1259    1213639    tr_category_sub    TABLE     �  CREATE TABLE public.tr_category_sub (
    i_category_sub integer NOT NULL,
    e_category_sub_name character varying(500),
    i_category integer NOT NULL,
    f_active boolean DEFAULT true,
    d_entry timestamp without time zone DEFAULT now() NOT NULL,
    d_update timestamp without time zone,
    CONSTRAINT c_tr_category_sub CHECK ((NOT (btrim((e_category_sub_name)::text) = ''::text)))
);
 #   DROP TABLE public.tr_category_sub;
       public         postgres    false    3            �            1259    1213637 "   tr_category_sub_i_category_sub_seq    SEQUENCE     �   CREATE SEQUENCE public.tr_category_sub_i_category_sub_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 9   DROP SEQUENCE public.tr_category_sub_i_category_sub_seq;
       public       postgres    false    217    3            �           0    0 "   tr_category_sub_i_category_sub_seq    SEQUENCE OWNED BY     i   ALTER SEQUENCE public.tr_category_sub_i_category_sub_seq OWNED BY public.tr_category_sub.i_category_sub;
            public       postgres    false    216            �            1259    1213511 
   tr_company    TABLE     Q  CREATE TABLE public.tr_company (
    i_company integer NOT NULL,
    e_company_name character varying(500),
    f_active boolean DEFAULT true,
    d_entry timestamp without time zone DEFAULT now() NOT NULL,
    d_update timestamp without time zone,
    CONSTRAINT c_tr_company CHECK ((NOT (btrim((e_company_name)::text) = ''::text)))
);
    DROP TABLE public.tr_company;
       public         postgres    false    3            �            1259    1213509    tr_company_i_company_seq    SEQUENCE     �   CREATE SEQUENCE public.tr_company_i_company_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 /   DROP SEQUENCE public.tr_company_i_company_seq;
       public       postgres    false    208    3            �           0    0    tr_company_i_company_seq    SEQUENCE OWNED BY     U   ALTER SEQUENCE public.tr_company_i_company_seq OWNED BY public.tr_company.i_company;
            public       postgres    false    207            �            1259    1213485    tr_department    TABLE     �  CREATE TABLE public.tr_department (
    i_department integer NOT NULL,
    e_department_name character varying(500),
    i_user_approve integer,
    f_skip boolean DEFAULT false,
    f_active boolean DEFAULT true,
    d_entry timestamp without time zone DEFAULT now() NOT NULL,
    d_update timestamp without time zone,
    CONSTRAINT c_tr_department CHECK ((NOT (btrim((e_department_name)::text) = ''::text)))
);
 !   DROP TABLE public.tr_department;
       public         postgres    false    3            �            1259    1213483    tr_department_i_department_seq    SEQUENCE     �   CREATE SEQUENCE public.tr_department_i_department_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 5   DROP SEQUENCE public.tr_department_i_department_seq;
       public       postgres    false    206    3            �           0    0    tr_department_i_department_seq    SEQUENCE OWNED BY     a   ALTER SEQUENCE public.tr_department_i_department_seq OWNED BY public.tr_department.i_department;
            public       postgres    false    205            �            1259    1213576    tr_group    TABLE     G  CREATE TABLE public.tr_group (
    i_group integer NOT NULL,
    e_group_name character varying(500),
    f_active boolean DEFAULT true,
    d_entry timestamp without time zone DEFAULT now() NOT NULL,
    d_update timestamp without time zone,
    CONSTRAINT c_tr_group CHECK ((NOT (btrim((e_group_name)::text) = ''::text)))
);
    DROP TABLE public.tr_group;
       public         postgres    false    3            �            1259    1213574    tr_group_i_group_seq    SEQUENCE     �   CREATE SEQUENCE public.tr_group_i_group_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.tr_group_i_group_seq;
       public       postgres    false    3    213            �           0    0    tr_group_i_group_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE public.tr_group_i_group_seq OWNED BY public.tr_group.i_group;
            public       postgres    false    212            �            1259    1213664    tr_item    TABLE     �  CREATE TABLE public.tr_item (
    i_item integer NOT NULL,
    e_item_name character varying(500),
    v_price numeric,
    i_group integer NOT NULL,
    i_category integer NOT NULL,
    i_category_sub integer NOT NULL,
    f_active boolean DEFAULT true,
    d_entry timestamp without time zone DEFAULT now() NOT NULL,
    d_update timestamp without time zone,
    CONSTRAINT c_tr_item CHECK ((NOT (btrim((e_item_name)::text) = ''::text)))
);
    DROP TABLE public.tr_item;
       public         postgres    false    3            �            1259    1213662    tr_item_i_item_seq    SEQUENCE     �   CREATE SEQUENCE public.tr_item_i_item_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.tr_item_i_item_seq;
       public       postgres    false    219    3            �           0    0    tr_item_i_item_seq    SEQUENCE OWNED BY     I   ALTER SEQUENCE public.tr_item_i_item_seq OWNED BY public.tr_item.i_item;
            public       postgres    false    218            �            1259    1213166    tr_level    TABLE     �  CREATE TABLE public.tr_level (
    i_level integer NOT NULL,
    e_level_name character varying(500) NOT NULL,
    f_super_admin boolean DEFAULT false NOT NULL,
    f_active boolean DEFAULT true,
    e_note character varying(500) DEFAULT NULL::character varying,
    d_entry timestamp without time zone DEFAULT now() NOT NULL,
    d_update timestamp without time zone,
    CONSTRAINT c_tr_level CHECK ((NOT (btrim((e_level_name)::text) = ''::text)))
);
    DROP TABLE public.tr_level;
       public         postgres    false    3            �            1259    1213164    tr_level_i_level_seq    SEQUENCE     �   CREATE SEQUENCE public.tr_level_i_level_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.tr_level_i_level_seq;
       public       postgres    false    3    200            �           0    0    tr_level_i_level_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE public.tr_level_i_level_seq OWNED BY public.tr_level.i_level;
            public       postgres    false    199            �            1259    1213529    tr_menu    TABLE     �  CREATE TABLE public.tr_menu (
    i_menu integer NOT NULL,
    e_menu character varying(500) NOT NULL,
    i_parent smallint,
    n_urut smallint NOT NULL,
    e_folder character varying(500) NOT NULL,
    icon character varying(500) NOT NULL,
    CONSTRAINT c_tr_menu CHECK (((NOT (btrim((e_menu)::text) = ''::text)) AND (NOT (btrim((e_folder)::text) = ''::text)) AND (NOT (btrim((icon)::text) = ''::text))))
);
    DROP TABLE public.tr_menu;
       public         postgres    false    3            �            1259    1213558    tr_status_document    TABLE     �   CREATE TABLE public.tr_status_document (
    i_status integer NOT NULL,
    e_status_name character varying(500),
    f_active boolean DEFAULT true,
    CONSTRAINT c_tr_status CHECK ((NOT (btrim((e_status_name)::text) = ''::text)))
);
 &   DROP TABLE public.tr_status_document;
       public         postgres    false    3            �            1259    1213556    tr_status_document_i_status_seq    SEQUENCE     �   CREATE SEQUENCE public.tr_status_document_i_status_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 6   DROP SEQUENCE public.tr_status_document_i_status_seq;
       public       postgres    false    211    3            �           0    0    tr_status_document_i_status_seq    SEQUENCE OWNED BY     c   ALTER SEQUENCE public.tr_status_document_i_status_seq OWNED BY public.tr_status_document.i_status;
            public       postgres    false    210            �            1259    1213416    tr_user    TABLE       CREATE TABLE public.tr_user (
    i_user integer NOT NULL,
    i_user_code character varying(100),
    e_user_password character varying(500),
    e_user_name character varying(500),
    i_user_approve integer,
    f_active boolean DEFAULT true,
    d_entry timestamp without time zone DEFAULT now() NOT NULL,
    d_update timestamp without time zone,
    CONSTRAINT c_tr_user CHECK (((NOT (btrim((i_user_code)::text) = ''::text)) AND (NOT (btrim((e_user_password)::text) = ''::text)) AND (NOT (btrim((e_user_name)::text) = ''::text))))
);
    DROP TABLE public.tr_user;
       public         postgres    false    3            �            1259    1213414    tr_user_i_user_seq    SEQUENCE     �   CREATE SEQUENCE public.tr_user_i_user_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.tr_user_i_user_seq;
       public       postgres    false    3    202            �           0    0    tr_user_i_user_seq    SEQUENCE OWNED BY     I   ALTER SEQUENCE public.tr_user_i_user_seq OWNED BY public.tr_user.i_user;
            public       postgres    false    201            X           2604    1213440    tm_log i_log    DEFAULT     l   ALTER TABLE ONLY public.tm_log ALTER COLUMN i_log SET DEFAULT nextval('public.tm_log_i_log_seq'::regclass);
 ;   ALTER TABLE public.tm_log ALTER COLUMN i_log DROP DEFAULT;
       public       postgres    false    203    204    204            w           2604    1213705    tm_user_cover i_user_cover    DEFAULT     �   ALTER TABLE ONLY public.tm_user_cover ALTER COLUMN i_user_cover SET DEFAULT nextval('public.tm_user_cover_i_user_cover_seq'::regclass);
 I   ALTER TABLE public.tm_user_cover ALTER COLUMN i_user_cover DROP DEFAULT;
       public       postgres    false    221    220    221            y           2604    1214685 .   tm_user_department_role i_user_department_role    DEFAULT     �   ALTER TABLE ONLY public.tm_user_department_role ALTER COLUMN i_user_department_role SET DEFAULT nextval('public.tm_user_department_role_i_user_department_role_seq'::regclass);
 ]   ALTER TABLE public.tm_user_department_role ALTER COLUMN i_user_department_role DROP DEFAULT;
       public       postgres    false    225    224    225            x           2604    1213734    tm_user_role i_user_role    DEFAULT     �   ALTER TABLE ONLY public.tm_user_role ALTER COLUMN i_user_role SET DEFAULT nextval('public.tm_user_role_i_user_role_seq'::regclass);
 G   ALTER TABLE public.tm_user_role ALTER COLUMN i_user_role DROP DEFAULT;
       public       postgres    false    222    223    223            K           2604    1213157    tr_access i_access    DEFAULT     x   ALTER TABLE ONLY public.tr_access ALTER COLUMN i_access SET DEFAULT nextval('public.tr_access_i_access_seq'::regclass);
 A   ALTER TABLE public.tr_access ALTER COLUMN i_access DROP DEFAULT;
       public       postgres    false    198    197    198            k           2604    1213598    tr_category i_category    DEFAULT     �   ALTER TABLE ONLY public.tr_category ALTER COLUMN i_category SET DEFAULT nextval('public.tr_category_i_category_seq'::regclass);
 E   ALTER TABLE public.tr_category ALTER COLUMN i_category DROP DEFAULT;
       public       postgres    false    215    214    215            o           2604    1213642    tr_category_sub i_category_sub    DEFAULT     �   ALTER TABLE ONLY public.tr_category_sub ALTER COLUMN i_category_sub SET DEFAULT nextval('public.tr_category_sub_i_category_sub_seq'::regclass);
 M   ALTER TABLE public.tr_category_sub ALTER COLUMN i_category_sub DROP DEFAULT;
       public       postgres    false    217    216    217            _           2604    1213514    tr_company i_company    DEFAULT     |   ALTER TABLE ONLY public.tr_company ALTER COLUMN i_company SET DEFAULT nextval('public.tr_company_i_company_seq'::regclass);
 C   ALTER TABLE public.tr_company ALTER COLUMN i_company DROP DEFAULT;
       public       postgres    false    208    207    208            Z           2604    1213488    tr_department i_department    DEFAULT     �   ALTER TABLE ONLY public.tr_department ALTER COLUMN i_department SET DEFAULT nextval('public.tr_department_i_department_seq'::regclass);
 I   ALTER TABLE public.tr_department ALTER COLUMN i_department DROP DEFAULT;
       public       postgres    false    205    206    206            g           2604    1213579    tr_group i_group    DEFAULT     t   ALTER TABLE ONLY public.tr_group ALTER COLUMN i_group SET DEFAULT nextval('public.tr_group_i_group_seq'::regclass);
 ?   ALTER TABLE public.tr_group ALTER COLUMN i_group DROP DEFAULT;
       public       postgres    false    212    213    213            s           2604    1213667    tr_item i_item    DEFAULT     p   ALTER TABLE ONLY public.tr_item ALTER COLUMN i_item SET DEFAULT nextval('public.tr_item_i_item_seq'::regclass);
 =   ALTER TABLE public.tr_item ALTER COLUMN i_item DROP DEFAULT;
       public       postgres    false    219    218    219            N           2604    1213169    tr_level i_level    DEFAULT     t   ALTER TABLE ONLY public.tr_level ALTER COLUMN i_level SET DEFAULT nextval('public.tr_level_i_level_seq'::regclass);
 ?   ALTER TABLE public.tr_level ALTER COLUMN i_level DROP DEFAULT;
       public       postgres    false    200    199    200            d           2604    1213561    tr_status_document i_status    DEFAULT     �   ALTER TABLE ONLY public.tr_status_document ALTER COLUMN i_status SET DEFAULT nextval('public.tr_status_document_i_status_seq'::regclass);
 J   ALTER TABLE public.tr_status_document ALTER COLUMN i_status DROP DEFAULT;
       public       postgres    false    210    211    211            T           2604    1213419    tr_user i_user    DEFAULT     p   ALTER TABLE ONLY public.tr_user ALTER COLUMN i_user SET DEFAULT nextval('public.tr_user_i_user_seq'::regclass);
 =   ALTER TABLE public.tr_user ALTER COLUMN i_user DROP DEFAULT;
       public       postgres    false    202    201    202            v          0    1213129    ci_sessions 
   TABLE DATA               H   COPY public.ci_sessions (id, ip_address, "timestamp", data) FROM stdin;
    public       postgres    false    196            �          0    1214876    ci_sessions_3 
   TABLE DATA               J   COPY public.ci_sessions_3 (id, ip_address, "timestamp", data) FROM stdin;
    public       postgres    false    226            ~          0    1213437    tm_log 
   TABLE DATA               M   COPY public.tm_log (i_log, i_user, ip_address, "time", activity) FROM stdin;
    public       postgres    false    204            �          0    1213702    tm_user_cover 
   TABLE DATA               X   COPY public.tm_user_cover (i_user_cover, i_level, i_department, i_category) FROM stdin;
    public       postgres    false    221            �          0    1214682    tm_user_department_role 
   TABLE DATA               h   COPY public.tm_user_department_role (i_user_department_role, i_user, i_department, i_level) FROM stdin;
    public       postgres    false    225            �          0    1213731    tm_user_role 
   TABLE DATA               \   COPY public.tm_user_role (i_user_role, i_menu, i_level, i_department, i_access) FROM stdin;
    public       postgres    false    223            x          0    1213154 	   tr_access 
   TABLE DATA               <   COPY public.tr_access (i_access, e_access_name) FROM stdin;
    public       postgres    false    198            �          0    1213595    tr_category 
   TABLE DATA               _   COPY public.tr_category (i_category, e_category_name, f_active, d_entry, d_update) FROM stdin;
    public       postgres    false    215            �          0    1213639    tr_category_sub 
   TABLE DATA               w   COPY public.tr_category_sub (i_category_sub, e_category_sub_name, i_category, f_active, d_entry, d_update) FROM stdin;
    public       postgres    false    217            �          0    1213511 
   tr_company 
   TABLE DATA               \   COPY public.tr_company (i_company, e_company_name, f_active, d_entry, d_update) FROM stdin;
    public       postgres    false    208            �          0    1213485    tr_department 
   TABLE DATA               }   COPY public.tr_department (i_department, e_department_name, i_user_approve, f_skip, f_active, d_entry, d_update) FROM stdin;
    public       postgres    false    206            �          0    1213576    tr_group 
   TABLE DATA               V   COPY public.tr_group (i_group, e_group_name, f_active, d_entry, d_update) FROM stdin;
    public       postgres    false    213            �          0    1213664    tr_item 
   TABLE DATA               �   COPY public.tr_item (i_item, e_item_name, v_price, i_group, i_category, i_category_sub, f_active, d_entry, d_update) FROM stdin;
    public       postgres    false    219            z          0    1213166    tr_level 
   TABLE DATA               m   COPY public.tr_level (i_level, e_level_name, f_super_admin, f_active, e_note, d_entry, d_update) FROM stdin;
    public       postgres    false    200            �          0    1213529    tr_menu 
   TABLE DATA               S   COPY public.tr_menu (i_menu, e_menu, i_parent, n_urut, e_folder, icon) FROM stdin;
    public       postgres    false    209            �          0    1213558    tr_status_document 
   TABLE DATA               O   COPY public.tr_status_document (i_status, e_status_name, f_active) FROM stdin;
    public       postgres    false    211            |          0    1213416    tr_user 
   TABLE DATA               �   COPY public.tr_user (i_user, i_user_code, e_user_password, e_user_name, i_user_approve, f_active, d_entry, d_update) FROM stdin;
    public       postgres    false    202            �           0    0    tm_log_i_log_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('public.tm_log_i_log_seq', 157, true);
            public       postgres    false    203            �           0    0    tm_user_cover_i_user_cover_seq    SEQUENCE SET     M   SELECT pg_catalog.setval('public.tm_user_cover_i_user_cover_seq', 1, false);
            public       postgres    false    220            �           0    0 2   tm_user_department_role_i_user_department_role_seq    SEQUENCE SET     `   SELECT pg_catalog.setval('public.tm_user_department_role_i_user_department_role_seq', 5, true);
            public       postgres    false    224            �           0    0    tm_user_role_i_user_role_seq    SEQUENCE SET     K   SELECT pg_catalog.setval('public.tm_user_role_i_user_role_seq', 19, true);
            public       postgres    false    222            �           0    0    tr_access_i_access_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.tr_access_i_access_seq', 6, true);
            public       postgres    false    197            �           0    0    tr_category_i_category_seq    SEQUENCE SET     I   SELECT pg_catalog.setval('public.tr_category_i_category_seq', 1, false);
            public       postgres    false    214            �           0    0 "   tr_category_sub_i_category_sub_seq    SEQUENCE SET     Q   SELECT pg_catalog.setval('public.tr_category_sub_i_category_sub_seq', 1, false);
            public       postgres    false    216            �           0    0    tr_company_i_company_seq    SEQUENCE SET     G   SELECT pg_catalog.setval('public.tr_company_i_company_seq', 27, true);
            public       postgres    false    207            �           0    0    tr_department_i_department_seq    SEQUENCE SET     M   SELECT pg_catalog.setval('public.tr_department_i_department_seq', 22, true);
            public       postgres    false    205            �           0    0    tr_group_i_group_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.tr_group_i_group_seq', 1, false);
            public       postgres    false    212            �           0    0    tr_item_i_item_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.tr_item_i_item_seq', 1, false);
            public       postgres    false    218            �           0    0    tr_level_i_level_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.tr_level_i_level_seq', 3, true);
            public       postgres    false    199            �           0    0    tr_status_document_i_status_seq    SEQUENCE SET     N   SELECT pg_catalog.setval('public.tr_status_document_i_status_seq', 1, false);
            public       postgres    false    210            �           0    0    tr_user_i_user_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.tr_user_i_user_seq', 1, false);
            public       postgres    false    201            }           2606    1213138    ci_sessions ci_sessions_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.ci_sessions
    ADD CONSTRAINT ci_sessions_pkey PRIMARY KEY (id);
 F   ALTER TABLE ONLY public.ci_sessions DROP CONSTRAINT ci_sessions_pkey;
       public         postgres    false    196            �           2606    1213446    tm_log pk_tm_log 
   CONSTRAINT     Q   ALTER TABLE ONLY public.tm_log
    ADD CONSTRAINT pk_tm_log PRIMARY KEY (i_log);
 :   ALTER TABLE ONLY public.tm_log DROP CONSTRAINT pk_tm_log;
       public         postgres    false    204            �           2606    1213707    tm_user_cover pk_tm_user_cover 
   CONSTRAINT     f   ALTER TABLE ONLY public.tm_user_cover
    ADD CONSTRAINT pk_tm_user_cover PRIMARY KEY (i_user_cover);
 H   ALTER TABLE ONLY public.tm_user_cover DROP CONSTRAINT pk_tm_user_cover;
       public         postgres    false    221            �           2606    1214687 2   tm_user_department_role pk_tm_user_department_role 
   CONSTRAINT     �   ALTER TABLE ONLY public.tm_user_department_role
    ADD CONSTRAINT pk_tm_user_department_role PRIMARY KEY (i_user_department_role);
 \   ALTER TABLE ONLY public.tm_user_department_role DROP CONSTRAINT pk_tm_user_department_role;
       public         postgres    false    225            �           2606    1213736    tm_user_role pk_tm_user_role 
   CONSTRAINT     c   ALTER TABLE ONLY public.tm_user_role
    ADD CONSTRAINT pk_tm_user_role PRIMARY KEY (i_user_role);
 F   ALTER TABLE ONLY public.tm_user_role DROP CONSTRAINT pk_tm_user_role;
       public         postgres    false    223            �           2606    1213161    tr_access pk_tr_access 
   CONSTRAINT     Z   ALTER TABLE ONLY public.tr_access
    ADD CONSTRAINT pk_tr_access PRIMARY KEY (i_access);
 @   ALTER TABLE ONLY public.tr_access DROP CONSTRAINT pk_tr_access;
       public         postgres    false    198            �           2606    1213606    tr_category pk_tr_category 
   CONSTRAINT     `   ALTER TABLE ONLY public.tr_category
    ADD CONSTRAINT pk_tr_category PRIMARY KEY (i_category);
 D   ALTER TABLE ONLY public.tr_category DROP CONSTRAINT pk_tr_category;
       public         postgres    false    215            �           2606    1213650 "   tr_category_sub pk_tr_category_sub 
   CONSTRAINT     l   ALTER TABLE ONLY public.tr_category_sub
    ADD CONSTRAINT pk_tr_category_sub PRIMARY KEY (i_category_sub);
 L   ALTER TABLE ONLY public.tr_category_sub DROP CONSTRAINT pk_tr_category_sub;
       public         postgres    false    217            �           2606    1213522    tr_company pk_tr_company 
   CONSTRAINT     ]   ALTER TABLE ONLY public.tr_company
    ADD CONSTRAINT pk_tr_company PRIMARY KEY (i_company);
 B   ALTER TABLE ONLY public.tr_company DROP CONSTRAINT pk_tr_company;
       public         postgres    false    208            �           2606    1213497    tr_department pk_tr_department 
   CONSTRAINT     f   ALTER TABLE ONLY public.tr_department
    ADD CONSTRAINT pk_tr_department PRIMARY KEY (i_department);
 H   ALTER TABLE ONLY public.tr_department DROP CONSTRAINT pk_tr_department;
       public         postgres    false    206            �           2606    1213587    tr_group pk_tr_group 
   CONSTRAINT     W   ALTER TABLE ONLY public.tr_group
    ADD CONSTRAINT pk_tr_group PRIMARY KEY (i_group);
 >   ALTER TABLE ONLY public.tr_group DROP CONSTRAINT pk_tr_group;
       public         postgres    false    213            �           2606    1213675    tr_item pk_tr_item 
   CONSTRAINT     T   ALTER TABLE ONLY public.tr_item
    ADD CONSTRAINT pk_tr_item PRIMARY KEY (i_item);
 <   ALTER TABLE ONLY public.tr_item DROP CONSTRAINT pk_tr_item;
       public         postgres    false    219            �           2606    1213179    tr_level pk_tr_level 
   CONSTRAINT     W   ALTER TABLE ONLY public.tr_level
    ADD CONSTRAINT pk_tr_level PRIMARY KEY (i_level);
 >   ALTER TABLE ONLY public.tr_level DROP CONSTRAINT pk_tr_level;
       public         postgres    false    200            �           2606    1213537    tr_menu pk_tr_menu 
   CONSTRAINT     T   ALTER TABLE ONLY public.tr_menu
    ADD CONSTRAINT pk_tr_menu PRIMARY KEY (i_menu);
 <   ALTER TABLE ONLY public.tr_menu DROP CONSTRAINT pk_tr_menu;
       public         postgres    false    209            �           2606    1213568    tr_status_document pk_tr_status 
   CONSTRAINT     c   ALTER TABLE ONLY public.tr_status_document
    ADD CONSTRAINT pk_tr_status PRIMARY KEY (i_status);
 I   ALTER TABLE ONLY public.tr_status_document DROP CONSTRAINT pk_tr_status;
       public         postgres    false    211            �           2606    1213427    tr_user pk_tr_user 
   CONSTRAINT     T   ALTER TABLE ONLY public.tr_user
    ADD CONSTRAINT pk_tr_user PRIMARY KEY (i_user);
 <   ALTER TABLE ONLY public.tr_user DROP CONSTRAINT pk_tr_user;
       public         postgres    false    202            �           2606    1213709    tm_user_cover u_tm_user_cover 
   CONSTRAINT     �   ALTER TABLE ONLY public.tm_user_cover
    ADD CONSTRAINT u_tm_user_cover UNIQUE (i_user_cover, i_level, i_department, i_category);
 G   ALTER TABLE ONLY public.tm_user_cover DROP CONSTRAINT u_tm_user_cover;
       public         postgres    false    221    221    221    221            �           2606    1213738    tm_user_role u_tm_user_role 
   CONSTRAINT     �   ALTER TABLE ONLY public.tm_user_role
    ADD CONSTRAINT u_tm_user_role UNIQUE (i_user_role, i_menu, i_level, i_department, i_access);
 E   ALTER TABLE ONLY public.tm_user_role DROP CONSTRAINT u_tm_user_role;
       public         postgres    false    223    223    223    223    223            �           2606    1213608    tr_category u_tr_category 
   CONSTRAINT     _   ALTER TABLE ONLY public.tr_category
    ADD CONSTRAINT u_tr_category UNIQUE (e_category_name);
 C   ALTER TABLE ONLY public.tr_category DROP CONSTRAINT u_tr_category;
       public         postgres    false    215            �           2606    1213652 !   tr_category_sub u_tr_category_sub 
   CONSTRAINT     k   ALTER TABLE ONLY public.tr_category_sub
    ADD CONSTRAINT u_tr_category_sub UNIQUE (e_category_sub_name);
 K   ALTER TABLE ONLY public.tr_category_sub DROP CONSTRAINT u_tr_category_sub;
       public         postgres    false    217            �           2606    1213524    tr_company u_tr_company 
   CONSTRAINT     \   ALTER TABLE ONLY public.tr_company
    ADD CONSTRAINT u_tr_company UNIQUE (e_company_name);
 A   ALTER TABLE ONLY public.tr_company DROP CONSTRAINT u_tr_company;
       public         postgres    false    208            �           2606    1213499    tr_department u_tr_department 
   CONSTRAINT     e   ALTER TABLE ONLY public.tr_department
    ADD CONSTRAINT u_tr_department UNIQUE (e_department_name);
 G   ALTER TABLE ONLY public.tr_department DROP CONSTRAINT u_tr_department;
       public         postgres    false    206            �           2606    1213589    tr_group u_tr_group 
   CONSTRAINT     V   ALTER TABLE ONLY public.tr_group
    ADD CONSTRAINT u_tr_group UNIQUE (e_group_name);
 =   ALTER TABLE ONLY public.tr_group DROP CONSTRAINT u_tr_group;
       public         postgres    false    213            �           2606    1213677    tr_item u_tr_item 
   CONSTRAINT     S   ALTER TABLE ONLY public.tr_item
    ADD CONSTRAINT u_tr_item UNIQUE (e_item_name);
 ;   ALTER TABLE ONLY public.tr_item DROP CONSTRAINT u_tr_item;
       public         postgres    false    219            �           2606    1213181    tr_level u_tr_level 
   CONSTRAINT     V   ALTER TABLE ONLY public.tr_level
    ADD CONSTRAINT u_tr_level UNIQUE (e_level_name);
 =   ALTER TABLE ONLY public.tr_level DROP CONSTRAINT u_tr_level;
       public         postgres    false    200            �           2606    1213570    tr_status_document u_tr_status 
   CONSTRAINT     b   ALTER TABLE ONLY public.tr_status_document
    ADD CONSTRAINT u_tr_status UNIQUE (e_status_name);
 H   ALTER TABLE ONLY public.tr_status_document DROP CONSTRAINT u_tr_status;
       public         postgres    false    211            �           2606    1213429    tr_user u_tr_user 
   CONSTRAINT     S   ALTER TABLE ONLY public.tr_user
    ADD CONSTRAINT u_tr_user UNIQUE (i_user_code);
 ;   ALTER TABLE ONLY public.tr_user DROP CONSTRAINT u_tr_user;
       public         postgres    false    202            ~           1259    1213139    ci_sessions_timestamp    INDEX     T   CREATE INDEX ci_sessions_timestamp ON public.ci_sessions USING btree ("timestamp");
 )   DROP INDEX public.ci_sessions_timestamp;
       public         postgres    false    196            �           1259    1213452    idx_tm_log_activity    INDEX     J   CREATE INDEX idx_tm_log_activity ON public.tm_log USING btree (activity);
 '   DROP INDEX public.idx_tm_log_activity;
       public         postgres    false    204            �           1259    1213453    idx_tm_log_address    INDEX     K   CREATE INDEX idx_tm_log_address ON public.tm_log USING btree (ip_address);
 &   DROP INDEX public.idx_tm_log_address;
       public         postgres    false    204            �           1259    1213454    idx_tm_log_id    INDEX     A   CREATE INDEX idx_tm_log_id ON public.tm_log USING btree (i_log);
 !   DROP INDEX public.idx_tm_log_id;
       public         postgres    false    204            �           1259    1213455    idx_tm_log_time    INDEX     D   CREATE INDEX idx_tm_log_time ON public.tm_log USING btree ("time");
 #   DROP INDEX public.idx_tm_log_time;
       public         postgres    false    204            �           1259    1213456    idx_tm_log_user    INDEX     D   CREATE INDEX idx_tm_log_user ON public.tm_log USING btree (i_user);
 #   DROP INDEX public.idx_tm_log_user;
       public         postgres    false    204            �           1259    1213728    idx_tm_user_cover_category    INDEX     Z   CREATE INDEX idx_tm_user_cover_category ON public.tm_user_cover USING btree (i_category);
 .   DROP INDEX public.idx_tm_user_cover_category;
       public         postgres    false    221            �           1259    1213727    idx_tm_user_cover_department    INDEX     ^   CREATE INDEX idx_tm_user_cover_department ON public.tm_user_cover USING btree (i_department);
 0   DROP INDEX public.idx_tm_user_cover_department;
       public         postgres    false    221            �           1259    1213725    idx_tm_user_cover_id    INDEX     V   CREATE INDEX idx_tm_user_cover_id ON public.tm_user_cover USING btree (i_user_cover);
 (   DROP INDEX public.idx_tm_user_cover_id;
       public         postgres    false    221            �           1259    1213726    idx_tm_user_cover_level    INDEX     T   CREATE INDEX idx_tm_user_cover_level ON public.tm_user_cover USING btree (i_level);
 +   DROP INDEX public.idx_tm_user_cover_level;
       public         postgres    false    221            �           1259    1214703    idx_tm_user_department_role_id    INDEX     t   CREATE INDEX idx_tm_user_department_role_id ON public.tm_user_department_role USING btree (i_user_department_role);
 2   DROP INDEX public.idx_tm_user_department_role_id;
       public         postgres    false    225            �           1259    1214704     idx_tm_user_department_role_user    INDEX     f   CREATE INDEX idx_tm_user_department_role_user ON public.tm_user_department_role USING btree (i_user);
 4   DROP INDEX public.idx_tm_user_department_role_user;
       public         postgres    false    225            �           1259    1214705 +   idx_tm_user_department_role_user_department    INDEX     w   CREATE INDEX idx_tm_user_department_role_user_department ON public.tm_user_department_role USING btree (i_department);
 ?   DROP INDEX public.idx_tm_user_department_role_user_department;
       public         postgres    false    225            �           1259    1214706 &   idx_tm_user_department_role_user_level    INDEX     m   CREATE INDEX idx_tm_user_department_role_user_level ON public.tm_user_department_role USING btree (i_level);
 :   DROP INDEX public.idx_tm_user_department_role_user_level;
       public         postgres    false    225            �           1259    1213763    idx_tm_user_role_access    INDEX     T   CREATE INDEX idx_tm_user_role_access ON public.tm_user_role USING btree (i_access);
 +   DROP INDEX public.idx_tm_user_role_access;
       public         postgres    false    223            �           1259    1213762    idx_tm_user_role_department    INDEX     \   CREATE INDEX idx_tm_user_role_department ON public.tm_user_role USING btree (i_department);
 /   DROP INDEX public.idx_tm_user_role_department;
       public         postgres    false    223            �           1259    1213759    idx_tm_user_role_id    INDEX     S   CREATE INDEX idx_tm_user_role_id ON public.tm_user_role USING btree (i_user_role);
 '   DROP INDEX public.idx_tm_user_role_id;
       public         postgres    false    223            �           1259    1213761    idx_tm_user_role_level    INDEX     R   CREATE INDEX idx_tm_user_role_level ON public.tm_user_role USING btree (i_level);
 *   DROP INDEX public.idx_tm_user_role_level;
       public         postgres    false    223            �           1259    1213760    idx_tm_user_role_menu    INDEX     P   CREATE INDEX idx_tm_user_role_menu ON public.tm_user_role USING btree (i_menu);
 )   DROP INDEX public.idx_tm_user_role_menu;
       public         postgres    false    223                       1259    1213162    idx_tr_access_id    INDEX     J   CREATE INDEX idx_tr_access_id ON public.tr_access USING btree (i_access);
 $   DROP INDEX public.idx_tr_access_id;
       public         postgres    false    198            �           1259    1213163    idx_tr_access_name    INDEX     Q   CREATE INDEX idx_tr_access_name ON public.tr_access USING btree (e_access_name);
 &   DROP INDEX public.idx_tr_access_name;
       public         postgres    false    198            �           1259    1213609    idx_tr_category_id    INDEX     P   CREATE INDEX idx_tr_category_id ON public.tr_category USING btree (i_category);
 &   DROP INDEX public.idx_tr_category_id;
       public         postgres    false    215            �           1259    1213610    idx_tr_category_name    INDEX     W   CREATE INDEX idx_tr_category_name ON public.tr_category USING btree (e_category_name);
 (   DROP INDEX public.idx_tr_category_name;
       public         postgres    false    215            �           1259    1213611    idx_tr_category_status    INDEX     R   CREATE INDEX idx_tr_category_status ON public.tr_category USING btree (f_active);
 *   DROP INDEX public.idx_tr_category_status;
       public         postgres    false    215            �           1259    1213660    idx_tr_category_sub_category    INDEX     ^   CREATE INDEX idx_tr_category_sub_category ON public.tr_category_sub USING btree (i_category);
 0   DROP INDEX public.idx_tr_category_sub_category;
       public         postgres    false    217            �           1259    1213658    idx_tr_category_sub_id    INDEX     \   CREATE INDEX idx_tr_category_sub_id ON public.tr_category_sub USING btree (i_category_sub);
 *   DROP INDEX public.idx_tr_category_sub_id;
       public         postgres    false    217            �           1259    1213659    idx_tr_category_sub_name    INDEX     c   CREATE INDEX idx_tr_category_sub_name ON public.tr_category_sub USING btree (e_category_sub_name);
 ,   DROP INDEX public.idx_tr_category_sub_name;
       public         postgres    false    217            �           1259    1213661    idx_tr_category_sub_status    INDEX     Z   CREATE INDEX idx_tr_category_sub_status ON public.tr_category_sub USING btree (f_active);
 .   DROP INDEX public.idx_tr_category_sub_status;
       public         postgres    false    217            �           1259    1213525    idx_tr_company_id    INDEX     M   CREATE INDEX idx_tr_company_id ON public.tr_company USING btree (i_company);
 %   DROP INDEX public.idx_tr_company_id;
       public         postgres    false    208            �           1259    1213526    idx_tr_company_name    INDEX     T   CREATE INDEX idx_tr_company_name ON public.tr_company USING btree (e_company_name);
 '   DROP INDEX public.idx_tr_company_name;
       public         postgres    false    208            �           1259    1213527    idx_tr_company_status    INDEX     P   CREATE INDEX idx_tr_company_status ON public.tr_company USING btree (f_active);
 )   DROP INDEX public.idx_tr_company_status;
       public         postgres    false    208            �           1259    1213507    idx_tr_department_appprove    INDEX     ^   CREATE INDEX idx_tr_department_appprove ON public.tr_department USING btree (i_user_approve);
 .   DROP INDEX public.idx_tr_department_appprove;
       public         postgres    false    206            �           1259    1213505    idx_tr_department_id    INDEX     V   CREATE INDEX idx_tr_department_id ON public.tr_department USING btree (i_department);
 (   DROP INDEX public.idx_tr_department_id;
       public         postgres    false    206            �           1259    1213506    idx_tr_department_name    INDEX     ]   CREATE INDEX idx_tr_department_name ON public.tr_department USING btree (e_department_name);
 *   DROP INDEX public.idx_tr_department_name;
       public         postgres    false    206            �           1259    1213508    idx_tr_department_status    INDEX     V   CREATE INDEX idx_tr_department_status ON public.tr_department USING btree (f_active);
 ,   DROP INDEX public.idx_tr_department_status;
       public         postgres    false    206            �           1259    1213590    idx_tr_group_id    INDEX     G   CREATE INDEX idx_tr_group_id ON public.tr_group USING btree (i_group);
 #   DROP INDEX public.idx_tr_group_id;
       public         postgres    false    213            �           1259    1213591    idx_tr_group_name    INDEX     N   CREATE INDEX idx_tr_group_name ON public.tr_group USING btree (e_group_name);
 %   DROP INDEX public.idx_tr_group_name;
       public         postgres    false    213            �           1259    1213592    idx_tr_group_status    INDEX     L   CREATE INDEX idx_tr_group_status ON public.tr_group USING btree (f_active);
 '   DROP INDEX public.idx_tr_group_status;
       public         postgres    false    213            �           1259    1213696    idx_tr_item_category    INDEX     N   CREATE INDEX idx_tr_item_category ON public.tr_item USING btree (i_category);
 (   DROP INDEX public.idx_tr_item_category;
       public         postgres    false    219            �           1259    1213697    idx_tr_item_category_sub    INDEX     V   CREATE INDEX idx_tr_item_category_sub ON public.tr_item USING btree (i_category_sub);
 ,   DROP INDEX public.idx_tr_item_category_sub;
       public         postgres    false    219            �           1259    1213695    idx_tr_item_group    INDEX     H   CREATE INDEX idx_tr_item_group ON public.tr_item USING btree (i_group);
 %   DROP INDEX public.idx_tr_item_group;
       public         postgres    false    219            �           1259    1213693    idx_tr_item_id    INDEX     D   CREATE INDEX idx_tr_item_id ON public.tr_item USING btree (i_item);
 "   DROP INDEX public.idx_tr_item_id;
       public         postgres    false    219            �           1259    1213694    idx_tr_item_name    INDEX     K   CREATE INDEX idx_tr_item_name ON public.tr_item USING btree (e_item_name);
 $   DROP INDEX public.idx_tr_item_name;
       public         postgres    false    219            �           1259    1213698    idx_tr_item_status    INDEX     J   CREATE INDEX idx_tr_item_status ON public.tr_item USING btree (f_active);
 &   DROP INDEX public.idx_tr_item_status;
       public         postgres    false    219            �           1259    1213182    idx_tr_level    INDEX     D   CREATE INDEX idx_tr_level ON public.tr_level USING btree (i_level);
     DROP INDEX public.idx_tr_level;
       public         postgres    false    200            �           1259    1213183    idx_tr_level_name    INDEX     N   CREATE INDEX idx_tr_level_name ON public.tr_level USING btree (e_level_name);
 %   DROP INDEX public.idx_tr_level_name;
       public         postgres    false    200            �           1259    1213184    idx_tr_level_status    INDEX     L   CREATE INDEX idx_tr_level_status ON public.tr_level USING btree (f_active);
 '   DROP INDEX public.idx_tr_level_status;
       public         postgres    false    200            �           1259    1213185    idx_tr_level_super_admin    INDEX     V   CREATE INDEX idx_tr_level_super_admin ON public.tr_level USING btree (f_super_admin);
 ,   DROP INDEX public.idx_tr_level_super_admin;
       public         postgres    false    200            �           1259    1213538    idx_tr_menu    INDEX     A   CREATE INDEX idx_tr_menu ON public.tr_menu USING btree (i_menu);
    DROP INDEX public.idx_tr_menu;
       public         postgres    false    209            �           1259    1213539    idx_tr_menu_folder    INDEX     J   CREATE INDEX idx_tr_menu_folder ON public.tr_menu USING btree (e_folder);
 &   DROP INDEX public.idx_tr_menu_folder;
       public         postgres    false    209            �           1259    1213540    idx_tr_menu_parent    INDEX     J   CREATE INDEX idx_tr_menu_parent ON public.tr_menu USING btree (i_parent);
 &   DROP INDEX public.idx_tr_menu_parent;
       public         postgres    false    209            �           1259    1213571    idx_tr_status_id    INDEX     S   CREATE INDEX idx_tr_status_id ON public.tr_status_document USING btree (i_status);
 $   DROP INDEX public.idx_tr_status_id;
       public         postgres    false    211            �           1259    1213572    idx_tr_status_name    INDEX     Z   CREATE INDEX idx_tr_status_name ON public.tr_status_document USING btree (e_status_name);
 &   DROP INDEX public.idx_tr_status_name;
       public         postgres    false    211            �           1259    1213573    idx_tr_status_status    INDEX     W   CREATE INDEX idx_tr_status_status ON public.tr_status_document USING btree (f_active);
 (   DROP INDEX public.idx_tr_status_status;
       public         postgres    false    211            �           1259    1213433    idx_tr_user_appprove    INDEX     R   CREATE INDEX idx_tr_user_appprove ON public.tr_user USING btree (i_user_approve);
 (   DROP INDEX public.idx_tr_user_appprove;
       public         postgres    false    202            �           1259    1213430    idx_tr_user_code    INDEX     K   CREATE INDEX idx_tr_user_code ON public.tr_user USING btree (i_user_code);
 $   DROP INDEX public.idx_tr_user_code;
       public         postgres    false    202            �           1259    1213431    idx_tr_user_id    INDEX     D   CREATE INDEX idx_tr_user_id ON public.tr_user USING btree (i_user);
 "   DROP INDEX public.idx_tr_user_id;
       public         postgres    false    202            �           1259    1213432    idx_tr_user_name    INDEX     K   CREATE INDEX idx_tr_user_name ON public.tr_user USING btree (e_user_name);
 $   DROP INDEX public.idx_tr_user_name;
       public         postgres    false    202            �           1259    1213434    idx_tr_user_status    INDEX     J   CREATE INDEX idx_tr_user_status ON public.tr_user USING btree (f_active);
 &   DROP INDEX public.idx_tr_user_status;
       public         postgres    false    202            �           2606    1213447    tm_log fk_tm_log    FK CONSTRAINT     �   ALTER TABLE ONLY public.tm_log
    ADD CONSTRAINT fk_tm_log FOREIGN KEY (i_user) REFERENCES public.tr_user(i_user) ON UPDATE CASCADE ON DELETE RESTRICT;
 :   ALTER TABLE ONLY public.tm_log DROP CONSTRAINT fk_tm_log;
       public       postgres    false    204    2961    202            �           2606    1213720 '   tm_user_cover fk_tm_user_cover_category    FK CONSTRAINT     �   ALTER TABLE ONLY public.tm_user_cover
    ADD CONSTRAINT fk_tm_user_cover_category FOREIGN KEY (i_category) REFERENCES public.tr_category(i_category) ON UPDATE CASCADE ON DELETE RESTRICT;
 Q   ALTER TABLE ONLY public.tm_user_cover DROP CONSTRAINT fk_tm_user_cover_category;
       public       postgres    false    215    221    3009            �           2606    1213715 )   tm_user_cover fk_tm_user_cover_department    FK CONSTRAINT     �   ALTER TABLE ONLY public.tm_user_cover
    ADD CONSTRAINT fk_tm_user_cover_department FOREIGN KEY (i_department) REFERENCES public.tr_department(i_department) ON UPDATE CASCADE ON DELETE RESTRICT;
 S   ALTER TABLE ONLY public.tm_user_cover DROP CONSTRAINT fk_tm_user_cover_department;
       public       postgres    false    221    206    2976            �           2606    1213710 $   tm_user_cover fk_tm_user_cover_level    FK CONSTRAINT     �   ALTER TABLE ONLY public.tm_user_cover
    ADD CONSTRAINT fk_tm_user_cover_level FOREIGN KEY (i_level) REFERENCES public.tr_level(i_level) ON UPDATE CASCADE ON DELETE RESTRICT;
 N   ALTER TABLE ONLY public.tm_user_cover DROP CONSTRAINT fk_tm_user_cover_level;
       public       postgres    false    200    2952    221            �           2606    1214693 =   tm_user_department_role fk_tm_user_department_role_department    FK CONSTRAINT     �   ALTER TABLE ONLY public.tm_user_department_role
    ADD CONSTRAINT fk_tm_user_department_role_department FOREIGN KEY (i_department) REFERENCES public.tr_department(i_department) ON UPDATE CASCADE ON DELETE RESTRICT;
 g   ALTER TABLE ONLY public.tm_user_department_role DROP CONSTRAINT fk_tm_user_department_role_department;
       public       postgres    false    206    2976    225            �           2606    1214698 8   tm_user_department_role fk_tm_user_department_role_level    FK CONSTRAINT     �   ALTER TABLE ONLY public.tm_user_department_role
    ADD CONSTRAINT fk_tm_user_department_role_level FOREIGN KEY (i_level) REFERENCES public.tr_level(i_level) ON UPDATE CASCADE ON DELETE RESTRICT;
 b   ALTER TABLE ONLY public.tm_user_department_role DROP CONSTRAINT fk_tm_user_department_role_level;
       public       postgres    false    2952    225    200            �           2606    1214688 7   tm_user_department_role fk_tm_user_department_role_user    FK CONSTRAINT     �   ALTER TABLE ONLY public.tm_user_department_role
    ADD CONSTRAINT fk_tm_user_department_role_user FOREIGN KEY (i_user) REFERENCES public.tr_user(i_user) ON UPDATE CASCADE ON DELETE RESTRICT;
 a   ALTER TABLE ONLY public.tm_user_department_role DROP CONSTRAINT fk_tm_user_department_role_user;
       public       postgres    false    225    202    2961            �           2606    1213754 #   tm_user_role fk_tm_user_role_access    FK CONSTRAINT     �   ALTER TABLE ONLY public.tm_user_role
    ADD CONSTRAINT fk_tm_user_role_access FOREIGN KEY (i_access) REFERENCES public.tr_access(i_access) ON UPDATE CASCADE ON DELETE RESTRICT;
 M   ALTER TABLE ONLY public.tm_user_role DROP CONSTRAINT fk_tm_user_role_access;
       public       postgres    false    2946    223    198            �           2606    1213749 '   tm_user_role fk_tm_user_role_department    FK CONSTRAINT     �   ALTER TABLE ONLY public.tm_user_role
    ADD CONSTRAINT fk_tm_user_role_department FOREIGN KEY (i_department) REFERENCES public.tr_department(i_department) ON UPDATE CASCADE ON DELETE RESTRICT;
 Q   ALTER TABLE ONLY public.tm_user_role DROP CONSTRAINT fk_tm_user_role_department;
       public       postgres    false    206    2976    223            �           2606    1213744 "   tm_user_role fk_tm_user_role_level    FK CONSTRAINT     �   ALTER TABLE ONLY public.tm_user_role
    ADD CONSTRAINT fk_tm_user_role_level FOREIGN KEY (i_level) REFERENCES public.tr_level(i_level) ON UPDATE CASCADE ON DELETE RESTRICT;
 L   ALTER TABLE ONLY public.tm_user_role DROP CONSTRAINT fk_tm_user_role_level;
       public       postgres    false    200    2952    223            �           2606    1213739 !   tm_user_role fk_tm_user_role_menu    FK CONSTRAINT     �   ALTER TABLE ONLY public.tm_user_role
    ADD CONSTRAINT fk_tm_user_role_menu FOREIGN KEY (i_menu) REFERENCES public.tr_menu(i_menu) ON UPDATE CASCADE ON DELETE RESTRICT;
 K   ALTER TABLE ONLY public.tm_user_role DROP CONSTRAINT fk_tm_user_role_menu;
       public       postgres    false    209    2990    223            �           2606    1213653 "   tr_category_sub fk_tr_category_sub    FK CONSTRAINT     �   ALTER TABLE ONLY public.tr_category_sub
    ADD CONSTRAINT fk_tr_category_sub FOREIGN KEY (i_category_sub) REFERENCES public.tr_category(i_category) ON UPDATE CASCADE ON DELETE RESTRICT;
 L   ALTER TABLE ONLY public.tr_category_sub DROP CONSTRAINT fk_tr_category_sub;
       public       postgres    false    3009    217    215            �           2606    1213500    tr_department fk_tr_department    FK CONSTRAINT     �   ALTER TABLE ONLY public.tr_department
    ADD CONSTRAINT fk_tr_department FOREIGN KEY (i_user_approve) REFERENCES public.tr_user(i_user) ON UPDATE CASCADE ON DELETE RESTRICT;
 H   ALTER TABLE ONLY public.tr_department DROP CONSTRAINT fk_tr_department;
       public       postgres    false    206    202    2961            �           2606    1213683    tr_item fk_tr_item_category    FK CONSTRAINT     �   ALTER TABLE ONLY public.tr_item
    ADD CONSTRAINT fk_tr_item_category FOREIGN KEY (i_category) REFERENCES public.tr_category(i_category) ON UPDATE CASCADE ON DELETE RESTRICT;
 E   ALTER TABLE ONLY public.tr_item DROP CONSTRAINT fk_tr_item_category;
       public       postgres    false    215    219    3009            �           2606    1213688    tr_item fk_tr_item_category_sub    FK CONSTRAINT     �   ALTER TABLE ONLY public.tr_item
    ADD CONSTRAINT fk_tr_item_category_sub FOREIGN KEY (i_category_sub) REFERENCES public.tr_category_sub(i_category_sub) ON UPDATE CASCADE ON DELETE RESTRICT;
 I   ALTER TABLE ONLY public.tr_item DROP CONSTRAINT fk_tr_item_category_sub;
       public       postgres    false    217    219    3017            �           2606    1213678    tr_item fk_tr_item_group    FK CONSTRAINT     �   ALTER TABLE ONLY public.tr_item
    ADD CONSTRAINT fk_tr_item_group FOREIGN KEY (i_group) REFERENCES public.tr_group(i_group) ON UPDATE CASCADE ON DELETE RESTRICT;
 B   ALTER TABLE ONLY public.tr_item DROP CONSTRAINT fk_tr_item_group;
       public       postgres    false    213    3002    219            v   

  x��\ٖ�8}��"�s�C����_$������(�*��&q�\U�Dql�,. x h�N�\��йf�Y4p�إ�^GiJ�����/D}񙲐��@ɉ�Q����"�d��g"�b��T�R��I�S��H�"F�d�i�0��	��ZP��TR�*��� �(�u9֎ �V_�N���kS�j���C�A	�=سA*��WO���A�;�|	��KӔO��e��ϙ�]S����%�Kv��D�B�N��p��_Y�k�cQ�c��J|V�!qs�M�JM���%;�gbB!��Q�Hs��1��:�~rmODWf�����e��B/����厰0R(e?�2���~v�ǳk2��ˮo%�[�&�EM�ԏ�F�F�*���q�h��R�������Ey�O�sy������4~,7�!G�|����2���\y9K���w~��	�q�Z6�ĕ"�i<����Ԧ��7�тeR��^��g�^����c�r���x���;ΣG����h��C\��$i��8�InI�NY1ʒ�n̋��}���3��!WB�P~��)6yVwC��X�	0��C[��A4�bm��د��ie'14�7Ӭ���c��P�M��)m�����㲦Ju=�,��dv3������D��g̳i�B]ֽ�jd������R�}O6���@��t�Yy��^��x��F��/ ����]Ĳb�������X���4ABNovh��['�>hW>ë`F� �W�
��K���9�Đ�I�p$� �2�'qid��Z9�{��
�
 ���G����\��vU_��8m�.1��(�tQjr��5����4��Ҁ��:���1�ē2c1��Z�˄��܎�P8ξ[D��^�� ��n��������x˨�vd�>�t�u�\-b Օv���.Ε�ǝno3�WpC\���Յ;]�s��.7����uپ�Y�ָܵ�k�q�*�R�>oIZg;�E;��q��Mg��� z�g����55�(l��#�l���]��S��S 2`RJr�)  ��>��TLK+�bm֐���Q2g�p�/�����4�À�J������˶~�B�n��E�ߴHuf�Ve�&n�*�6���ԃLL�� ��&�A�l�d����ɇ�'m�%�����(Ӓ�u/�Z�Y�Ɏ~�
P� y�!Ͷ�׸�~:�S|Iڙ���4��e�t�f(r�LedG?y�B������z������a��)�y����jl����G]Ӹ�+�MZ��'�B��g�`t�k&�o�8}�S�k���eQ��r�,eg�9��$�݁(g>FjC�`��CTl���K�	��7������}�)DO���͊��p�BR!C�����
��ԭDmbV��7t }550�uZ#��>#<�{�!��#U�!����	�At�I��u-LjmQȲ�R4���i2��}����< BSsx��l;���d'Y�)Y�\H-H-]��J�M3���{��\��� G���h�*�取�gv��'����G+)Wi�J˚��ꡭX���*�\VD
��F*������]�|���i�x��]y@p���k��"H$A6C�#m���ǉ��l̶:m˼(V�FU9kI{5�lF�ɞ�l`!�� b${ʛ7b�����=ϕ� �.��W�͏Bs���s��ŷ��O.&[׾]a�Mh#]>[�Y�[�L6�DTꀒ��~F-�'H�2��J�[4ϯ�U�)�����ඖ�v%�	~����x�����~G��a�n�>��I_ɚq��L;�y]�Ml�gV1h�K'���H"��y�>���D�_��ͤ��Yq5���	�gb��)#IWmJ���J����Z�C9O��۩���؏��k�VҬ��z�Ӌ�Ve[6�,�vF�Dq��@�j���!q|���i���)51��!�d?Y���d#'���ڽ�*d"0���=���,�o�_s�� ~���h7�;�yC�׃��%��j��h�GbJt�e�ZwK4�&c��S��3%!z
p�=�-��ޫ[��5.�|0h�̵���1\E��Xe~qczmS�9ӹ%�:[�5V޴�3��A�4v�GlMHt@�3s�����Q�`���~k�	�<O��]�L@Vu�M'�u#�9�Qظb.�n]m��S���@��@��W�Bn?Dr���Ow��f��x����#��Ń�W��;+ƤLZ�a�9ڨoڦ܁'�!!���x�qx���~���Oa�XQ�ue�#�hY,t2y�`S�ځ�?�b�H��{��l��	Ϸ<1��b-0���_�nj�5.���$�+��CJ ]�Gl����8�o����4D�/r��Fwq�;�Ĕ��qK9�"���~1{�Ԡ��jy ��:�n��N�R�6��cڌnv�eC9tu���y��]pX����A���38|�����Ŏ�ׇ��J�3-	wj��Xv��^A��Z[�9Wr���-K*�Ii6�a/@��#�=d��i��d)���첸��s�JW��rҿ�+�v
W��Rr�s����_	�߄x�⿂���?���      �   �  x�ՒMo1���c����d�vQ��^�ٲ,	���:IOͅKՖ/�?�;�:A�Ay�E8(i�A���Z�G���FLF�y!F-ӽi��kkhi1l�9o��O?o�Y?]꾻T�����8D�փ{��e?e?�.��v?;mڷ���.�n��`�C�w�:���O����uc�z���^����>�\��TC׮��?ڏ߶���d�(2 Zǉ��F/��QZy`LZ`����>/G������%���u� �yt:�E�����B^�������-^�o�q��j���g�{)��.Y�,�����l��}h^�]%����(���<U���w?��Q��\s�*uT��ɘ�����y���0J��c�:���M���	�tO�2y���y���8!T,��B�u� �?��~3�K�vN      ~   �  x��Y�j�8}n���-F��%�m���\�I2��/���6L�C�3���J�m��"9G*��Je���i`��WL_qU�h8k�������p; �r@H1��(�T��Jo����o]����iO�ơ�a<�
�d�$T�P`8�6�l�ߺC����Z~z~��� 
-u����M���^ ��m��=u��9�B�S�b9�����pل��|�N@�������� \H�qa�{�k��	WӁ%���z��M����c⸺���p\]��W��d��Ճ9����8�m�]yݞ�\޵�]ٔ�������ށ�DG��tt�ܺadt���'��$0Br�����^.�	Bs�Q���\7��`\q��Cz���RK�-.0�e��
Lh���`z�F��y�~����]�{�P�`R�Ajd�_�
�	�Sȼ ���m�4�ro�Df6�zO�D\�G�>[��^wo�����>���C!�G�������?�=��~/d"�D�+S&��,�-����w�������������B&��>����N���=V̰Ȣ�'���JȊ{2a�9�q��<(nϙ�>���^^�����Ԟ��B�>�T�P�$�p�8��哫½�	�����*zCp_(��^�
w�C�w~H�����1�ЌQV�~pf��e�eI7g`6p�9*:5�{�B����6�bB��z�G���3D2:�k����Km�sy��BK�����>?=�K�Ќ�h�9(��D�p�"�YIA�H�bZ+������B��[O�$��XB�@�#"����'PL,50C��ao K;Aq�����{a�LJ4��U��5��z��/jK�!�gkaǢN,ep�υ�eHr6Uz˘X�K�k<�fG �W�i�S��ޭ�b��eL��b��L��b«�X�]�3DO%
B�6�"���%n�\�L����Rt��E<i-@�"�e3�����ƅ	7�g��}�`�KdM�������A`�O��]�X��Sd��rS�~q�$�R���h2-B�)�@ɴ/���N��d��J�H2Ѳ;�M&.��K�1±�$����+��Lh��H���H�G��~����܃�[%����Y2t,1���m.�k౞��������V�<౤Z�* <qy�^汾X$Og౺�t\B������!�зRPi%k��G�^!H�۬��A\�0k�������ZO�F�=�
4�2���q. ������V1M�tA�Ҳ��0������1�TYQ[wD�)�:A��Y!�l^[����T�Hn��4�[��6'PY8�_8�o�!-�"�^^n�E�?ơ���!I.t��D,���^�I5�|E:�֮��̎@�NA��$̎���A��Vϳ���	2o��k�$B�v�,�EG�y�:2q�p�J��*�~������,�6ǳ�T�k��C�i���'������]T�K��*�"UO�7�k�zH��	�[��
m�1��]E�?	X~�      �      x������ � �      �   )   x�3�4�44�4�2�0���!c. �(`
���b���� ���      �   `   x�E�Q
�0�o=�0�]�����Xʨ�|���0nKϽ�k3D�y0�v���O�:��Σ��T�9��TW�V�w���Sbn�y�����@�"�      x   =   x�3�t.JM,I�2�JML�2�-H�M8]RsR�SNǂ����T.3΀�̼�=... ���      �      x������ � �      �      x������ � �      �   �  x����n�@�y�y�Dx08ɮ�64
��d�#F�{,ͥU޾��)]Tb��o�\'�V?���V�(8���8�B��Z�L�=W��(��vV���D{$4]��F�[G-��Y�b�ڙ@Pk�����ma�i��R [��f.�	�Ź��'B[\���%zX.D����˞a��(ߝ������8OcÇ"���G�o�T�r�0��}��Y����x�㳟�=ǆR��a�gc=5��t��9����l���*�}��Mw@C��%�b���@��I��M�Y
[�F��8��w"W�G�P��������^�eIe�$;���S=��Egi ܗ��$`.��I�'Y�Ӑsj��8 |�T��031{S����WP]��2�#{������GT/�7��Ԓ�%�kq��$P9�'��}��L& enx      �   $  x����n�0Eד���(v�v<T���钍�h �șT��שZ��a�s<����v�%�-��+5��J�L�I�L�YRd�8J��b"��'C����ep)l��k��Ż�P~ }1�,�=�`�w�`9l�Eҝ(��f��j[����1�,o�SV�	��Hxr7�u���R�޴��	�]�=�Cϡ�V���X��=�-3(w��/����YQ���,o1zgSS����1{;f'qhd�B��:�.�z�d��2)	oa.g���Jw�w��~RN�::��8����i��      �      x������ � �      �      x������ � �      z   �   x�]��
�0E痯�4$/Mb��M\������!�"���N�]�9WB7�Cʞ�Tv��B��.�[<�%
�Z�u%���5��1;�BbG�_��#\K��J�~�i���)��´�՛�����Z'
������1�c�,�      �   �   x�}��N�0��?OQ�sQ�v �ā�®\�`���t����B��f�?ǖ;l��c����\��)�s
<(����$U�����������T*�fK���7V*�c�t��`-�"�k�5���ܖ@ދ3�� ��T�=�EP�p�̀���; K�J7إIdu'W?�n�ӴB��wX��*O);YҠ,�񖯵h����O9-��#e����t~��ߖ�{�����w      �      x������ � �      |   a   x�3�LL����L��2I̍rO�Ot5-�u�
3�0
��2J�2��t)�,.)J,�/���,�4202�50�5�T04�25�22�363Jr��qqq �)     